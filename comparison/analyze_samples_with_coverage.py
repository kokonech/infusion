import os
import datetime

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import argparse
import sys
import run_comparison

sys.path.append("..")

import matplotlib.pyplot as plt
import numpy as np
import pandas

TOOL_NAME = "InFusion"


def plotResults(data, num_expected ):

    x = []
    y1 = []
    y2 = []

    for item in data:
        x.append( item[0] )
        y1.append( item[1] )
        y2.append( item[2] )
    
    fig, ax = plt.subplots()
    ax.plot(x, y1, label="Validated detected", color = 'g')
    ax.plot(x, y2, label="Total detected", color = 'b' )
    ax.hlines(num_expected, 0, max(x), label="Total validated", color = 'r')


    legend = ax.legend(loc='upper left', shadow=True)

    frame  = legend.get_frame()
    frame.set_facecolor('0.90')
  
    for label in legend.get_texts():
        label.set_fontsize('large')

    for label in legend.get_lines():
        label.set_linewidth(1.5)  # the legend line width

    plt.xlabel("Read depth")
    plt.ylabel("Number of fusions detected")
    plt.title("Fusion detection versus read depth")

    plt.savefig("subsampling_result.svg")

def getSampleStats(fusions, expectedFusions):
    
    tp = 0
    for f in fusions:
        foundFusion  =  run_comparison.addFusionToPool(f, expectedFusions, TOOL_NAME, create=False)
        if foundFusion:
            tp += 1
            #print "Detected %s-%s" % (f.genes1,f.genes2)
    
    return tp, len(fusions)



if __name__ == "__main__":

    descriptionText = "The script compares results of tools for fusion detection for multiple runs"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("sample_list", help="File which contains a list of configuration files corresponding for each run")
    parser.add_argument("--start-size", default=5000000, type=int, dest="startSize", help="Starting size of the sample")
    parser.add_argument("--step", default=5000000, type=int, dest="step", help="Size of the step")
    parser.add_argument("-e", dest="pathToExpected", required=True, help="Path to expected fusions")
    args = parser.parse_args()

    sampleSize = args.startSize
    numExpected = 0

    results = []
    
    for line in open(args.sample_list):
        if not line.strip() or line.startswith("#"):
            continue

        items = line.strip().split()

        expectedFusions = run_comparison.loadExpectedFusions(args.pathToExpected)
        numExpected = len(expectedFusions)

        dataset_name = items[0]
        tool = run_comparison.InFusion()
        
        fusionsPath = os.path.join(dataset_name, "fusions.txt")
        
        if not os.path.exists(fusionsPath):
            print "File %s is not found. Skip..." % fusionsPath
            sampleSize += args.step
            continue
        else:
            print "Loaded file %s " % fusionsPath

        tool.expectedFusionsOutput = fusionsPath

        fusions = tool.getFusionsOutput()

        tp, total = getSampleStats(fusions, expectedFusions)
        print sampleSize, tp, total

        results.append( (sampleSize,tp,total) ) 
        sampleSize += args.step


    plotResults(results, numExpected)

