import os

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import argparse
import sys
import numpy as np
import matplotlib.pyplot as plt

class ComparisonResult:
     
    def __init__(self, name, recall, pr):
        self.toolName = name
        self.precision = pr
        self.recall = recall
                                             
    def __str__(self):
        return "%s %.2f %.2f" % (self.toolName, self.recall, self.precision)

                                                                                                                                                   
def parseResults(resultsPath):
    
   
    results = {}
    
    file = open(resultsPath)
    
    for line in file:

        if line.startswith("#") or line.startswith("Tool") or len(line) == 0:
            continue
    
        items = line.strip().split()
        toolName = items[0]

        recall = float(items[4].split(":")[0])
        pr = float(items[5].split(":")[0])

        r = ComparisonResult(toolName,recall, pr)

        results[toolName] = r
    
    
    return results


def plotResults(simIssues, recallDatasets, title, yLabel, path ):

    
    fig, ax = plt.subplots()
    
    for datasetName in recallDatasets:

        data = recallDatasets[datasetName]   
        #print datasetName
        #print simIssues
        #print data
        ax.plot(simIssues, data, 'o-', label=datasetName )    
        
    
    xLabels = []
    for val in simIssues:
        xLabels.append( str(val) )
    
    plt.xticks(simIssues, xLabels )
    plt.xlabel("Read size (bp)")
    
    legend = ax.legend(loc='lower left', shadow=True)

    ax.set_xlim(simIssues[0] - 25, simIssues[-1] + 25)
    ax.set_ylim(0,1)


    #plt.setp(ax, xticklabels=names)

    plt.ylabel(yLabel)
    plt.title(title)

    plt.savefig(path)


if __name__ == "__main__":

    descriptionText = "The script outputs table of results (recall, precisision) of from a number of simulation expereiments with several datasets"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("resultList", help="File which contains a list of simulation analysis results. Each line of the list has the following parameters: dataset name and path to results.")

    args = parser.parse_args()

    block_datasets = {}
    block_precision = {}
    block_recall = {}

    for line in open(args.resultList):
        if not line.strip() or line.startswith("#"):
            continue
         
        items = line.strip().split()
        assert len(items) == 2

        dataset_name = int(items[0])
        dataset_path = items[1]

        block_datasets[dataset_name] = dataset_path 
    
    #print block_datasets
    
    #names = block_datasets.keys()
    #print names
    #names.sort()
    #print names

    toolSet = set()
    analysisBlocks = {}
    

    for name,path in block_datasets.iteritems():
        
        #print name

        toolResults = parseResults(path)
            
        for tool in toolResults:
            #print toolResults[tool]
            toolSet.add(tool)
                
        analysisBlocks[name] = toolResults

        #print

    keys = block_datasets.keys()
    keys.sort()

    line = "Toolkit"

    for key in keys:
        line += "\t" + str(key)
    print line
    
    recallDatasets = {}
    precisionDatasets = {}

    for tool in toolSet:

        tool_info = tool

        recallData = []
        prData = []

        for key in keys:
            res = analysisBlocks[key]
            
            if tool in res:
                tool_report = res[tool]
                tool_info += "\t%0.2f/%0.2f" % (tool_report.recall, tool_report.precision)
                recallData.append(tool_report.recall)
                prData.append(tool_report.precision)
            else:
                tool_info += "\t."
                recallData.append(0)
                prData.append(0)
                
        print tool_info

        recallDatasets[tool] = recallData
        precisionDatasets[tool] = prData


    print "\n"

    #print recallDatasets
    #print precisionDatasets

    plotResults(keys, recallDatasets, "Fusion detection recall via read size", "Recall", "recall_vs_read_size.svg")    
    plotResults(keys, precisionDatasets, "Fusion detection precision via read size", "Precision", "precision_vs_read_size.svg")    
    







           

        

    


