import os

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import argparse
import sys
import run_comparison

dirpath = os.path.dirname(__file__)
sys.path.append( os.path.join(dirpath, "..") )
from fusion import *

def parseFusions(detailedReportPath):

    results = []

    for line in open(detailedReportPath):
        if line.startswith("#"):
            continue

        items = line.split()

        f= Fusion(items[0])

        f.i1.ref = items[1]
        f.i1.bpos = int(items[2])

        f.i2.ref = items[4]
        f.i2.bpos = int(items[5])

        f.genes1 = items[20].split(";")
        f.genes2 = items[26].split(";")

        f.numSplits = int(items[7])
        f.numPaired = int(items[8])
        f.numSplitsWithPair = int(items[9])
        f.numSplitsRescued = int(items[10])
        f.uniqueStarts = items[11]
        f.papRate = items[12]
        f.split_pos = items[13]
        f.split_std = items[14]
        f.homogeneity = items[15]
        f.expr1 = float(items[24])
        f.expr2 = float(items[30])
        
        #print "Loaded fusion %s-%s" % (f.genes1, f.genes2)
        #sys.stdin.readline()

        results.append(f)


    return results

if __name__ == "__main__":

    descriptionText = "The script outputs detailed fusion statistcs for expected events reported by InFusion from multiple runs using sample list"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("samples_list", help="File which contains a list of configuration files corresponding for each run")

    args = parser.parse_args()

    globalToolNames = set()
    globalResults = []
    out = sys.stdout


    out.write("sample\tgene1\tgene2\tdist")
    out.write("\tnum_splits\tnum_pairs\tnum_splits_with_pair\tnum_not_rescued\texpr1\texpr2\tunique_starts\thomogeneity\n")

    for line in open(args.samples_list):
        if not line.strip() or line.startswith("#"):
            continue

        items = line.strip().split('\t')
        assert len(items) == 5

        dataset_name = items[0]
        sample_name = items[1]
        num_reads = items[2]
        read_size = items[3]
        conf_path = items[4]

        if not conf_path.startswith("/"):
            conf_path = os.path.join( os.path.dirname(args.samples_list), conf_path)

        names = []

        toolConfigs, common = run_comparison.parseConfigurationFile(conf_path)

        fusions = []
        for tool in toolConfigs:
            if tool.name == "InFusion":
                sys.stderr.write( "Loading %s results\n" % tool.name )
                reportPath = tool.expectedFusionsOutput
                detailedReportPath = reportPath.replace(".txt", ".detailed.txt")
                if not os.path.exists(detailedReportPath):
                    sys.stderr.write("ERROR! The output file %s does not exist!" % detailedReportPath)
                    continue
                fusions = parseFusions(detailedReportPath)
                #print "Loaded %d fusions from %s" % (len(fusions), sample_name)
                break

        if not fusions:
            continue

        pathToExpected = common["validated"]

        if not pathToExpected.startswith("/"):
            #path is relative to configuration file
            dirpath = os.path.dirname(conf_path)
            pathToExpected = os.path.join(dirpath,pathToExpected)


        expectedResults = run_comparison.selectExpectedFusions(pathToExpected, "InFusion", fusions)
        #print pathToExpected,len(expectedResults)
        #print "Selected %d fusions from %s" % (len(expectedResults). pathToExpected)

        for f in expectedResults:
            dist = "inf" if f.i1.ref != f.i2.ref else str(abs(f.i1.bpos - f.i2.bpos ))
            out.write("%s\t%s\t%s\t%s\t" % (sample_name, f.genes1[0], f.genes2[0], dist ) )
            out.write("%d\t%d\t%d\t%d\t"  % (f.numSplits, f.numPaired, f.numSplitsWithPair, f.numSplits - f.numSplitsRescued) )
            out.write("%f\t%f\t" % (f.expr1, f.expr2) )
            
            out.write("%s\t%s\n"  % (f.uniqueStarts,  f.homogeneity) )





