import os
import datetime

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import argparse
import sys
import run_comparison
import datetime

sys.path.append("..")

import matplotlib.pyplot as plt
import numpy as np
import pandas

from matplotlib.table import Table

def plotResultsTable(globalResults):



    col_names = ["Sample", "Num reads", "Read size (bp)"]

    for toolName in globalToolNames:
        col_names.append( "%s:VALID" % toolName )
        col_names.append( "%s:TOTAL" % toolName )

    table_rows = []

    for res in globalResults:
        sample_name = res[0]
        num_reads = res[1]
        read_size = res[2]
        res_dict = res[3]
        row = [ sample_name, num_reads, read_size ]
        for toolName in globalToolNames:
            real = 0
            other = 0
            expected = 0
            if toolName in res_dict:
                real,other,expected = res_dict[toolName]
            row.append( "%d of %d" % (real,expected) )
            row.append( "%d" % (other + real) )


        table_rows.append ( row )



    data = pandas.DataFrame(table_rows,
                            columns=col_names)


    fig, ax = plt.subplots()
    ax.set_axis_off()
    tb = Table(ax, bbox=[0,0,1,1])

    nrows, ncols = data.shape
    width, height = 1.0 / ncols, 1.0 / nrows

    for (i,j), val in np.ndenumerate(data):
        tb.add_cell(i, j, width, height, text=val,
                loc='center')

    for j, label in enumerate(data.columns):
        tb.add_cell(-1, j, width, height/2, text=label, loc='center',
                    edgecolor='black', facecolor='none')

    ax.add_table(tb)
    #plt.show()
    fig.set_size_inches(20,5)
    #fig.frameon = False
    plt.savefig("/home/kokonech/test.png")



def plotResults(globalResults):

    import matplotlib.pylab as plt

    fig = plt.figure()

    font = {'family' : 'normal',
            'weight' : 'normal',
            'size'   : 36}

    #matplotlib.rc('font', **font)

    frame1 = plt.gca()
    for xlabel_i in frame1.axes.get_xticklabels():
        xlabel_i.set_visible(False)
        xlabel_i.set_fontsize(0.0)
    for xlabel_i in frame1.axes.get_yticklabels():
        xlabel_i.set_fontsize(0.0)
        xlabel_i.set_visible(False)
    for tick in frame1.axes.get_xticklines():
        tick.set_visible(False)
    for tick in frame1.axes.get_yticklines():
        tick.set_visible(False)

    col_names = ["Sample"]

    for toolName in globalToolNames:
        col_names.append( "%s:valid" % toolName )
        col_names.append( "%s:other" % toolName )

    row_names = []
    table_rows = []

    for res in globalResults:
        sample_name = res[0]
        res_dict = res[1]
        row = [ sample_name ]
        for toolName in globalToolNames:
            real = 0
            total = 0
            if toolName in res_dict:
                real,total = res_dict[toolName]
            row.append( "%d" % real )
            row.append( "%d" % total )


        table_rows.append ( row )

    table_vals=[[11,12,13],[21,22,23],[31,32,33]]
    # the rectangle is where I want to place the table
    #the_table = plt.table(cellText=table_vals,
    #                  colWidths = [0.1]*3,
    #                  rowLabels=row_labels,
    #                  colLabels=col_labels,
    #                  loc='center')

    plt.table(cellText=table_rows, cellLoc='center', colLabels=col_names, loc='center')
    #plt.text(12,3.4,'Table Title',size=8)

    #plt.show()
    fig.set_size_inches(20,5)
    #fig.frameon = False
    date_str = datetime.datetime.now().strftime("%y-%m-%d_%H_%M_%S")
    plt.savefig("/home/kokonech/result.png" % date_str)

class ResultStore:
    def __init__(self):
        self.total_num_reads = 0
        self.total_tp = 0
        self.total_expected = 0
        self.total_detected = 0

def outputLatexTable(latexTable, globalResults):

    report = open(latexTable, "w")

    table_rows = []

    total_num_reads = 0
    totals = {}

    for res in globalResults:
        sample_name = res[0]
        num_reads = res[1]
        total_num_reads += int(num_reads)
        res_dict = res[3]
        row = [ sample_name, num_reads ]
        for toolName in globalToolNames:
            real = 0
            other = 0
            expected = 0
            if toolName in res_dict:
                real,other,expected = res_dict[toolName]
                if not toolName in totals:
                    totals[toolName] = ResultStore()
                res = totals[toolName]
                res.total_tp += real
                res.total_expected += expected
                res.total_detected += (other + real)

            row.append( "%d of %d / %d" % (real,expected, other + real) )

        table_rows.append ( row )

    num_tools = len(globalToolNames)

    columns_str = "|".join( ["c"]*(num_tools + 2) )

    tool_column_names = []
    for tool_name in globalToolNames:
        tool_column_names.append( "%s (V/T)" % tool_name )

    tool_names_str = " & ".join(tool_column_names)

    report.write("\\begin{tabular} {|%s|}\n" % columns_str)
    report.write("\hline\n")
    report.write("Sample & Num reads & %s \\\\\n" % tool_names_str)
    report.write("\hline\n")
    for row in table_rows:
        report.write("%s \\\\\n" %  " & ".join( row) )
        report.write("\hline\n")

    final_row = ["Summary", str(total_num_reads)]
    for tool_name in globalToolNames:
        res = totals[tool_name]
        final_row.append(  "%d of %d / %d" % (res.total_tp, res.total_expected, res.total_detected) )

    report.write("%s \\\\\n" %  " & ".join( final_row ) )
    report.write("\hline\n")


    report.write("\\end{tabular}\n")

   

def loadPerfData(path):
    
    perf_data = {}

    for line in open(path):
        items = line.split()
        sample_name = items[0]
        tool_name = items[1]
        time_path = os.path.join(".", sample_name, items[2])
        if not os.path.exists(time_path):
            print "FAIL! ", time_path
            continue
        elapsedTime, peakMemory = run_comparison.parsePerfReport(time_path)
        tm_items = elapsedTime.split(":")
        
        if len(tm_items) == 3:
            hours, minutes, seconds =  int(tm_items[0]), int(tm_items[1]), int(tm_items[2])
        else:
            assert len(tm_items) == 2
            hours, minutes, seconds = 0, int(tm_items[0]), int(float(tm_items[1]))

        dt = datetime.timedelta(hours = hours, minutes = minutes, seconds = seconds)
        if tool_name not in perf_data:
            perf_data[tool_name] = []
        
        perf_data[tool_name].append( (sample_name, dt, peakMemory) )
    
    return perf_data

def sortedNames(names):
    
    requiredNames = ["InFusion", "deFuse", "ChimeraScan", "TophatFusion", "SOAPFuse", "FusionCatcher" ]

    for name in names:
        if name not in requiredNames:
            sys.err.println("Warning! Tool %s is not found in required tools, skipping sorting..." % name)
            return names

    return requiredNames


def saveExpectedFusionsReport(expected, toolNames, path ):
    outfile = open(path, "w")
    
    outfile.write("Sample\tFusion gene\t" + "\t".join(toolNames) + "\n")

    for sample,fusions in expected:
        for f in fusions:
            outfile.write( "%s\t%s-%s\t" % ( sample, f.genes1[0], f.genes2[0]) )
            results = []
            for toolName in toolNames:
                if toolName in f.tools:
                    results.append( "yes" )
                else:
                    results.append( "no" )
            outfile.write( "\t".join(results) )
            outfile.write("\n")


if __name__ == "__main__":

    descriptionText = "The script compares results of tools for fusion detection for multiple runs"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("samples_list", help="File which contains a list of configuration files corresponding for each run")
    parser.add_argument("--latex", default="", dest="latexReport", help="Path to out table in latex format")
    parser.add_argument("--perf", default="", dest="perfMetrics", help="Path to performance data")
    parser.add_argument("--tn", default="", dest="toolNames", help="List of tool names to output. For example: InFusion,deFuse,TophatFusion")

    args = parser.parse_args()

    globalToolNames = set()
    globalResults = []
    global_dataset_name = ""
    global_read_size = ""
    global_perf_data = {}
    total_expected_fusions = []

    if args.perfMetrics:
        global_perf_data = loadPerfData(args.perfMetrics)

    for line in open(args.samples_list):
        if not line.strip() or line.startswith("#"):
            continue

        items = line.strip().split('\t')
        assert len(items) == 5

        dataset_name = items[0]
        sample_name = items[1]
        num_reads = int(items[2])
        read_size = items[3]
        conf_path = items[4]
        
        if not global_dataset_name:
            global_dataset_name = dataset_name

        if not global_read_size:
            global_read_size = read_size

        if not conf_path.startswith("/"):
            conf_path = os.path.join( os.path.dirname(args.samples_list), conf_path)

        names = []
        results = []

        toolConfigs, common = run_comparison.parseConfigurationFile(conf_path)

        for tool in toolConfigs:
            if os.path.exists(tool.expectedFusionsOutput):
                sys.stderr.write( "Loading %s results\n" % tool.name )
                fusions = tool.getFusionsOutput()
            else:
                sys.stderr.write("WARNING! No fusions detected by %s\n" % tool.name)
                fusions = []
            results.append (  [tool.name, fusions] )

        pathToExpected = common["validated"]

        if not pathToExpected.startswith("/"):
            #path is relative to configuration file
            dirpath = os.path.dirname(conf_path)
            pathToExpected = os.path.join(dirpath,pathToExpected)


        comparison_results,expected_fusions = run_comparison.createComparisonSummary(pathToExpected, results)
        #print comparison_results
        total_expected_fusions.append(  (sample_name,expected_fusions) )

        line = sample_name

        toolNames = comparison_results.keys()
        for toolName in toolNames:
            globalToolNames.add(toolName)

        globalResults.append ( [sample_name, num_reads, read_size, comparison_results] )
    

    if args.toolNames:
        globalToolNames = args.toolNames.split(",")
   

    header = "Dataset\tNum. reads\tRead size\tValidated"
    for toolName in globalToolNames:
        header += "\t%s (Found|Total)" % toolName
    print header

    
    totals = {}
    total_num_reads = 0

    totalValidated = 0

    for res in globalResults:
        sample_name = res[0]
        num_reads = res[1]
        read_size = res[2]
        res_dict = res[3]
        line = "%s\t%.2fM\t%s" % (sample_name, num_reads / 1000000., read_size)
        total_num_reads += num_reads

        validatedWritten = False
        validated = 0

        for toolName in globalToolNames:
            if toolName in res_dict:
                tool_totals = totals.get(toolName, [0,0,0] )
                real,other,expected = res_dict[toolName]

                if not validatedWritten:
                    line += "\t%d" % expected
                    validated += expected
                    validatedWritten = True

                line += "\t%d|%d" % (real,  other + real)
                tool_totals = [x + y for x,y in zip(tool_totals, res_dict[toolName]) ]
                totals[toolName] = tool_totals
        
        totalValidated += validated
        print line
    
    line = "%s\t%.1f M\t%s\t%d" % (global_dataset_name, total_num_reads / 1000000., global_read_size, totalValidated)
    
    for toolName in globalToolNames:
        real, other, expected = totals[toolName]
        
        line += "\t%d|%d" % (real, other + real)
        if toolName in global_perf_data:
            elapsed = datetime.timedelta()
            tool_data = global_perf_data[toolName]
            for s in tool_data:
                elapsed += s[1]
                        
            line += " " + str(elapsed)

    print line

    if args.latexReport:
        outputLatexTable(args.latexReport, globalResults)

    plotResultsTable(globalResults)

    saveExpectedFusionsReport(total_expected_fusions, globalToolNames,  "fusions.txt")

