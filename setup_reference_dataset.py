#!/usr/bin/python

__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import argparse
import os
import gzip
import subprocess
import multiprocessing
import sys
from os.path import join as joinpath
from ftplib import FTP
from ftplib import Error as FTPError

# URLS

ENSEMBLE_SERVER = "ftp.ensembl.org"
PATH_TO_ENSEMBLE_CDNA = "pub/release-%v/fasta/homo_sapiens/cdna/Homo_sapiens.GRCh37.%v.cdna.all.fa.gz"
PATH_TO_ENSEMBLE_GTF = "pub/release-%v/gtf/homo_sapiens/Homo_sapiens.GRCh37.%v.gtf.gz"
PATH_TO_ENSEMBLE_DNA = "pub/release-%v/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.%v.dna.chromosome.%c.fa.gz"

UCSC_REPEATS_TRACK = "hgdownload.cse.ucsc.edu/goldenPath/hg19/database/rmsk.txt.gz"

HUMAN_GENOME_NAME = "Homo_sapiens.GRCh37.%v.dna.fa"
HUMAN_CHROMOSOMES = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15",
                     "16","17","18","19","20","21","22","X","Y","MT"]


class ConfigWriter:
    def __init__(self, fileName):
        self.configFile = open(fileName, "w")
    def writeValue(self, name, value):
        self.configFile.write("%s = %s\n\n" % (name,str(value)))

    def writeComment(self, comment):
        self.configFile.write("# %s\n\n" % comment)

def saveConfiguration(args):

    outDir = getOutDir(args)
    args.outFile = os.path.join(outDir, "infusion.cfg")

    configWriter = ConfigWriter(args.outFile)
    configWriter.writeComment("InFusion configuration file")

    configWriter.writeComment("path to reference")
    configWriter.writeValue("reference", args.genomeFile)
    configWriter.writeValue("bowtie2_index", args.genomeIndexFile)

    configWriter.writeComment("number of computational threads")
    configWriter.writeValue("num_threads" ,args.numThreads)

    configWriter.writeComment("path to transcriptome")
    configWriter.writeValue("transcripts", args.gtfFile)
    configWriter.writeValue("transcriptome", args.transcriptsFile)
    configWriter.writeValue("transcriptome_index", args.transcriptsIndexFile)

    configWriter.writeComment("path to repeats")

    configWriter.writeValue("repeats_track", args.repeatsTrack )

    #configWriter.writeComment("minimum number of reads spanning fusion junction")
    #configWriter.writeValue("min_span_reads", 2)

    #configWriter.writeComment("minimum number read pairs supporting the breakpoint ")
    #configWriter.writeValue("min_bridge_paired_reads", 0)

    #configWriter.writeComment("minimum number of fragments (both splitted and bridged) supporting the fusion ")
    #configWriter.writeValue("min_fragments", 3)

    configWriter.writeComment("maximum number of multimapped genomic alignments")
    configWriter.writeValue("num_genomic_alignments_per_read", 20)

    configWriter.writeValue("num_seed_mismatches", 1 )

    configWriter.writeValue("ignore_chromosomes", "MT" )

    configWriter.writeValue("library_protocol" ,"NA" )

    configWriter.writeComment("minimum distance between local alignments in a SPLIT read")
    configWriter.writeValue("local_alignment_in_read_tolerance", 2)
    
    configWriter.writeComment("maximum distance between local alignments in a SPLIT read")
    configWriter.writeValue("local_alignment_in_read_max_overlap", 10 )

    configWriter.writeComment("maximum intron size, everything bigger than that considered as possible abberation")
    configWriter.writeValue("max_intron_size", 20000)

    configWriter.writeValue("excluded_biotypes",  "pseudogene,polymorphic_pseudogene")


def runBowtie2Indexer(inputPath, indexerPath):

    #TODO: what if extention is other than .fa?
    resultIndexPath = inputPath.replace(".fa", "")

    #todo: this is copy-paste from infusion module
    extentions = [ "1.bt2", "2.bt2", "3.bt2", "4.bt2", "rev.1.bt2", "rev.2.bt2" ]

    indexExists = True
    for ext in extentions:
        fileName = resultIndexPath + "." + ext
        if not os.path.exists(fileName):
            indexExists = False
            break

    if not indexExists:

        print "Index %s doesn't exist, performing bowtie2 indexing..." % resultIndexPath
        args = [indexerPath]
        args.append(inputPath)

        args.append(resultIndexPath)

        proc = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=dict(os.environ) )

        stdout_str, stderr_str = proc.communicate()
        if proc.returncode:
            raise RuntimeError("Bowtie2-build command crashed",stdout_str, stderr_str)

        print "Bowtie2 index is created.\n"
    else:
        print "Bowtie2 index for %s exists, skip indexing..." % inputPath

    return resultIndexPath


def getOutDir(args):

    if args.outDir is None:
        raise RuntimeError("The output directory for reference dataset is not given, but required.")

    if not os.path.exists(args.outDir):
        os.mkdir(args.outDir)

    return os.path.abspath(args.outDir)

def downloadDataFromFtp(url, targetPath):

    if url.startswith("ftp://"):
        url = url.replace("ftp://","")

    url_items = url.split("/")
    server = url_items[0]
    subpath = "/".join(url_items[1:])

    downloadDataFromFtpServer(server, subpath, targetPath)



def downloadDataFromFtpServer(server, downloadPath, targetPath ):

    if os.path.exists(targetPath):
        print "File %s exists, skip downloading" % targetPath
        return

    ftp = FTP(server)
    ftp.login()
    remoteDirPath = os.path.dirname(downloadPath)
    remoteFileName = os.path.basename(downloadPath)

    ftp.cwd( remoteDirPath )

    lf = open(targetPath, "wb")
    print "Downloading file %s.%s ..." % (server,downloadPath)
    ftp.retrbinary("RETR " + remoteFileName, lf.write, 8*1024)
    lf.close()
    print "File downloaded to ", targetPath

def unzipFile(gzippedPath, removeGzipped):
    targetPath = gzippedPath.replace(".gz", "")
    if not os.path.exists(targetPath):
        targetFile = open(targetPath, "w")
        print "Unpacking file %s ..." % gzippedPath
        try:
            zipFile = gzip.open(gzippedPath, "rb")

            for line in zipFile:
                if line.startswith(">"):
                    line = line.split()[0] + "\n"
                targetFile.write(line)

            targetFile.close()
            print "File unpacked to %s\n" % targetPath
            zipFile.close()

        except IOError:

            print "Failed to unpack %s, removing data"
            targetFile.close()
            os.remove(targetPath)
            raise

    if removeGzipped:
        os.remove(gzippedPath)

    return targetPath


def downloadFileFromEnsemble(outdir, downloadPath, unzip):

    targetGzippedPath = joinpath( outdir, os.path.basename(downloadPath) )
    try:
        downloadDataFromFtpServer(ENSEMBLE_SERVER, downloadPath, targetGzippedPath)
    except FTPError:
        print "Failed to download %s, removing data" % downloadPath
        os.remove(targetGzippedPath)
        raise

    targetPath = targetGzippedPath.replace(".gz", "")
    if unzip:
        if not os.path.exists(targetPath):
            targetFile = open(targetPath, "w")
            print "Unpacking file %s ..." % targetGzippedPath
            try:
                zipFile = gzip.open(targetGzippedPath, "rb")

                for line in zipFile:
                    if line.startswith(">"):
                        line = line.split()[0] + "\n"
                    targetFile.write(line)

                targetFile.close()
                print "File unpacked to %s\n" % targetPath

            except IOError:

                print "Failed to unpack %s, removing data"
                targetFile.close()
                os.remove(targetPath)
                raise

        os.remove(targetGzippedPath)

        return targetPath

    else:

        return targetGzippedPath


def downloadGenome(outdir, ensVersion):

    genomeFilePath = joinpath(outdir, HUMAN_GENOME_NAME.replace("%v", ensVersion))

    if not os.path.exists(genomeFilePath):

        genomeFile = open(genomeFilePath, "w")

        for chrName in HUMAN_CHROMOSOMES:
            downloadPath = PATH_TO_ENSEMBLE_DNA.replace("%v", ensVersion).replace("%c", chrName)
            chrFilePath = downloadFileFromEnsemble(outdir, downloadPath, False)
            chrFile = gzip.open(chrFilePath, "rb")

            for line in chrFile:
                if line.startswith(">"):
                    line = line.split()[0] + "\n"
                genomeFile.write(line)

            os.remove(chrFilePath)

        genomeFile.close()

    return genomeFilePath



def getIndexerPath(bowtieDir):
    fullDirPath = os.path.abspath(bowtieDir)
    return os.path.join(fullDirPath,"bowtie2-build")

def verifyFile(fileName):
    if not os.path.exists(fileName):
        raise IOError("File %s doesn't exist" % fileName )   
    return os.path.abspath(fileName)

def verifyBowtie2Index(fileName):
    fName = fileName + ".1.bt2"
    if not os.path.exists(fName):
        raise IOError("Index %s doesn't exist" % fileName )   
    return os.path.abspath(fName).replace(".1.bt2", "")

def verifyExecutable(fileName):

    infusionDir = os.path.dirname(__file__)
    externalToolsDir = os.path.join(infusionDir, "external")

    os.environ["PATH"] = infusionDir + os.pathsep + os.environ["PATH"]
    if os.path.exists(externalToolsDir):
        os.environ["PATH"] = externalToolsDir + os.pathsep + os.environ["PATH"]

    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(fileName)
    if fpath:
        if is_exe(fileName):
            return fileName
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, fileName)
            if is_exe(exe_file):
                return exe_file

    return None

def confirmTranscriptSeqs(cdnaFastaFile):
    
    novelName = cdnaFastaFile.replace(".fa", ".adjusted.fa")
    novelFile = open(novelName, "w")

    for line in open(cdnaFastaFile):
        if line.startswith(">"):
            items = line.strip().split()
            vals = items[0].split(".")
            if len(vals) == 2:
                items[0] = vals[0]
                items.append("ver:" + vals[1])
            line = " ".join(items) + "\n"

        novelFile.write(line)
    
    return novelName


# this function can be applied to make sure 
# that the annotations pass the limits

def verifyAnnotation(transcriptsFile, gtfFile):
    transcriptsCdna = set()
    for line in open(transcriptsFile):
        tName = line.split()[0][1:]
        transcriptsCdna.add(tName)

    transcriptsCommon= set()
    transcriptsGtf = set()
    detectedProps = set()

    for line in open(gtfFile):
        if line.startswith("#"):
            continue
        items = line.strip().split("\t")
        if (items[2] == "exon"):
            props = items[-1].split(";")
            for p in props:
                if len(p) == 0:
                    continue
                vals = p.split()
                #print vals
                detectedProps.add(vals[0])
                if vals[0] == "transcript_id":
                    if vals[1][1:-1] in transcriptsCdna:
                        transcriptsCommon.add(vals[1])
                    #else:
                        #print "NOT FOUND: ",vals[1][1:-1]
                    transcriptsGtf.add(vals[1])
    
    # check required exon attributes
    requiredProps = [ "gene_id", "gene_name", "transcript_id", "transcript_name", "exon_number"]

    notFoundProps = []
    for prop in requiredProps:
        if prop not in detectedProps:
            notFoundProps.append(prop)

    if len(notFoundProps) > 0:
        print "ERROR! The GTF file does not have special required attributes for exons: %s\n" % ",".join(notFoundProps)
        print "Use Ensembl/Gencode annotation or modify the annotation file to fill in missing informaton accordingly\n"
        sys.exit(-1)
    else:
        print "All required exon attributes are present in GTF records\n"
 
    # check transcripts
    if len(transcriptsCommon) == 0:
        print "ERROR! Theree are no cDNA sequences detected for transcripts in GTF file!"
        print "In cDNA - %d, in GTF - %d, common - %d" % ( len(transcriptsCdna), len(transcriptsGtf), len(transcriptsCommon) )
        print "Possible solution: ID adjustment option \"--adj-cdna-ids\" to exclude index suffix in the name of transcript in cDNA FASTA file\n"
        sys.exit(-1)
    else:
        print "Annotated genes cDNA detected: %.2f\n"  % (float(len(transcriptsCommon)) / len(transcriptsGtf) )

       


if __name__ == "__main__":

    descriptionText = "The script creates a reference dataset for Infusion pipeline " \
                      "by using existing reference genome and annotations or by downloading data from Ensembl"
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument("-gtf", dest="gtfFile", required=False,
                        help="Annotations in GTF format. Make sure the chromosome names the same as in the reference genome")

    parser.add_argument("-f", dest="genomeFile", required=False, help="Reference genome sequence in FASTA format.")

    parser.add_argument("-t", dest="transcriptsFile", required=False, help="Transcript sequences (cDNA) in FASTA format.")

    parser.add_argument("--adj-cdna-ids", dest="adjustCdnaIds", action="store_true", default=False, help="Special option to exclude suffix id in transcript sequences (cDNA) in FASTA file")


    parser.add_argument("-r", dest="repeatsTrack", required=False, help="Repeats track from UCSC. hg19 genome is assumed.")

    parser.add_argument("-fIdx", dest="genomeIndexFile", required=False, help="Genome sequence in FASTA format.")

    parser.add_argument("-tIdx", dest="transcriptsIndexFile", required=False, help="Transcript sequences in FASTA format.")

    parser.add_argument("-o", dest="outDir", required=True, help="Path to resulting reference dataset directory")

    parser.add_argument("--num-threads", dest="numThreads", default=multiprocessing.cpu_count(),
                        help="Number of threads to use when running InFusion. "
                             "Default equals to the number of available CPUs.")

    parser.add_argument("--ens-ver", dest="ensVersion", default="68",
                        help="Ensemble release number used to create database. Default is \"68\"")
    	
    parser.add_argument("-a", dest="indexerPath", default="bowtie2-build", help="Path to indexer (Default: bowtie2-build)" )    

    args = parser.parse_args()

    print "Preparing reference dataset\n"

    indexer = verifyExecutable(args.indexerPath)
    if indexer is None:
        print "The indexer %s doesn't exist. Please check the path." % args.indexerPath
        exit(-1)
    else:
        print "Indexer is ", indexer

    if args.genomeFile is None:
        args.genomeFile = downloadGenome(getOutDir(args), args.ensVersion)
    else:
        args.genomeFile = verifyFile(args.genomeFile)
    print "Set reference genome sequence: %s\n" % args.genomeFile
    
    if args.gtfFile is None:
        downloadPath = PATH_TO_ENSEMBLE_GTF.replace("%v", args.ensVersion)
        args.gtfFile = downloadFileFromEnsemble(getOutDir(args), downloadPath, unzip=True)
    else:
        args.gtfFile = verifyFile(args.gtfFile)

    print "Set transcript annotations: %s\n" % args.gtfFile

    if args.transcriptsFile is None:
        downloadPath = PATH_TO_ENSEMBLE_CDNA.replace("%v", args.ensVersion)
        args.transcriptsFile = downloadFileFromEnsemble(getOutDir(args), downloadPath,unzip=True)
    else:
        args.transcriptsFile = verifyFile(args.transcriptsFile)
    
    if args.adjustCdnaIds:
        print "Adjust Ensembl annotation transcript sequence names...\n"
        args.transcriptsFile = confirmTranscriptSeqs(args.transcriptsFile)
    
    print "Verify annotations...\n"
    verifyAnnotation(args.transcriptsFile, args.gtfFile)

    print "Set transcript sequences: %s\n" % args.transcriptsFile

    if args.repeatsTrack is None:
        targetPath = os.path.join(getOutDir(args), "hg19.ucscRepeats.txt")
        if not os.path.exists(targetPath):
            targetPath += ".gz"
            downloadDataFromFtp(UCSC_REPEATS_TRACK, targetPath)
            args.repeatsTrack = unzipFile(targetPath, True)
        else:
            args.repeatsTrack = verifyFile(targetPath)
    else:
        args.repeatsTrack = verifyFile(args.repeatsTrack)

    print "Set repeats track: %s\n" % args.repeatsTrack


    if args.genomeIndexFile is None:
        args.genomeIndexFile =  runBowtie2Indexer(args.genomeFile, indexer)
    else:
        args.genomeIndexFile = verifyBowtie2Index(args.genomeIndexFile)

    print "Set genome Bowtie2 index: %s\n" % args.genomeIndexFile

    if args.transcriptsIndexFile is None:
        args.transcriptsIndexFile =  runBowtie2Indexer(args.transcriptsFile, indexer)
    else:
        args.transcriptsIndexFile = verifyBowtie2Index(args.transcriptsIndexFile)

    print "Set transcriptome Bowtie2 index: %s\n" % args.transcriptsIndexFile

    saveConfiguration(args)
	
    #cleanup()
    print "Reference dataset configuration %s is ready.\n" % args.outFile
	

