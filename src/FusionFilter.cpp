#include <seqan/seq_io.h>

#include "Fusion.h"
#include "StringUtils.h"
#include "FusionGeneAnnotation.h"
#include "Reference.h"
#include "FusionFilter.h"
#include "InsertSizeDistr.h"
#include "AnalyzeClusterHomogeneityTask.h"
#include "AnalyzeFusionCoverageTask.h"

using namespace std;

FusionFilter::Result MinSupportFilter::process(Fusion &f)
{
    Result res;

    int numSplits = 0;
    int numSpanPairs = 0;
    int numSplitsWithPair = 0;
    int numSplitsRescued = 0;

    // A pair of split reads could be inside of fusion,
    // therefore only one split read should be counted
    set<string> commonSplitReads;

    foreach_ (const BreakpointCandidatePtr& bp, f.getBreakpoints()) {
        if (bp->isOriginLocal() ) {
            numSplits++;
            if (bp->hasProperPair()) {
                string readName = StringUtils::removeReadMateIdentifer(bp->readName);
                if (commonSplitReads.count(readName) == 0) {
                    numSplitsWithPair++;
                    commonSplitReads.insert(readName);
                }
            }
            if (bp->isRedeemed()) {
                numSplitsRescued++;
            }
        } else if (bp->isOriginPaired()) {
            numSpanPairs++;
        } else {
            assert(0);
        }
    }


    int minSplitReadsAdjusted = cfg.minSplitReads;

    auto iter = fusionJunctionMap.find(f.getIndex());
    if (iter != fusionJunctionMap.end()) {
        FusionJunctionType junctionType = iter->second;
        if (junctionType != FUSION_JUNCTION_KNOWN && cfg.minSplitReads < cfg.minSplitReadsNovelJunction ) {
            minSplitReadsAdjusted =  cfg.minSplitReadsNovelJunction;
        }
    }

    if (isPaired) {
        res.passed = (numSplitsWithPair >= minSplitReadsAdjusted) && (numSpanPairs >= cfg.minSpanPairs) && ( (numSplitsWithPair + numSpanPairs) >= cfg.minFragments);
    } else {
        assert(numSpanPairs == 0);
        res.passed = (numSplits >= minSplitReadsAdjusted) && ( numSplits >= cfg.minFragments);
    }

    int localNotRescued = numSplits - numSplitsRescued;
    if (res.passed && (localNotRescued < cfg.minNotRescuedReads) ) {
        res.passed = false;
        res.message = "min_not_rescued_splits";
    }

    f.setAnnotationValue(ANNOTATION_NUM_SPLIT_READS, StringUtils::numToStr(numSplits));
    f.setAnnotationValue(ANNOTATION_NUM_SPANNING_PAIRS, StringUtils::numToStr(numSpanPairs));
    f.setAnnotationValue(ANNOTATION_NUM_SPLIT_WITH_PAIR, StringUtils::numToStr(numSplitsWithPair));
    f.setAnnotationValue(ANNOTATION_NUM_SPLITS_RESCUED, StringUtils::numToStr(numSplitsRescued));

    return res;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



static bool inline haveSameGenes(const GenomicAnnotation& a1, const GenomicAnnotation& a2) {
     foreach_ (const RegionDesc& f1, a1.features) {
         foreach_ (const RegionDesc& f2, a2.features) {
             if (f1.geneName == f2.geneName) {
                 return true;
             }
         }
     }

     return false;
}


static inline bool hasExcludedBiotype(const GenomicAnnotation& a, const std::set<string>& excludedBiotypes ) {
    int numNormalBiotypes = 0;
    int numExcludedBiotypes = 0;
    foreach_ (const RegionDesc& feature, a.features) {
        if (excludedBiotypes.count(feature.bioType) > 0) {
            numExcludedBiotypes++;
        } else {
            numNormalBiotypes++;
        }

    }

    return numExcludedBiotypes > numNormalBiotypes;
}

FusionFilter::Result NonCodingRegionFilter::process(Fusion& fusion)
{
    Result res;

    string a1Str = fusion.getAnnotationValue(ANNOTATION_GENE_1, "");
    string a2Str = fusion.getAnnotationValue(ANNOTATION_GENE_2, "");

    if (a1Str.length() > 0 && a2Str.length() > 0) {
        GenomicAnnotation a1 = GenomicAnnotation::parseAnnotation(a1Str);
        GenomicAnnotation a2 = GenomicAnnotation::parseAnnotation(a2Str);


        if (a1.isIntergenic() || a2.isIntergenic()) {
            fusionJunctionMap[fusion.getIndex()] = FUSION_JUNCTION_INTERGENIC;
        } else if (a1.isIntronic() || a2.isIntronic()) {
            fusionJunctionMap[fusion.getIndex()] = FUSION_JUNCTION_INTRONIC;
        } else {
            fusionJunctionMap[fusion.getIndex()] = FUSION_JUNCTION_KNOWN;
        }

        if ( hasExcludedBiotype(a1, cfg.excludedBiotypes) ) {
            res.message = "gene_1_excluded_biotype";
            res.passed = false;
            return res;
        }


        if ( hasExcludedBiotype(a2, cfg.excludedBiotypes) ) {
            res.message = "gene_2_excluded_biotype";
            res.passed = false;
            return res;
        }


        if (!cfg.allowIntergenic) {
            if ( a1.isIntergenic() || a2.isIntergenic() ) {
                res.passed = false;
                res.message = "intergenic";
                return res;
            }
        }

        if (!cfg.allowIntronic) {
            if (a1.isIntronic() || a2.isIntronic()) {
                res.passed = false;
                res.message = "intronic";
                return res;
            }
        }

        if (a1.isIntergenic() && a2.isIntergenic()) {
            res.passed = false;
            res.message = "both_intergenic";
            return res;
        }

        if (!cfg.allowNonCoding) {
            // there could be a fusion of an intergenic region with a coding protein
            bool c1 = a1.isProteinCoding() || a1.isIntergenic();
            bool c2 = a2.isProteinCoding()  || a1.isIntergenic();
            if (  !(c1 && c2) ) {
                res.message = "non_coding";
                res.passed = false;
                return res;
            }
        }

        if (haveSameGenes(a1, a2)) {
            res.message = "same_gene";
            res.passed = false;
        }

    }


    return res;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


HomologyFilter::HomologyFilter()
: FusionFilter("homology")
{


}

HomologyFilter::~HomologyFilter()
{

}

AlignmentResult HomologyFilter::parseAnnotation(const string &annStr) {
    AlignmentResult res;
    if (annStr.size() > 0) {
        vector<string> items = StringUtils::split(annStr, ',' );
        if (items.size() == 2) {
            bool ok = true;
            res.score = StringUtils::parseNumber<int>(items.at(0), ok);
            assert(ok);
            res.percentageIdentity = StringUtils::parseNumber<int>(items.at(1), ok);
            assert(ok);
        }
    }

    return res;
}


FusionFilter::Result HomologyFilter::process(Fusion& fusion)
{
    Result res;

    string annStr1(fusion.getAnnotationValue(ANNOTATION_HOMOLOGY_1) );
    string annStr2(fusion.getAnnotationValue(ANNOTATION_HOMOLOGY_2) );

    if (annStr1.length() > 0 && annStr2.length() > 0) {

        AlignmentResult r1 = parseAnnotation( annStr1 );
        AlignmentResult r2 = parseAnnotation( annStr2 );

        if (r1.percentageIdentity > 80 && r2.percentageIdentity > 80) {
            res.passed = false;
        }
    }


    if ( fusion.hasAnnotation(ANNOTATION_HOMOLOGOUS_GENES) || fusion.hasAnnotation(ANNOTATION_LOCAL_HOMOLOGY) ) {
        res.passed = false;
    }

    return res;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


FusionFilter::Result InsertSizeFilter::process(Fusion &fusion)
{
    Result res;

    string annStr = fusion.getAnnotationValue(ANNOTATION_INSERT_SIZE);

    bool ok = false;
    float papRate = DistrParams::parsePapRateFromAnnotation(annStr, ok);
    if (ok ) {
        res.score = papRate*100;
        res.passed = papRate >= minPAPRate;
    }

    return res;

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


FusionFilter::Result SplitCoverageFilter::process(Fusion &fusion)
{
    Result res;

    string annStr = fusion.getAnnotationValue(ANNOTATION_NUM_UNIQUE_STARTS);

    if (annStr.length() > 0) {
        bool ok = false;
        vector<string> rates = StringUtils::split(annStr, ';');
        if (rates.size() != 3) {
            return res;
        }

        float startsRate = StringUtils::parseNumber<float>(rates[0], ok);
        float endsRate = StringUtils::parseNumber<float>(rates[1], ok);
        int numUniqueSplitReads = StringUtils::parseNumber<int>(rates[2], ok);

        // Warning! Make sure the min support filter is applied er!
        /*string numSplitsRescuedAnn = fusion.getAnnotationValue(ANNOTATION_NUM_SPLITS_RESCUED, "");
        string numSplitsAnn = fusion.getAnnotationValue(ANNOTATION_NUM_SPLIT_READS, "");

        if (numSplitsRescuedAnn.size() > 0 && numSplitsAnn.size() > 0) {
            int numSplits = boost::lexical_cast<int>(numSplitsRescuedAnn);
            int numSplitsRescued = boost::lexical_cast<int>(numSplitsAnn);
            if ( (numSplits - numSplitsRescued) == 0 && numUniqueSplitReads == 0) {
                res.passed = false;
                return res;
            }
        }*/

        if (ok ) {
            res.passed = (startsRate >= minUniqReadRate) && (endsRate >= minUniqReadRate) && (numUniqueSplitReads >= minUniqueSplitReads);
        }
    }

    return res;

}


bool ParalogsFilter::loadParalogs(const string &filePath)
{
    std::ifstream infile;

    infile.open(filePath);
    if ( !infile.is_open())  {
        return false;
    }

    string line;

    while (getline(infile,line)) {
        if (StringUtils::trim(line).length() == 0 || line[0] == '#' ) {
            continue;
        }

        vector<string> items = StringUtils::split(line, '\t');
        const string& genePair = items.at(0);

        paralogs.insert(genePair);
    }

    return true;
}

ParalogsFilter::ParalogsFilter(const string &pathToParologsList) : FusionFilter("paralogs")
{
    loadedParalogs = loadParalogs(pathToParologsList);
}

FusionFilter::Result ParalogsFilter::process(Fusion &f)
{
    Result res;

    if (!loadedParalogs) {
        return res;
    }

    string a1Str = f.getAnnotationValue(ANNOTATION_GENE_1, "");
    string a2Str = f.getAnnotationValue(ANNOTATION_GENE_2, "");

    if (a1Str.length() > 0 && a2Str.length() > 0) {
        GenomicAnnotation a1 = GenomicAnnotation::parseAnnotation(a1Str);
        GenomicAnnotation a2 = GenomicAnnotation::parseAnnotation(a2Str);

        foreach_ (const RegionDesc& desc1, a1.features) {
            foreach_ (const RegionDesc& desc2, a2.features) {
                string key = desc1.transcriptName < desc2.transcriptName ? desc1.transcriptName + "|" + desc2.transcriptName
                                                                         : desc2.transcriptName + "|" + desc1.transcriptName;
                if (paralogs.count(key) > 0) {
                    //cerr << "BINGO! " << key << endl;
                    res.passed = false;
                    break;
                }
            }
        }
    }

    return res;
}




FusionFilter::Result SplitPositionFilter::process(Fusion &fusion)
{
    Result res;

    bool ok = false;
    string annStr = fusion.getAnnotationValue(ANNOTATION_SPLIT_POS_STATS);
    if (annStr.size() > 0 ) {
        if (fusion.getNumLocalAlignments() >= 3) {
            vector<string> items = StringUtils::split(annStr, ';');
            float meanSplitPos = StringUtils::parseNumber<float>(items.at(0), ok);
            float splitPosStd = StringUtils::parseNumber<float>(items.at(1),ok);
            if (ok ) {
                res.passed = meanSplitPos >= lowerBound && meanSplitPos <= upperBound && splitPosStd > 0;
            }
        }
    }

    return res;

}



FusionFilter::Result HomogeneityFilter::process(Fusion &fusion)
{
    Result res;

    string annStr = fusion.getAnnotationValue(ANNOTATION_METACLUSTER_PROPS);
    MetaClusterAnnotation props;
    MetaClusterAnnotation::fromStr( annStr, props);
    if (annStr.size() > 0 ) {
        res.passed = (props.w1 >= minHomogeneityWeight) && (props.w2 >= minHomogeneityWeight) && (props.w1 >= requiredHomogeneityWeight || props.w2 >= requiredHomogeneityWeight);
    }

    return res;

}



FusionFilter::Result CoverageContextFilter::process(Fusion &fusion)
{
    Result res;

    string annStr = fusion.getAnnotationValue(ANNOTATION_FUSION_COVERAGE);
    CoverageAnnotation covProps;
    CoverageAnnotation::fromStr( annStr, covProps);

    if (annStr.size() > 0 ) {
        //int numSupportingReads = int (fusion.getNumSupporters()*minBckgCoverage + 0.5);
        //float strangeVal = fusion.getNumSupporters()*minBckgCoverage;
        //std::cerr << strangeVal << std::endl;
        res.passed = covProps.fusionCoveringReads[0] >= minBckgReads && covProps.fusionCoveringReads[1] >= minBckgReads;
    }

    return res;

}
