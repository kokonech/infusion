#ifndef CLEANUP_FUSION_CLUSTERS_H
#define CLEANUP_FUSION_CLUSTERS_H


#include "Fusion.h"

struct CleanupFusionClustersConfig {
    string inputPath, outputPath;
    int maxDistToBreakpoint;
    CleanupFusionClustersConfig() : maxDistToBreakpoint(5) {}

};

class CleanupFusionClustersTask
{
    vector<Fusion> fusions;
    CleanupFusionClustersConfig cfg;
    void cleanupDiscordantRescuedReads();
public:
    CleanupFusionClustersTask(CleanupFusionClustersConfig& config) : cfg(config) {}
    int run();
};

#endif // CLEANUP_FUSION_CLUSTERS_H
