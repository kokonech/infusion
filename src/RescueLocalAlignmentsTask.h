#ifndef RESCUE_LOCAL_ALIGNMENTS_TASK_H
#define RESCUE_LOCAL_ALIGNMENTS_TASK_H

#include <boost/thread/mutex.hpp>

#include "seqan/sequence.h"
#include "QueryAlignment.h"
#include "BreakpointCandidate.h"
#include "Cluster.h"
#include "GenomicOverlapDetector.h"

struct ReadLocalAlignment {
    QueryAlignment aln;
    seqan::CharString seq;
    seqan::CharString readName;
    bool multiMapped;
};


struct FindSupportingReadsConfig {
    int minDistanceToBreakpoint;
    int numMismatches;
    int maxMultiMappedAssignments;
    int additionalReserveSize;
    int chunkOfReadsSize;
    int numCores;
    int maxAdditionalClippedRegionSize;
    int minAnchorSize;
    int matchBonus,mismatchCost;
    bool firstMates,debugOutput;
    FindSupportingReadsConfig() : minDistanceToBreakpoint(5), numMismatches(1), maxMultiMappedAssignments(10),
        additionalReserveSize(100), chunkOfReadsSize(2000000), numCores(1), maxAdditionalClippedRegionSize(3),
        minAnchorSize(3), matchBonus(2), mismatchCost(-4), firstMates(true),debugOutput(false) {}
    void print() const {
        std::cerr << "[FindSupporters config] minDistToBreakpoint=" << minDistanceToBreakpoint <<
                "; numMismatches=" << numMismatches <<
                "; maxMultimappedAssignments=" << maxMultiMappedAssignments
                << std::endl;
    }
};


struct RescueAlignmentsTaskResult {
    BreakpointCandidatePtr bp;
    ClusterPtr sourceCluster;
    ClusterPtr partnerCluster;
    int sourceIdx, queryIdx;
};

typedef vector<RescueAlignmentsTaskResult> ResultStore;


class RescueLocalAlignmentsTask
{
   const vector<ReadLocalAlignment>& localAlns;
   GenomicOverlapDetector<ClusterPtr>& overlapDetector;
   FindSupportingReadsConfig cfg;
   ResultStore* resultStore;
   int startIdx, endIdx;

   static boost::mutex guard;
   static std::vector<ClusterSegmentPtr> pickUniqueSegments(const std::vector<ClusterSegmentPtr>& segments);
   static BreakpointCandidatePtr testForBreakpoint(const ReadLocalAlignment& aln, const ClusterPtr& targetCluster, const ClusterPtr& sourceCluster,
                                                    int queryIndex, const FindSupportingReadsConfig& cfg);

public:
    RescueLocalAlignmentsTask(const vector<ReadLocalAlignment>& localAlignments,
                              GenomicOverlapDetector<ClusterPtr>& overlapDetector,
                              const FindSupportingReadsConfig& c, int start, int end,
                              ResultStore* resultStore);

    void run();
    void operator ()() { run(); }
};

#endif //RESCUE_LOCAL_ALIGNMENTS_TASK_H
