#ifndef ANALYZE_CLUSTER_HOMOGENEITY_TASK_H
#define ANALYZE_CLUSTER_HOMOGENEITY_TASK_H

#include <boost/shared_ptr.hpp>

#include "Global.h"
#include "Fusion.h"
#include "ClusterTree.h"


struct AnalyzeClusterHomogeneityTaskConfig {
    string inPath, outPath, reportPath;
    int binSize;
    AnalyzeClusterHomogeneityTaskConfig() : binSize(10) {}

};

struct MetaClusterAnnotation {
    int idx1, idx2;
    float w1, w2;
    MetaClusterAnnotation() : idx1(0), idx2(0), w1(0), w2(0) {}
    static string toStr(MetaClusterAnnotation& ann);
    static bool fromStr(const string& str, MetaClusterAnnotation& ann);
};

class FusionRegion {
    Fusion& fusion;
    int intervalIdx;
public:
    FusionRegion(Fusion& f, int idx) : fusion(f), intervalIdx(idx)  {

    }

    const FusionInterval& getInterval() const { return fusion.getEncompassingInterval(intervalIdx); }
    const Fusion& getFusion() const { return fusion; }
    int getIntervalIndex() const { return intervalIdx; }

    void setMetaClusterProperties( int index, float weight);

};

typedef  boost::shared_ptr<FusionRegion> FusionRegionPtr;
typedef  boost::shared_ptr<ClusterTree<FusionRegionPtr> > ClusterTreePtr;

class AnalyzeClusterHomogeneityTask
{
    map<string,ClusterTreePtr> clusterTreeMap;
    vector<Fusion> fusions;
    AnalyzeClusterHomogeneityTaskConfig cfg;
    void addFusionRegion( Fusion& f, int idx );
public:
    AnalyzeClusterHomogeneityTask( const AnalyzeClusterHomogeneityTaskConfig& config) : cfg(config) {}
    int run();
};

#endif // ANALYZE_CLUSTER_HOMOGENEITY_TASK_H
