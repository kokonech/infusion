#ifndef RESOLVE_MULTIMAPPED_TASK_H
#define RESOLVE_MULTIMAPPED_TASK_H

#include <boost/scoped_ptr.hpp>
#include <map>

#include "Fusion.h"

using std::string;

struct ResolveMultimappedConfig {
    string input, output, pathToFusionSeqs;
    float splitBpWeight, bridgeBpWeight;
    ResolveMultimappedConfig() : splitBpWeight(1.0), bridgeBpWeight(1.0) {}

};

struct MultimappedBreakpoint {
    BreakpointCandidatePtr bp;
    int fusionIdx;
    MultimappedBreakpoint(BreakpointCandidatePtr bpc, int idx) : bp(bpc), fusionIdx(idx) {}
};

typedef std::vector<MultimappedBreakpoint> MultimappedBreakpointList;
typedef std::map<string, MultimappedBreakpointList > MultimappedBreakpointDict;

class MultimappedReadsResolultionStrategy {
public:
    virtual void resolve(vector<Fusion>& fusions, const string& readName, const MultimappedBreakpointList& multimappedBreakpoints ) = 0;
};

/* This strategy uses size of the cluster to resolve multi mapping */

class ContextBasedStrategy : public MultimappedReadsResolultionStrategy {
public:
    ContextBasedStrategy() {}
    virtual void resolve(vector<Fusion>& fusions, const string& readName, const MultimappedBreakpointList& multimappedBreakpoints );
};

class ResolveMultimappedTask
{
    boost::scoped_ptr<MultimappedReadsResolultionStrategy> resolutionStrategy;
    ResolveMultimappedConfig cfg;
public:
    ResolveMultimappedTask(const ResolveMultimappedConfig& config) : resolutionStrategy(new ContextBasedStrategy()), cfg(config) {}
    int performAnalysis();
};

#endif // RESOLVE_MULTIMAPPED_TASK_H
