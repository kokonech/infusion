#include "seqan/bam_io.h"
#include "StringUtils.h"
#include "LocalAlignmentStore.h"
#include "AnalyzeFusionCoverageTask.h"


using namespace std;

bool AnalyzeFusionCoverageTask::loadCoverageData()
{
    string line;

    std::ifstream infile;

    infile.open(cfg.inputCoverageData);
    if ( !infile.is_open())  {
        return false;
    }


    while (getline(infile,line)) {
        if (StringUtils::trim(line).length() == 0 || line[0] == '#' ) {
            continue;
        }
        vector<string> items = StringUtils::split(line, '\t');
        if (items.size() < 4) {
            continue;
        }

        const string& refName = items.at(0);
        bool ok = false;
        int pos = StringUtils::parseNumber<int>(items[1], ok);
        if (!ok) {
            continue;
        }
        int cov = StringUtils::parseNumber<int>(items[3], ok);
        if (!ok) {
            continue;
        }

        coverageArrayMap[refName].setCoverage(pos, cov);


    }



    return true;
}

bool AnalyzeFusionCoverageTask::outputFusionCoveringReads()
{
    cerr << "Save fusion covering reads as annotations" << endl;

    ofstream outReport;

    if (cfg.coveringReadsReport.size() > 0) {

        outReport.open(cfg.coveringReadsReport, ios_base::out);

        if (!outReport.is_open()) {
            cerr << "Failed to open report file for writing" << endl;
            return false;
        }

        outReport << "#fusion_id\tinterval_idx\tstart\tend\tnum_covering_reads\n";
    }

    for (auto iter = intervalTreeMap.begin(), endIter = intervalTreeMap.end(); iter != endIter; ++iter ) {

        const IntervalTree<FusionBpRegionPtr>& segmentITree = iter->second;

        auto node = segmentITree.leftMostNode();

        while (node != NULL) {
            FusionBpRegionPtr curSegment = node->getValue();

            Fusion& f = curSegment->getFusion();
            int ivIdx = curSegment->getIntervalIdx();

            CoverageAnnotation ann;
            string annStr = f.getAnnotationValue(ANNOTATION_FUSION_COVERAGE);
            CoverageAnnotation::fromStr(annStr,ann);
            ann.fusionCoveringReads[ivIdx] = curSegment->getNumCoveringReads();
            annStr = CoverageAnnotation::toStr(ann);
            f.setAnnotationValue(ANNOTATION_FUSION_COVERAGE, annStr);

            if (cfg.coveringReadsReport.size() > 0) {
                outReport << f.getIndex() << "\t" << curSegment->getIntervalIdx() << "\t";
                outReport << node->getStart() << "\t" << node->getEnd() << "\t";
                outReport << curSegment->getNumCoveringReads();
                if (curSegment->getIntersectingReadNames().size() > 0) {
                    outReport << "\t" << StringUtils::join(curSegment->getIntersectingReadNames(), ",");
                }
                outReport << "\n";
            }

            node = segmentITree.stepForward(node);
        }

    }

    return true;

}

static vector<int> createBuffer(int limit) {
    vector<int> x;
    x.resize(limit);
    return x;
}

static void computeStats(const vector<int>& vals, double& mean) {
    mean = 0;
    for (size_t i = 0; i < vals.size(); ++i) {
        mean += vals[i];
    }

    mean /= vals.size();
}

void AnalyzeFusionCoverageTask::reportFusionCoverage(ofstream& outReport, const Fusion& fusion, int idx) {

    static vector<int> buf = createBuffer(cfg.limit);

    const FusionInterval& iv = fusion.getEncompassingInterval(idx);

    ostringstream ss;

    for (int i = 0; i < cfg.limit; ++i) {
        int pos = iv.breakpointPos - i;
        int val = coverageArrayMap[iv.refId].getCoverage(pos);
        buf[i] = val;
        ss << buf[i] << " ";
    }
    double mean1 = 0;
    computeStats(buf, mean1);

    ss << Cluster::directionToChar(iv.direction) << " ";
    for (int i = 0; i < cfg.limit; ++i) {
        int pos = iv.breakpointPos + i + 1;
        int val = coverageArrayMap[iv.refId].getCoverage(pos);
        buf[i] = val;
        ss << val << " ";
    }
    double mean2 = 0;
    computeStats(buf, mean2);
    outReport << fusion.getIndex() << "\t";
    outReport << mean1 << Cluster::directionToChar(iv.direction) << mean2 << "\t";
    outReport << ss.str() << endl;

}

bool AnalyzeFusionCoverageTask::createFusionCoverageReport()
{
    if (!loadCoverageData()) {
        cerr << "Failed to load coverage data from " << cfg.inputCoverageData << endl;
        return false;
    }


    cerr << "Loaded coverage data" << endl;

    ofstream outReport;

    outReport.open(cfg.outputReport, ios_base::out);

    if (!outReport.is_open()) {
        cerr << "Failed to open report file for writing" << endl;
        return false;
    }

    outReport << "#id\tmean_coverage\tvalues\n";

    foreach_ (const Fusion& f, fusions ) {
        reportFusionCoverage(outReport, f, 0);
        reportFusionCoverage(outReport, f, 1);
    }

    cerr << "Created fusion coverage report" << endl;

    return true;
}


static inline void recordToIntervals(const seqan::BamAlignmentRecord& r, vector<SimpleInterval>& intervals) {

    using namespace seqan;

    int numElems = length(r.cigar);
    int startPos = r.beginPos;
    int size = 0;

    for (int i = 0; i < numElems; i++) {
        const CigarElement<>& elem = r.cigar[i];
        if (elem.operation == 'M' || elem.operation == 'I' ) {
            size += elem.count;
        } else if (elem.operation == 'N') {
            intervals.push_back( SimpleInterval(startPos,startPos + size - 1));
            startPos = startPos + size + elem.count;
            size = 0;
        }
    }
    if (size > 0) {
        intervals.push_back( SimpleInterval(startPos, startPos + size - 1));
    }

}

bool AnalyzeFusionCoverageTask::processCoverageDownstreamFusionBreakpoint()
{
    using seqan::BamStream;
    using seqan::BamAlignmentRecord;

    createBpRegionsIntervalTree();

    cerr << "Counting reads covering region downstream of fusion breakpoint" << endl;

    BamStream bamStream(cfg.inputBamFile.c_str(), BamStream::READ, BamStream::BAM);

    if (!isGood(bamStream)) {
        cerr << "Failed to open BAM file " << cfg.inputBamFile << endl;
        return false;
    }

    vector<string> chrNames;
    StringUtils::convertStringSetToVector(bamStream._nameStore, chrNames);

    while (!atEnd(bamStream)) {

        BamAlignmentRecord r;
        if (readRecord(r, bamStream) != 0)
        {
            std::cerr << "Could not read alignment from " << cfg.inputBamFile << std::endl;
            return false;
        }

        if (hasFlagUnmapped(r)) {
            continue;
        }

        /*if (r.qName == "HWI-ST863:228:C211TACXX:6:2106:6089:17609" ) {
            cerr << "It's me: " << r.qName << endl;
        }*/

        const string& refName = chrNames[r.rID];
        IntervalTree<FusionBpRegionPtr>& iTree = intervalTreeMap[refName];

        vector<SimpleInterval> intervals;
        recordToIntervals(r, intervals);

        bool ok = false;
        int fusionIdx = LocalAlignmentStore::getIntTagValue(r, "XF", ok);

        foreach_ (SimpleInterval& iv, intervals) {
            vector<FusionBpRegionPtr> results;
            iTree.findOverlapers(results, iv.begin, iv.end);
            foreach_ (FusionBpRegionPtr bpRegion, results) {
                // TODO: check if intersects breakpoint
                if (fusionIdx == 0) {
                    bpRegion->incNumCoveringReads();
#ifdef DEBUG
                    bpRegion->addIntersectingRead(toCString(r.qName));
#endif
                }
            }
        }

    }

    if (!outputFusionCoveringReads()) {
        return false;
    }


    if (!Fusion::writeFusionClusters(fusions, cfg.outputFusionsPath)) {
        return false;
    }

    return true;
}


void AnalyzeFusionCoverageTask::addRegionToIntervalTree(Fusion &f, int intervalIndex)
{

    const FusionInterval& iv = f.getEncompassingInterval(intervalIndex);

    IntervalTree<FusionBpRegionPtr>& iTree = intervalTreeMap[iv.refId];
    FusionBpRegionPtr bpRegionPtr(new FusionBpRegion(f, intervalIndex));

    if (iv.direction == Cluster::DirectionLeft) {
        iTree.insertInterval(iv.breakpointPos - cfg.downstreamRegionSize, iv.breakpointPos, bpRegionPtr);
    } else {
        iTree.insertInterval(iv.breakpointPos, iv.breakpointPos + cfg.downstreamRegionSize, bpRegionPtr);
    }




}


void AnalyzeFusionCoverageTask::createBpRegionsIntervalTree()
{

    foreach_ (Fusion& f, fusions) {
        addRegionToIntervalTree(f, 0);
        addRegionToIntervalTree(f, 1);
    }

}


int AnalyzeFusionCoverageTask::run()
{

    if (!Fusion::readFusionClusters(fusions, cfg.inputFusionsPath)) {
        cerr << "Failed to read fusion from " << cfg.inputFusionsPath << endl;
        return -1;
    }

    cerr << "Loaded fusions" << endl;


    if (!processCoverageDownstreamFusionBreakpoint()) {
        return -1;
    }

    if (cfg.inputCoverageData.size() > 0 && cfg.outputReport.size() > 0) {
        if (!createFusionCoverageReport()) {
            return -1;
        }

    }

    return 0;

}


string CoverageAnnotation::toStr(CoverageAnnotation &ann)
{
    stringstream ss;
    ss << ann.fusionCoveringReads[0] << ";";
    ss << ann.fusionCoveringReads[1];

    return ss.str();
}

bool CoverageAnnotation::fromStr(const string &str, CoverageAnnotation &ann)
{
    if (str.size() == 0) {
        return false;
    }

    vector<string> items = StringUtils::split(str, ';');
    if (!items.size() == 2) {
        return false;
    }

    bool ok = true;
    ann.fusionCoveringReads[0] = StringUtils::parseNumber<int>(items[0], ok);
    ann.fusionCoveringReads[1] = StringUtils::parseNumber<int>(items[1], ok);

    return ok;

}
