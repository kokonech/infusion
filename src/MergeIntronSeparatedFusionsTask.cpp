#include "MergeIntronSeparatedFusionsTask.h"


inline static void checkSeparatedByIntron(const Fusion& f, const BreakpointCandidatePtr& bp, int idx, int meanInsertSize) {
    const QueryAlignment& aln = bp->alns[idx];
    const FusionInterval& iv = f.getEncompassingInterval(idx);
    GenomicInterval alnIv(aln.refId, aln.refStart, aln.inRefEnd());

    if (iv.direction == Cluster::DirectionRight) {
        alnIv.begin -= meanInsertSize;
    } else {
        alnIv.end += meanInsertSize;
    }

    if ( !iv.intersects(alnIv) ) {
        bp->setSeparatedByIntron(idx);
    }

}

void MergeIntronSeparatedFusionsTask::joinFusionsSeparatedByIntron(FusionSegmentPtr& destFusionSeg, FusionSegmentPtr& toJoinFusionSeg) {

    Fusion& destFusion = destFusionSeg->getFusion();
    Fusion& joinedFusion = toJoinFusionSeg->getFusion();

    bool swapAlignments = ( destFusionSeg->getIntervalIdx() != toJoinFusionSeg->getIntervalIdx() );

    assert(destFusion.getIndex() != joinedFusion.getIndex());

    if (destFusion.getIndex() == joinedFusion.getIndex()) {
        return;
    }

    const vector<BreakpointCandidatePtr> toJoinList = joinedFusion.getBreakpoints();

    foreach_ (const BreakpointCandidatePtr bp, toJoinList) {
        if (swapAlignments) {
            std::swap(bp->alns[0],bp->alns[1]);
        }

        checkSeparatedByIntron(destFusion, bp, 0, config.meanInsertSize);
        checkSeparatedByIntron(destFusion,bp,1, config.meanInsertSize);
        destFusion.addBreakpoint(bp);
    }

    // TODO: maybe it makes sense to update intervals?
    destFusion.updateIntervals(false);
    joinedFusion.clear();

}

bool MergeIntronSeparatedFusionsTask::canBeMerged(const FusionSegmentPtr &curSegment, const FusionSegmentPtr &nextSegment)
{
    return ( curSegment->getDirection() == nextSegment->getDirection() ) && haveCompatiblePartners(curSegment, nextSegment);
}

inline bool MergeIntronSeparatedFusionsTask::segmentsWithinMaxIntronLength(const FusionSegmentPtr &prevSegment, const FusionSegmentPtr &curSegment)
{

    const GenomicInterval& iv1 = prevSegment->getEncompassingInterval();
    const GenomicInterval& iv2 = curSegment->getEncompassingInterval();

    return intervalsWithinMaxIntronLength(iv1,iv2);
}

inline bool MergeIntronSeparatedFusionsTask::intervalsWithinMaxIntronLength(const GenomicInterval &iv1, const GenomicInterval &iv2)
{
    return ( iv1.end + config.maxIntronLength > iv2.begin );
}

bool MergeIntronSeparatedFusionsTask::haveCompatiblePartners(const FusionSegmentPtr &prevSegment, const FusionSegmentPtr &curSegment)
{
    const FusionSegmentPtr& p1 = prevSegment->getPartner();
    const FusionSegmentPtr& p2 = curSegment->getPartner();

    // TODO: improve condition (within intron length with respect to fusion segment direction)
    if ( (p1->getDirection() == p2->getDirection()) ) {

        FusionSegmentPtr destSegment, joiningSegment;
        if ( p1->getFusion().getNumLocalAlignments() > 0 ) {
            destSegment = p1;
            joiningSegment = p2;
        } else {
            assert(p2->getFusion().getNumLocalAlignments() > 0 );
            destSegment = p2;
            joiningSegment = p1;
        }

        if (p1->getDirection() == Cluster::DirectionLeft) {

            if (destSegment->getBreakpointPos() < joiningSegment->getBreakpointPos() ) {
                return false;
            }

            return intervalsWithinMaxIntronLength(joiningSegment->getEncompassingInterval(), destSegment->getEncompassingInterval());

        } else {
            if (destSegment->getBreakpointPos() > joiningSegment->getBreakpointPos()) {
                return false;
            }

            return intervalsWithinMaxIntronLength(destSegment->getEncompassingInterval(), joiningSegment->getEncompassingInterval());

        }
    }

    return false;
}


void MergeIntronSeparatedFusionsTask::mergeIntronSepartedFusions()
{

    const std::map<std::string,IntervalTree<FusionSegmentPtr> > & intervalMap = fusionOverlapDetector.getIntervalMap();

    for (auto iter = intervalMap.begin(), endIter = intervalMap.end(); iter != endIter; ++iter ) {
        const IntervalTree<FusionSegmentPtr>& segmentITree = iter->second;

        auto node = segmentITree.leftMostNode();

        while (node != NULL) {
            FusionSegmentPtr curSegment = node->getValue();
            const Fusion& f = curSegment->getFusion();
            //cerr << "Looking at cluster: " << curCluster << endl;
            if (f.getNumLocalAlignments() == 0 && f.getNumSupporters() > 0) {
                if (curSegment->getDirection() == Cluster::DirectionLeft) {
                    auto nextNode = segmentITree.stepForward(node);

                    while (nextNode != NULL ) {
                        FusionSegmentPtr nextSegment = nextNode->getValue();
                        if (nextSegment->getFusion().getNumLocalAlignments() > 0) {
                            if ( segmentsWithinMaxIntronLength(curSegment,nextSegment) ) {
                                if ( canBeMerged(curSegment, nextSegment) ) {
                                    joinFusionsSeparatedByIntron( nextSegment, curSegment );
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        nextNode = segmentITree.stepForward(nextNode);
                    }

                } else {
                    assert(curSegment->getDirection() == Cluster::DirectionRight);
                    auto prevNode = segmentITree.stepBack(node);
                    while (prevNode != NULL ) {
                        FusionSegmentPtr prevSegment = prevNode->getValue();
                        if (prevSegment->getFusion().getNumLocalAlignments() > 0) {
                            if ( segmentsWithinMaxIntronLength(prevSegment,curSegment) ) {
                                if ( canBeMerged(curSegment, prevSegment) ) {
                                    joinFusionsSeparatedByIntron( prevSegment,curSegment );
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        prevNode = segmentITree.stepBack(prevNode);
                    }

                }

            }
            node = segmentITree.stepForward(node);
        }

    }

}

void MergeIntronSeparatedFusionsTask::createFusionOverlapDetector()
{
    fusionOverlapDetector.clear();

    foreach_ (Fusion& f, fusions) {

        if ( f.hasAnnotation(ANNOTATION_HOMOLOGOUS_GENES) ) {
            continue;
        }

        FusionSegmentPtr fs1(new FusionSegment(f,0));
        const FusionInterval& iv1 = f.getEncompassingInterval(0);
        fusionOverlapDetector.insertInterval(iv1.refId, iv1.begin, iv1.end, fs1);

        FusionSegmentPtr fs2(new FusionSegment(f,1));
        const FusionInterval& iv2 = f.getEncompassingInterval(1);
        fusionOverlapDetector.insertInterval(iv2.refId, iv2.begin, iv2.end, fs2);

        fs1->setPartner(fs2);
        fs2->setPartner(fs1);

    }
}

int MergeIntronSeparatedFusionsTask::run()
{
    if (!Fusion::readFusionClusters(fusions, config.inputPath)) {
        return -1;
    }

    createFusionOverlapDetector();

    mergeIntronSepartedFusions();

    if (!Fusion::writeFusionClusters(fusions, config.outputPath, true))  {
        return -1;
    }

    return 0;


}



