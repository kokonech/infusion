#include <fstream>

#include "BreakpointCandidate.h"

#include "StringUtils.h"
#include "MiscUtils.h"

using namespace std;

const std::string BreakpointCandidate::ORIGIN_LOCAL("local");
const std::string BreakpointCandidate::ORIGIN_LOCAL_MATE_1("local_1");
const std::string BreakpointCandidate::ORIGIN_LOCAL_MATE_2("local_2");
const std::string BreakpointCandidate::ORIGIN_PAIRED("paired");
const std::string BreakpointCandidate::ORIGIN_LOCAL_REDEEMED("local_redeemed");
const std::string BreakpointCandidate::ORIGIN_LOCAL_REDEEMED_MATE_1("local_redeemed_1");
const std::string BreakpointCandidate::ORIGIN_LOCAL_REDEEMED_MATE_2("local_redeemed_2");
const std::string BreakpointCandidate::ORIGIN_UNKNOWN("unknown");
const std::string BreakpointCandidate::TAG_ALN_SCORE("AS");

void BreakpointCandidate::writeCandidate(const BreakpointCandidatePtr& c, ostream& outfile )
{
    outfile << c->readName;
    for ( int j = 0; j < 2; ++j) {
        outfile << "\t" << c->alns[j].refId << "\t" << c->alns[j].refStart << "\t" << strandToChar(c->alns[j].positiveStrand)
                << "\t" << c->alns[j].queryStart  << "\t" << c->alns[j].length;
    }
    outfile << "\t" << c->flags;
    if (c->alns[0].score > 0 && c->alns[1].score > 0) {
        outfile << "\tAS:" << c->alns[0].score << ":" << c->alns[1].score;
    }
    outfile << endl;
}

void BreakpointCandidate::sortAlignmentsAccordingToInterval(const GenomicInterval &firstInterval)
{
       assert(alns.size() >= 2);
       const QueryAlignment& aln = alns.front();
       GenomicInterval alnIv(aln.refId, aln.refStart, aln.inRefEnd());

       if (isSeparatedByIntron()) {
           bool swapAlns = false;
           if (alns[0].refId == alns[1].refId) {
               int dist0 = abs(alns[0].refStart - firstInterval.begin);
               int dist1 = abs(alns[1].refStart - firstInterval.begin);
               swapAlns = dist0 > dist1;
           } else {
               swapAlns = alns[0].refId != firstInterval.refId;
           }

           if (swapAlns) {
               std::swap(alns[0],alns[1]);
               if ( (flags & BreakpointFlags_SeparatedByIntron) != BreakpointFlags_SeparatedByIntron ) {
                   if (isFirstSeparatedByIntron()) {
                       flags = BreakpointFlags_SecondSeparatedByIntron | (flags & ~BreakpointFlags_FirstSeparatedByIntron) ;
                       assert(isSecondSeparatedByIntron() && !isFirstSeparatedByIntron());
                   } else if (isSecondSeparatedByIntron()) {
                       flags = BreakpointFlags_FirstSeparatedByIntron | (flags & ~BreakpointFlags_SecondSeparatedByIntron) ;
                       assert(!isSecondSeparatedByIntron() && isFirstSeparatedByIntron());
                   }
               }
           }

       } else if (!firstInterval.intersects(alnIv) ) {
           std::swap(alns[0],alns[1]);

       }



}


bool BreakpointCandidate::readCandidates(vector<BreakpointCandidatePtr> &candidates, const char *path)
{

    int count = 0;
    ifstream infile;

    infile.open(path);

    if (!infile.is_open()) {
        return false;
    }

    while (infile.good()) {
        string line;
        getline(infile,line);
        ++count;
        if (StringUtils::trim(line).length() == 0 || line[0] == '#' ) {
            continue;
        }

        BreakpointCandidatePtr bp = parseCandidateRecord(line);
        if ( bp ) {
            //std::sort(bp->alns.begin(), bp->alns.end());
            candidates.push_back(bp);
        } else {
            cerr << "Failed to parse line " << count << " from " << path << endl;
        }

    }


    return true;

}

void BreakpointCandidate::writeCandidates(const vector<BreakpointCandidatePtr> &candidates, const char *path)
{
        ofstream outfile;

        outfile.open(path, ios_base::out);

        // header
        outfile << "#name\tseq1\tpos1\tstrand1\tlocal_pos1\tlength1\tseq2\tpos2\tstrand2\tlocal_pos2\tlength2\tflag\tprops";
        outfile << endl;

        // contents
        for (size_t i = 0, count = candidates.size(); i < count; ++i ) {
            const BreakpointCandidatePtr& c = candidates.at(i);
            writeCandidate(c, outfile);
        }

}

BreakpointCandidatePtr BreakpointCandidate::parseCandidateRecord(const string &line)
{
    BreakpointCandidatePtr bp;

    vector<string> items = StringUtils::split(line,'\t');

    if (items.size() < 12) {
        assert(0);
        return bp;
    }

    string readName = items[0];

    vector<QueryAlignment> alns;

    for (int i = 0; i < 2; ++i) {
        QueryAlignment aln;
        aln.refId = items[5*i + 1];
        bool ok = true;
        aln.refStart = StringUtils::parseNumber<int>(items[5*i + 2], ok); // 0-based!
        if (!ok) {
            return bp;
        }
        aln.positiveStrand =  items[5*i + 3] == "+";
        aln.queryStart = StringUtils::parseNumber<int>(items[5*i + 4], ok); // 0-based!
        if (!ok) {
            return bp;
        }
        aln.length = StringUtils::parseNumber<int>(items[5*i + 5], ok);
        if (!ok) {
            return bp;
        }

        alns.push_back( aln );
    }

    bool ok = true;
    uint flags = StringUtils::parseNumber<uint>(items[11], ok);
    if (!ok) {
        return bp;
    }

    if (items.size() == 13) {
        bool ok = true;
        vector<string> tagList = StringUtils::split(items[12], ';');
        foreach_ (const string& tagRecord, tagList) {
            vector<string> tagItems = StringUtils::split(tagRecord, ':');
            if (tagItems[0] == "AS") {
                assert( tagItems.size() == 3 );
                alns[0].score = StringUtils::parseNumber<int>(tagItems[1], ok);
                assert(ok);
                alns[1].score = StringUtils::parseNumber<int>(tagItems[2], ok);
                assert(ok);
            }
        }
    }

    bp.reset ( new BreakpointCandidate() );
    bp->readName = readName;
    bp->alns = alns;
    bp->flags = flags;

    return bp;
}


bool BreakpointCandidate::passConstraints(const QueryAlignment& aln1, const QueryAlignment& aln2,const BreakpointConstraints &bc, bool& splicing)
{

    // Checking intersection size of alignments in read or distance between them

    if (bc.alignmentsCanIntersectLocally) {
        int leftStart = aln1.queryStart, leftLen = aln1.length,  rightStart = aln2.queryStart;

        if (aln1.queryStart > aln2.queryStart) {
            leftStart = aln2.queryStart;
            leftLen = aln2.length;
            rightStart = aln1.queryStart;
        }

        int diffInRead = rightStart - (leftStart + leftLen);
        //cout << "Distance between alignments in read: " << diffInRead << endl;

        if ( diffInRead > bc.maxDistBetweenAlignInRead )   {
            //cout << "Local alignments don't pass the tolerance, skipping this candidate" << endl;
            return false;
        }

        if ( diffInRead < - ( bc.maxIntersectionSizeInRead) ) {
            return false;
        }
    }

    if ( MiscUtils::contains(bc.ignoredSequences, aln1.refId) || MiscUtils::contains( bc.ignoredSequences, aln2.refId) ) {
        return false;
    }

    // Checking disatnce between alignments in reference
    if (aln1.refId == aln2.refId) {

        // Condition similar to local alignments

        int leftInRefStart = aln1.refStart, leftInRefLen = aln1.length,  rightInRefStart = aln2.refStart;

        if (aln1.refStart > aln2.refStart) {
            leftInRefStart = aln2.refStart;
            leftInRefLen = aln2.length;
            rightInRefStart = aln1.refStart;
        }

        int diffInRef = (rightInRefStart - leftInRefStart) - leftInRefLen;

        //cout << "Distance between alignments in reference: " << diffInRef << endl;
        if (abs(diffInRef) < bc.minDistBetweenAlignInRef) {
            //cout << "Reference alignments don't pass tolerance, skipping this candidate" << endl;
            splicing = true;
            return false;
        }
    }


    return true;
}

