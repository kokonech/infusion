#include <fstream>

#include "StringUtils.h"
#include "AnalyzeClusterHomogeneityTask.h"

using namespace std;


void FusionRegion::setMetaClusterProperties(int index, float weight)
{
    string annVal = fusion.getAnnotationValue(ANNOTATION_METACLUSTER_PROPS);
    MetaClusterAnnotation ann;

    MetaClusterAnnotation::fromStr(annVal, ann);

    if (intervalIdx == 0) {
        ann.idx1 = index;
        ann.w1 = weight;
    } else {
        ann.idx2 = index;
        ann.w2 = weight;
    }

    fusion.setAnnotationValue(ANNOTATION_METACLUSTER_PROPS, MetaClusterAnnotation::toStr(ann));

}


void AnalyzeClusterHomogeneityTask::addFusionRegion(Fusion &f, int idx)
{
    FusionRegionPtr fr( new FusionRegion(f,idx) );
    const GenomicInterval& iv = fr->getInterval();

    if (clusterTreeMap.count(iv.refId) == 0 ) {
        ClusterTreePtr clusterTreePtr( new ClusterTree<FusionRegionPtr>() );
        clusterTreeMap.insert( std::make_pair(iv.refId, clusterTreePtr) );
    }

    ClusterTreePtr clusterTree = clusterTreeMap[iv.refId];

    clusterTree->insertInterval(iv.begin, iv.end, fr);
}

int AnalyzeClusterHomogeneityTask::run()
{

    if (!Fusion::readFusionClusters(fusions, cfg.inPath)) {
        cerr << "Failed to read fusion from " << cfg.inPath << endl;
        return -1;
    }

    cerr << "Loaded fusions" << endl;

    foreach_ (Fusion& f, fusions) {
        addFusionRegion(f, 0);
        addFusionRegion(f, 1);
    }

    cerr << "Created cluster tree" << endl;

    ofstream outReport;

    outReport.open(cfg.reportPath, ios_base::out);

    if (!outReport.is_open()) {
        cerr << "Failed to open report file for writing" << endl;
        return -1;
    }

    outReport << "#id\tchr\tstart\tend\tnum_fusions\tnum_bp\tunique_bp\tfusions\n";
    int metaClusterId = 0;

    for (auto iter = clusterTreeMap.begin(); iter != clusterTreeMap.end(); ++iter) {
        ClusterTreePtr tree = iter->second;

        ClusterTree<FusionRegionPtr>::Node node = tree->leftMostCluster();


        while (!node.isNull()) {

            clusternode* clusterNode = node.getClusterNode();

            vector<FusionRegionPtr> regions;
            node.getIntervalItems(regions);

            int totalWeight = 0, numFusions = 0;
            set<int> uniqueBreakpoints;


            vector<int> bins;
            int numBins = ( clusterNode->end - clusterNode->start + 1 ) / cfg.binSize;
            bins.reserve( numBins );

            foreach_ (const FusionRegionPtr& region, regions) {

                // calcualte homogeneity
                const Fusion& f = region->getFusion();
                totalWeight += f.getNumSupporters();
                numFusions++;

            }

            outReport << ++metaClusterId << "\t" << iter->first << "\t";
            outReport << clusterNode->start << "\t" << clusterNode->end << "\t";
            outReport << numFusions << "\t" << totalWeight << "\t";

            foreach_ (const FusionRegionPtr& region, regions) {
                float weight =region->getFusion().getNumSupporters();
                region->setMetaClusterProperties(metaClusterId, weight / totalWeight );
                outReport << region->getFusion().getIndex() << "-" << region->getIntervalIndex() << ";";
            }

            outReport << endl;

            node.stepForward();

        }


    }

    cerr << "Finished homogeneity analysis" << endl;

    if (!Fusion::writeFusionClusters(fusions, cfg.outPath)) {
        cerr << "Failed to write fusions to " << cfg.outPath << endl;
        return -1;
    }


    return 0;


}


string MetaClusterAnnotation::toStr(MetaClusterAnnotation &ann)
{
    stringstream ss;
    ss << ann.idx1 << ";";
    ss << ann.w1 << ";";
    ss << ann.idx2 << ";";
    ss << ann.w2;

    return ss.str();
}

bool MetaClusterAnnotation::fromStr(const string &str, MetaClusterAnnotation &ann)
{
    if (str.size() == 0) {
        return false;
    }

    vector<string> items = StringUtils::split(str, ';');
    if (!items.size() == 4) {
        return false;
    }

    bool ok = true;
    ann.idx1 = StringUtils::parseNumber<int>(items[0], ok);
    ann.w1 = StringUtils::parseNumber<float>(items[1], ok);
    ann.idx2 = StringUtils::parseNumber<int>(items[2], ok);
    ann.w2 = StringUtils::parseNumber<float>(items[3], ok);

    return ok;


}
