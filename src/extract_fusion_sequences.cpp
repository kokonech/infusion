#include <iostream>
#include <vector>

#include <boost/scoped_ptr.hpp>

#include <seqan/arg_parse.h>

#include "Reference.h"
#include "Fusion.h"
#include "StringUtils.h"
#include "FusionGeneAnnotation.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "extract_fusion_sequences"


bool extractFlankingFusionSequencesFromReference(const vector<Fusion>& fusions, const string& outputPath, const string& pathToReference, int threshold) {

    boost::scoped_ptr<ReferenceSeq> ref( new ReferenceSeqInMemory() );

    if (!ref->load(pathToReference)) {
        cerr << "Failed to load reference from " << pathToReference << endl;
        return false;
    }

    ofstream out;
    out.open(outputPath, ios_base::out);

    if (!out.is_open()) {
        cerr << "Failed to open file for writing " << outputPath << endl;
        return false;
    }

    foreach_ (const Fusion& f, fusions) {

        if (f.isEmpty()) {
            continue;
        }

        for (int i = 0; i < 2; ++i) {
            GenomicInterval iv = f.getEncompassingInterval(i);

            if (f.getDirection(i) == Cluster::DirectionLeft) {
                iv.begin = iv.end;
                iv.end += threshold;
            } else {
                iv.end = iv.begin;
                iv.begin -= threshold;
            }

            CharString seq = ref->getSubSequence(iv.refId, iv.begin, iv.length());

            out << ">" << f.getIndex() << "/" << i + 1 << endl;
            out << seq << endl;

        }
    }

    return false;

}


bool extractFusionSequencesFromReference(const vector<Fusion>& fusions, const string& outputPath, const string& pathToReference, int segLength) {

    boost::scoped_ptr<ReferenceSeq> ref( new ReferenceSeqInMemory() );

    if (!ref->load(pathToReference)) {
        cerr << "Failed to load reference from " << pathToReference << endl;
        return false;
    }

    ofstream out;
    out.open(outputPath, ios_base::out);

    if (!out.is_open()) {
        cerr << "Failed to open file for writing " << outputPath << endl;
        return false;
    }

    foreach_ (const Fusion& f, fusions) {

        if (f.isEmpty()) {
            continue;
        }

        CharString seq[2];

        for (int i = 0; i < 2; ++i) {
            GenomicInterval iv = f.getEncompassingInterval(i);

            if (f.getDirection(i) == Cluster::DirectionRight) {
                iv.end = iv.begin + segLength;
            } else {
                iv.begin = iv.end - segLength;
            }

            seq[i] = ref->getSubSequence(iv.refId, iv.begin, iv.length());

        }

        if (f.getDirection(0) == Cluster::DirectionRight) {
            reverseComplement(seq[0]);
        }

        if (f.getDirection(1) == Cluster::DirectionLeft) {
            reverseComplement(seq[1]);
        }

        string gene1 = Fusion::getGeneStr(f.getAnnotationValue(ANNOTATION_GENE_1));
        string gene2 = Fusion::getGeneStr(f.getAnnotationValue(ANNOTATION_GENE_2));

        out << ">" << f.getIndex() << " size=" << length(seq[0]) + length(seq[1]) << " gene1=" << gene1 << " gene2=" << gene2 << endl;

        out << seq[0]<< "\t" << seq[1] << endl;

    }

    return false;

}


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It takes fusion clusters as input and resolves multimapped reads based on given strategy");

    string usageLine;
    usageLine.append(" [OPTIONS] -i fusionClusters.txt -ofs sequences.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");
    addOption(parser, ArgParseOption( "i", "input", "Path to input fusion clusters. Use - to for standard input.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "r", "reference", "Path to reference. If give than fusion sequence are extracted from reference sequence within a given distance", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "l", "length", "Length of reference segment to be extracted", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "s", "sample-size", "Length of fusion transcript surronding breakpoint", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "o", "output-fusion-seqs", "Path to ouptut fusion sequences in FASTA format.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "ft", "fusion-transcripts", "Output fusion transcript with correct orientation."));
    addOption(parser, ArgParseOption( "d", "insert-separator", "Add separating symbol for between fusion transcript sequences."));
    addOption(parser, ArgParseOption( "gtf", "gene-annotations", "Path to input GTF annotations", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "t", "transcript-seqs", "Path to input transcript sequences in FASTA format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "ne", "num-exonss", "Number of exons to use for fusion transcript reconstruction", ArgParseArgument::INTEGER ));


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}


typedef vector<string> ExonSeqs;
typedef map<string,ExonSeqs> ExonSeqsMap;


bool loadExonSequences(ExonSeqsMap& exonSequences, const string& pathToTranscripts, const AnnotationModel& annotations) {

    CharString id;
    CharString seq;

    SequenceStream seqStream(pathToTranscripts.c_str());

    if (!isGood(seqStream))
    {
        return false;
    }

    while (!atEnd(seqStream))
    {
        if (readRecord(id, seq, seqStream) != 0) {
            std::cerr << "ERROR: Could not read from " << pathToTranscripts;
            return false;
        }


        TranscriptPtr transcript = annotations.getTranscript(toCString(id));
        if (!transcript){
            std::cerr << "Transcript is not found: *" << id <<  "*" << endl;
            return false;
        }
        vector<ExonPtr> exons = transcript->getExons();
        string idStr = transcript->getName();

        size_t pos = 0;

        if (!transcript->isPositiveStranded() ) {
            std::reverse(exons.begin(), exons.end());
        }


        foreach_ (const ExonPtr& exon, exons) {
            CharString exonSeq = infix(seq, pos, pos + exon->length );
            assert (length(exonSeq) == exon->length);
            exonSequences[ idStr ].push_back( toCString(exonSeq) );
            pos += exon->length;
        }



    }

    return true;

}


string getExonicSequence(const Fusion& f, int idx, const GenomicAnnotation& ann, const ExonSeqsMap& exonSequences, int numExons) {

    string res;

    if ( ann.isIntergenic() || ann.isIntronic() ) {
        // TODO: go on with exctration of the reference sequence
        return res;
    }

    int firstExonId = -1, lastExonId = -1;

    foreach_ (const RegionDesc& exonDesc, ann.features ) {

        string seq;

        bool revCompl = false;
        bool ok = true;
        int exonId = StringUtils::parseNumber<int>(exonDesc.exonId, ok) - 1;
        if (!ok) {
            break;
        }

        if (f.getDirection(idx) == Cluster::DirectionLeft) {
            if (exonDesc.transcriptStrand == '+' ) {
                firstExonId = exonId - numExons + 1;
                lastExonId = exonId;
            } else {
                firstExonId = exonId;
                lastExonId = exonId + numExons - 1;
                if (idx == 0) {
                    revCompl = true;
                }
            }
        } else {
            if ( exonDesc.transcriptStrand == '+') {
                firstExonId = exonId;
                lastExonId = exonId + numExons - 1;
            } else {
                firstExonId = exonId - numExons + 1;
                lastExonId = exonId;
                if (idx == 1) {
                    revCompl = true;
                }
            }
        }

        if  (firstExonId < 0) {
            firstExonId = 0;
        }

        const vector<string>& exons = exonSequences.at(exonDesc.transcriptName);

        stringstream hs;
        hs << ">f" << f.getIndex() << "-" << idx+1 << " transcript=" << exonDesc.transcriptName;
        hs << " exons=" << firstExonId+1 << "-" << lastExonId+1;
        if (revCompl) {
            hs << " rev-compl";
        }


        for (size_t i = firstExonId; i <= lastExonId; ++i) {
            if (i < exons.size()) {
                seq += exons.at(i);
            }
        }

        if (revCompl) {
            seqan::reverseComplement(seq);
        }
        if (seq.length() > 0) {
            hs << " size=" <<seq.length() << endl << seq;
            return hs.str();
        }

    }

    return res;


}

bool extractFusionTranscriptsFromExons(const vector<Fusion>& fusions, const string& pathToGtf, const string& pathToTranscripts, int numExons, const string& outPath) {

    AnnotationModel gtfModel;

    if (!gtfModel.loadTranscripts(pathToGtf,false)) {
        cerr << "Failed to load GTF file " << pathToGtf << endl;
        return false;
    }

    map<string,ExonSeqs> exonSequences;

    if (!loadExonSequences(exonSequences, pathToTranscripts, gtfModel)) {
        cerr << "Failed to load exon sequences from " << pathToTranscripts << endl;
        return false;
    }

    ofstream out(outPath);

    if (!out.is_open()) {
        return false;
    }

    foreach_ (const Fusion& f, fusions ) {

        if (f.isEmpty()) {
            continue;
        }

        string annStr1 = f.getAnnotationValue(ANNOTATION_GENE_1);
        string annStr2 = f.getAnnotationValue(ANNOTATION_GENE_2);

        if (annStr1.length() == 0 || annStr2.length() == 0) {
            continue;
        }

        GenomicAnnotation ann1 = GenomicAnnotation::parseAnnotation(annStr1);
        string seq1 = getExonicSequence(f,0,ann1,exonSequences,numExons);
        out << seq1 << endl;

        GenomicAnnotation ann2 = GenomicAnnotation::parseAnnotation(annStr2);
        string seq2 = getExonicSequence(f,1,ann2,exonSequences,numExons);
        out << seq2 << endl;



    }


    return true;



}



int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    string inputPath, outputPath, reference;
    string gtfAnnotations, transcripts;

    bool outputTranscripts = false, insertSeparator = false;
    int refSegmentLength = 500;
    int sampleSize = -1;
    int numExons = 1;

    getOptionValue(inputPath, parser, "i");
    getOptionValue(outputPath, parser, "o");
    getOptionValue(reference, parser, "r");
    getOptionValue(refSegmentLength, parser, "l");
    getOptionValue(sampleSize, parser, "s");
    getOptionValue(outputTranscripts, parser, "ft");
    getOptionValue(insertSeparator, parser, "d");
    getOptionValue(gtfAnnotations, parser, "gtf");
    getOptionValue(transcripts,parser, "t");
    getOptionValue(numExons, parser, "ne");


    cerr << "Options" << endl;
    cerr << "Input file: " << inputPath << endl;
    cerr << "Fusion seqs output file: " << outputPath << endl;

    vector<Fusion> fusions;
    if (!Fusion::readFusionClusters(fusions, inputPath)) {
        cerr << "Failed to read fusion clusters from " << inputPath << endl;
        return -1;
    }

    bool ok = true;
    if (outputTranscripts) {
        if (transcripts.length() > 0) {
            cerr << "Extracting fusion transcripts from exons" << endl;
            ok = extractFusionTranscriptsFromExons(fusions, gtfAnnotations, transcripts, numExons, outputPath);
        } else if (reference.size() > 0) {
            cerr << "Extracting fusion transcripts from reference" << endl;
            ok = extractFusionSequencesFromReference(fusions, outputPath, reference, refSegmentLength);
        } else {
            cerr << "Extracting fusion transcripts from supporting reads" << endl;
            ok = Fusion::writeFusionTranscripts(fusions, outputPath, sampleSize, insertSeparator );
        }
    } else if (reference.size() > 0) {
        cerr << "Extracting flanking fusion sequences from reference" << endl;
        ok = extractFlankingFusionSequencesFromReference(fusions, outputPath, reference, refSegmentLength );
    } else {
        cerr << "Extracting fusion sequences from supporting reads" << endl;
        ok = Fusion::writeFusionSeqsAsFasta(fusions, outputPath, refSegmentLength, true);
    }

    if (ok) {
        return 0;
    } else {
        return -1;
    }

}





