#ifndef CHUNK_BAM_READER_H
#define CHUNK_BAM_READER_H

#include <vector>
#include <seqan/bam_io.h>

using seqan::BamAlignmentRecord;
using seqan::CharString;
using seqan::CigarElement;

struct ChunkBamReaderConfig
{
    ChunkBamReaderConfig()
        : maxChunkSize(1000000), skipUnmapped(true), removeMateId(false), reverseAlignmentStrand(false) {}
    int maxChunkSize;
    bool skipUnmapped;
    bool removeMateId;
    bool reverseAlignmentStrand;
};

class ChunkBamReader
{
    ChunkBamReaderConfig cfg;
    seqan::BamStream bamStreamIn;
    BamAlignmentRecord prevRecord;
public:
    ChunkBamReader(const char* pathToFile, const ChunkBamReaderConfig& config = ChunkBamReaderConfig() );
    bool init();
    bool loadNextChunk(std::vector<BamAlignmentRecord>& result);
    bool isEof();
    const seqan::StringSet<CharString> & getRefNames() { return bamStreamIn._nameStore; }
    static bool readCompletelytAligned(const BamAlignmentRecord& r) {

        auto it = begin(r.cigar);
        auto itEnd = end(r.cigar);
        uint mappedBasesCount = 0;
        while ( it != itEnd ) {

            CigarElement<> c = *it;
            if (c.operation == 'M') {
                mappedBasesCount += c.count;
            }

            ++it;
        }

        return mappedBasesCount == length(r.seq);

    }


};

#endif // CHUNK_BAM_READER_H
