#include <iostream>
#include <fstream>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/mapped_file.hpp>

using namespace std;
using boost::iostreams::mapped_file_source;
using boost::iostreams::stream;

int testMemoryMappedFileReading() {

    string inFilePath("/data/annotations/hg19.ucscRepeats.txt");

    // Memory mapped

    {

        clock_t start = clock();

        stream<mapped_file_source> ifstream(inFilePath);

        if (!ifstream.is_open()) {
            cerr << "Failed to load file " << inFilePath << endl;
            return -1;
        }

        string line;
        int numLines = 0;
        while (getline(ifstream,line)) {
            numLines++;
        }

        ifstream.close();

        double overall = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
        cout << "Num lines: " << numLines << endl;
        cout << "Time taken: " << overall << " sec" << endl;

    }


    // I/O based
    {
        clock_t start = clock();

        ifstream inputStream(inFilePath);

        if (!inputStream.is_open()) {
            cerr << "Failed to load file " << inFilePath << endl;
            return -1;
        }

        string line;
        int numLines = 0;
        while (getline(inputStream,line)) {
            numLines++;
        }

        inputStream.close();

        double overall = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
        cout << "Num lines: " << numLines << endl;
        cout << "Time taken: " << overall << " sec" << endl;

    }


    return 0;


}


int main(int argc, char const ** argv)
{

#ifdef DEBUG

    cout << "Running in DEBUG mode" << endl;

#else
    cout << "Running in RELEASE mode" << endl;

#endif //DEBUG

    return testMemoryMappedFileReading();
}
