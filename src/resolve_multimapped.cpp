#include <iostream>
#include <vector>

#include <seqan/arg_parse.h>

#include "ResolveMultimappedTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "resolve_multimapped"


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It takes fusion clusters as input and resolves multimapped reads based on given strategy");

    string usageLine;
    usageLine.append(" [OPTIONS] -i fusionClusters.txt -o updatedClusters.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");
    addOption(parser, ArgParseOption( "i", "input", "Path to input fusion clusters. Use - to for standard input.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "o", "output", "Path to output updated fusion clusters. Use - to for standard output", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "ofs", "output-fusion-seqs", "Path to ouptut fusion sequences in FASTA format.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "sw", "split-bp-weight", "Split breakpoint candidate weight", ArgParseArgument::DOUBLE ));
    addOption(parser, ArgParseOption( "bw", "bridge-bp-weight", "Bridge breakpoint candidate weight", ArgParseArgument::DOUBLE ));

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    ResolveMultimappedConfig cfg;

    getOptionValue(cfg.input, parser, "i");
    getOptionValue(cfg.output, parser, "o");
    getOptionValue(cfg.splitBpWeight, parser, "sw");
    getOptionValue(cfg.bridgeBpWeight, parser, "bw");
    getOptionValue(cfg.pathToFusionSeqs, parser, "ofs");

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input file: " << cfg.input << endl;
    cerr << "Output file: " << cfg.output << endl;
    cerr << "Fusion seqs output file: " << cfg.pathToFusionSeqs << endl;

#endif

    ResolveMultimappedTask task(cfg);

    return task.performAnalysis();

}




