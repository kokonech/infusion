#ifndef LOCAL_ALIGNMENT_STORE_H
#define LOCAL_ALIGNMENT_STORE_H


#include <ostream>
#include <seqan/sequence.h>
#include <seqan/bam_io.h>
#include <boost/unordered_set.hpp>


#include "QueryAlignment.h"
#include "BreakpointCandidate.h"


using seqan::StringSet;
using seqan::CharString;
using seqan::BamStream;
using seqan::BamHeader;
using seqan::BamAlignmentRecord;

class SplitReadAnalysisHelper {
    const StringSet<CharString>& refNames;
    BamStream alnOutStream;
    bool unusedAlignmentsStreamOk;
    float minScorePercentageForUnused;
    int maxNM;
    int matchBonus;
public:
    SplitReadAnalysisHelper(const StringSet<CharString>& referenceNames, int maxNumMismatches, float minScorePercentage) : refNames(referenceNames) {
        unusedAlignmentsStreamOk = false;
        maxNM = maxNumMismatches;
        minScorePercentageForUnused = minScorePercentage;
        matchBonus = 2;
    }
    ~SplitReadAnalysisHelper();
    const StringSet<CharString>& getRefNames() { return refNames; }
    bool initUnusedAlignmentsStream(const std::string& outUnalignedPath, const BamHeader& header);
    void writeUnusedLocalAlignments(vector<BamAlignmentRecord>& alignments, const vector<int>& indexes, bool readsFromAntisenceStrand, bool multimapped);
};

struct SplitReadAlgoSettings {

    SplitReadAlgoSettings() : minAnchorLength(5), alignmentToleranceInRead(2),
        maxAlignIntersectionInRead(2), minAlignDistanceInRef(10000),
        maxAdditionalClippedRegionSize(5), maxMultimappedCount(0), maxNumMismatches(2),
        maxAlnScorePerBP(2), minScorePercentage(0.5), minBreakpointScorePercentage(0.9),
        antisenceStrand(false), transcriptomeAnalysis(false)
    {

    }
    uint minAnchorLength;
    uint alignmentToleranceInRead;
    uint maxAlignIntersectionInRead;
    uint minAlignDistanceInRef;
    uint maxAdditionalClippedRegionSize;
    uint maxMultimappedCount;
    uint maxNumMismatches;
    uint maxAlnScorePerBP;
    float minScorePercentage, minBreakpointScorePercentage;

    bool antisenceStrand, transcriptomeAnalysis;
    std::string outBreakPath, outUnalignedPath;
    std::vector<std::string> ignoredSequences;

    string gtfPath;
};


// this structure is used to filter the same breakpoints created from transcriptome local alignments

struct SplitBreakpoint {
    BreakpointCandidatePtr bp;
    int idx;
    SplitBreakpoint(int index ) : idx(index) {}
};


inline bool operator==(SplitBreakpoint const &sb1, SplitBreakpoint const &sb2)
{
    //TODO: what if in other directions?

    //Note: most likely length is not so important

    BreakpointCandidatePtr br1 = sb1.bp;
    BreakpointCandidatePtr br2 = sb2.bp;
    return (
             br1->readName == br2->readName &&
             br1->alns[0].refId == br2->alns[0].refId &&
             br1->alns[0].refStart == br2->alns[0].refStart &&
             br1->alns[0].positiveStrand == br2->alns[0].positiveStrand &&
            //br1->alns[0].queryStart == br2->alns[0].queryStart &&
             //br1->alns[0].length == br2->alns[0].length &&
             br1->alns[1].refId == br2->alns[1].refId &&
             br1->alns[1].refStart == br2->alns[1].refStart &&
             br1->alns[1].positiveStrand == br2->alns[1].positiveStrand
             //br1->alns[1].queryStart == br2->alns[1].queryStart &&
             //br1->alns[1].length == br2->alns[1].length
     )  ;
}

inline std::size_t hash_value(SplitBreakpoint const& br) {
    std::size_t seed = 0;

    string readName = toCString(br.bp->readName);

    foreach_ (char c, readName) {
        boost::hash_combine(seed, c);
    }

    std::vector<QueryAlignment>& alns = br.bp->alns;

    foreach_ (const QueryAlignment& aln, alns) {
        foreach_ (char c, aln.refId) {
            boost::hash_combine(seed, c);
        }

        boost::hash_combine(seed, aln.refStart);
        boost::hash_combine(seed, aln.positiveStrand);
        //boost::hash_combine(seed, aln.queryStart);
        //boost::hash_combine(seed, aln.length);

    }


    return seed;
}


typedef map<CharString,vector<BamAlignmentRecord> > QueryAlignmentMap;

class LocalAlignmentStore
{
    SplitReadAlgoSettings settings;
    const StringSet<CharString>& refNames;
    SplitReadAnalysisHelper analysisHelper;
    std::vector<BreakpointCandidatePtr> findCandidatesForOneRead(const CharString& readName, const vector<BamAlignmentRecord>& alignments, vector<int>& notUsedAlignmentIndexes);

public:
    LocalAlignmentStore(const SplitReadAlgoSettings& s, const StringSet<CharString>& rNames) : settings(s), refNames(rNames), analysisHelper(rNames,s.maxNumMismatches, s.minScorePercentage) {}
    bool init(const BamHeader& header);
    std::vector<BreakpointCandidatePtr> constructCandidates( QueryAlignmentMap& alignmentsMap);

    static QueryAlignment getLocalAlignment(const BamAlignmentRecord& record,const StringSet<CharString> &refNames, int maxAdditionalClippedRegionSize);
    static QueryAlignment getLocalAlignment(const BamAlignmentRecord& record,const std::vector<std::string> &refNames, int maxAdditionalClippedRegionSize);
    static int getIntTagValue(const BamAlignmentRecord& record, const char* tag, bool& ok);

};

#endif // LOCAL_ALIGNMENT_STORE_H
