#ifndef REFERENCE_H
#define REFERENCE_H

#include <string>
#include <seqan/seq_io.h>

using std::string;

class ReferenceSeq
{
    seqan::FaiIndex refIndex;
public:
    ReferenceSeq() {}
    virtual bool load(const string& path) = 0;
    virtual seqan::CharString getSubSequence(const string& refId, int start, int length, bool revCompl = false) = 0;
    virtual bool getWholeSequence(seqan::CharString& sequence, const string& refId) = 0;
    virtual bool includesSequence(const string& refId) = 0;
};


class ReferenceSeqIndexed : public ReferenceSeq
{
    seqan::FaiIndex refIndex;
public:
    bool load(const string& path);
    seqan::CharString getSubSequence(const string& refId, int start, int length, bool revCompl = false);
    bool getWholeSequence(seqan::CharString& sequence, const string& refId);
    bool includesSequence(const string& refId);
};


class ReferenceSeqInMemory : public ReferenceSeq
{
    std::map<string, seqan::CharString> seqMap;
public:
    bool load(const string& path);
    seqan::CharString getSubSequence(const string& refId, int start, int length, bool revCompl = false);
    bool getWholeSequence(seqan::CharString& sequence, const string& refId);
    bool includesSequence(const string &refId);
};



#endif // REFERENCE_H
