#include <algorithm>

#include "GenomicInterval.h"

using std::min;
using std::max;

void GenomicInterval::joinInterval(const GenomicInterval& r)
{
    if ( !intersects(r) ) {
        return;
    }

    begin = min(begin, r.begin);
    end = max(end, r.end);

}


void GenomicInterval::joinInterval(int startPos, int endPos) {
    return joinInterval( GenomicInterval(refId, startPos,endPos));
}



