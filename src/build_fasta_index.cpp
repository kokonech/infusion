#include <iostream>
#include <seqan/seq_io.h>

using namespace std;

int main(int argc, char const ** argv)
{
    // TODO: it's just a quick hack, make it look nice later

    if (argc < 2) {
        cerr << "Wrong number of arguments!" << endl;
        cerr << "Usage: ./build_fasta_index PATH_TO_FASTA_FILE" << endl;
        return -1;
    }

    string pathToFasta = argv[1];
    std::string pathToIndex(pathToFasta + ".fai");

    seqan::FaiIndex faiIndex;
    int res = build(faiIndex, pathToFasta.c_str(), pathToIndex.c_str());
    if (res != 0) {
        std::cerr << "ERROR: Could not build the index!\n";
        return -1;
    }

    int res2 = write(faiIndex);
    if (res2 != 0) {
        std::cerr << "ERROR: Could not save the index\n";
        return -1;
    }

    cerr << "Saved index to " << pathToIndex << endl;

    return 0;
}



