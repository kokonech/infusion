#ifndef PAIRED_ALIGNMENTS_STORE_H
#define PAIRED_ALIGNMENTS_STORE_H

#include <iostream>
#include <vector>
#include <map>
#include <boost/unordered_set.hpp>

#include <seqan/bam_io.h>
#include <seqan/sequence.h>
#include "BreakpointCandidate.h"
#include "AnnotationModel.h"

using std::vector;
using std::string;
using boost::unordered_set;
using seqan::BamAlignmentRecord;
using seqan::String;
using seqan::StringSet;

typedef std::pair<BamAlignmentRecord,BamAlignmentRecord> ReadPair;

// TODO: reduce memory usage by replacing BamAlignmentRecord with custom class

enum PairOrientation {
    FF, FR, RF
};

enum LibraryProtocol {
    NonStrandSpecific = 0, StrandSpecificForward, StrandSpecificReverse
};

struct SplicedAlignment {
    GenomicInterval interval;
    string transcriptId;
    int score;
    SplicedAlignment() : score(0) {}
};

inline bool operator==(SplicedAlignment const &aln1, SplicedAlignment const &aln2)
{
    return ( aln1.interval.refId == aln2.interval.refId &&
             aln1.interval.begin == aln2.interval.begin &&
             aln1.interval.end == aln2.interval.end ) ;
}

inline std::size_t hash_value(SplicedAlignment const& splicedAln) {
    std::size_t seed = 0;
    boost::hash_combine(seed, splicedAln.interval.begin);
    boost::hash_combine(seed, splicedAln.interval.end);
    foreach_ (char c, splicedAln.interval.refId) {
        boost::hash_combine(seed, c);
    }

    return seed;
}

class PairedAlignment {
    unordered_set<SplicedAlignment> firstMates; // -> to SplicedAlignment!
    unordered_set<SplicedAlignment> secondMates;
    PairOrientation orientation;
public:
    PairedAlignment() : orientation(PairOrientation::FR) {}
    void addUpstreamAlignment(const SplicedAlignment& aln) {
        firstMates.insert(aln);
    }

    void addDownstreamAlignment(const SplicedAlignment& aln) {
        secondMates.insert(aln);
    }

    const unordered_set<SplicedAlignment>& getFirstMates() const { return firstMates; }
    const unordered_set<SplicedAlignment>& getSecondMates() const { return secondMates; }

    PairOrientation getOrientation() { return orientation; }

};


struct AlignmentContext {
    bool alignedToTranscriptome;
    TranscriptSet transcripts;
    StringSet<CharString> refNameSet;
    map<string,string> transcriptToGeneMap;
    AlignmentContext() : alignedToTranscriptome(true){}
    int getRefIdByName(const CharString& name);
};


struct PairedAlignmentStoreConfig {
    int maxInsertSize;
    int minAnchorSize;
    int matchBonus;
    int mismatchScore;
    long numReads;
    bool computeCounts;
    PairedAlignmentStoreConfig() : maxInsertSize(10000), minAnchorSize(8),
        matchBonus(2), mismatchScore(4), numReads(0), computeCounts(false) {}

};


class PairedAlignmentsStore
{
    std::map<string,PairedAlignment> alignmentMap;
    std::map<string, int> countsData;
    AlignmentContext ctx;
    std::ofstream outfile;
    std::vector<int> insertSize;
    PairedAlignmentStoreConfig cfg;
    void collectInsertSize(const QueryAlignment& aln1, const QueryAlignment& aln2);
    int getAlignmentScore(const BamAlignmentRecord& rec);
    SplicedAlignment getSplicedAlignment(const BamAlignmentRecord& rec, bool& ok);
    SplicedAlignment getSplicedAlignmentFromGenomic(const BamAlignmentRecord& rec);
    //QueryAlignment toQueryAlignment(const BamAlignmentRecord& r, string& geneName, bool& ok);
    QueryAlignment toQueryAlignment(const SplicedAlignment& splicedAln);
public:
    PairedAlignmentsStore(const TranscriptSet& transcripts, const StringSet<CharString>& refNameSet, const PairedAlignmentStoreConfig& cfg );
    void clear() { alignmentMap.clear(); }
    void addAlignment(BamAlignmentRecord& record, bool upstream);
    void findBreakpointCandidates(vector<BreakpointCandidatePtr>& result, const BreakpointConstraints& bc);
    bool initBreakpointCandidatesOutput(const string& path);
    void writeBreakpointCandidates(const vector<BreakpointCandidatePtr>& result);
    void writeInsertSizeDistribution(const string& pathToOutFile);
    void writeInsertSizeDistributionParams(const string& pathToOutFile);
    void writeGeneCounts(const string& path);

};

#endif // PAIRED_ALIGNMENTS_STORE_H
