#include <iostream>
#include <vector>

#include <seqan/arg_parse.h>

#include "AnnotateFusionsTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME "annotate_fusions"

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It takes fusion clusters as input and annotates the fusions based on their properties. \
                   This includes intersection with genes, homology and other filters.");

    string usageLine;
    usageLine.append(" [OPTIONS] -i fusionClusters.txt -o updatedClusters.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");
    addOption(parser, ArgParseOption( "i", "input", "Path to input fusion clusters. Use - to for standard input.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "o", "output", "Path to output updated fusion clusters. Use - to for standard output", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "g", "gtf", "Path to annotations in GTF format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "t", "transcripts", "Path to transcript sequences in FASTA format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "b", "fusions-alignment", "Path to fusion seq alignment in SAM format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "is", "insert-size-params", "Path to insert size distribution params", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "ge", "gene-expression", "Path to gene expression data.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "f", "genome-sequence", "Path to genome data in FASTA format.", ArgParseArgument::STRING ));

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    AnnotateFusionsTaskConfig cfg;

    getOptionValue(cfg.inputPath, parser, "i");
    getOptionValue(cfg.outputPath, parser, "o");
    getOptionValue(cfg.pathToGeneModels, parser, "g");
    getOptionValue(cfg.pathToTranscripts, parser, "t");
    getOptionValue(cfg.pathToInsertSizeDistrParams, parser, "is");
    getOptionValue(cfg.pathToFusionSeqAlignments, parser, "b");
    getOptionValue(cfg.geneExpressionPath, parser, "ge");
    getOptionValue(cfg.pathToGenomeFile, parser,"f");


#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input file: " << cfg.inputPath << endl;
    cerr << "Output file: " << cfg.outputPath << endl;
    cerr << "Annotations file: " << cfg.pathToGeneModels << endl;
    cerr << "Transcripts file: " << cfg.pathToTranscripts << endl;
    cerr << "Insert size params: " << cfg.pathToInsertSizeDistrParams << endl;
    cerr << "Genome file" << cfg.pathToGenomeFile << endl;

#endif

    AnnotateFusionsTask task(cfg);
    return task.run();

}





