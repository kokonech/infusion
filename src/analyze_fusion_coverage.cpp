#include <iostream>

#include <seqan/arg_parse.h>
#include "AnalyzeFusionCoverageTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "analyze_fusion_coverage"

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It analyzes coverage around fusion breakpoints.");

    string usageLine;
    usageLine.append(" [-c coverageData.txt -r outputReport] -b inputBam -i fusionClusters.txt  -o outputCoverage.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");

    vector<ArgParseOption> options;

    options.push_back( ArgParseOption( "i", "input", "Path to input fusion clusters", ArgParseArgument::STRING ));
    options.push_back( ArgParseOption( "b", "bam", "Path to input BAM file with fusion alignments", ArgParseArgument::STRING ));
    options.push_back(ArgParseOption( "o", "output", "Path to output fusion cluster with annotated coverage", ArgParseArgument::STRING ));

    //addOption(parser, ArgParseOption("d", "detailed", "Output alignments of reads supporting fusions, without read sequences"));

    foreach_ (ArgParseOption& opt, options) {
        setRequired(opt, true);
        addOption(parser, opt);
    }

    addOption(parser, ArgParseOption( "c", "coverage", "Path to coverage data in SamTools mpileup format. When given - fusion coverage will be analyzed.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "or", "covering-reads", "Path to output list of reads covering downstrea fusion region", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "od", "detailed-report", "Path to output detailed fusion coverage report", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "s", "region-size", "Size of the region downstream of fusion breakpoint to check for coverage. Default is 50.", ArgParseArgument::INTEGER ));



    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    AnalyzeFusionCoverageTaskConfig cfg;

    getOptionValue(cfg.inputFusionsPath, parser, "i");
    getOptionValue(cfg.inputBamFile, parser, "b");
    getOptionValue(cfg.outputFusionsPath, parser, "o");

    getOptionValue(cfg.inputCoverageData, parser, "c");
    getOptionValue(cfg.outputReport, parser, "od");
    getOptionValue(cfg.coveringReadsReport, parser, "or");
    getOptionValue(cfg.downstreamRegionSize, parser, "s");


    cerr << "Downstream region size: " << cfg.downstreamRegionSize << endl;

    AnalyzeFusionCoverageTask task(cfg);
    return task.run();

}





