#include <iostream>
#include <memory>
#include <boost/scoped_ptr.hpp>
#include <boost/filesystem.hpp>
#include <seqan/arg_parse.h>
#include <seqan/bam_io.h>

#include "ChunkBamReader.h"
#include "StringUtils.h"
#include "BreakpointCandidate.h"
#include "ClusterRegistry.h"
#include "GlobalContext.h"
#include "LocalAlignmentStore.h"
#include "PerfTimer.h"

using namespace std;
using namespace seqan;

#define PROGRAM_NAME "cluster_candidates"

struct ClusterCandidatesSettings {
    vector<string> localCandidatesPathList;
    string pairedCandidatesPath;
    string outputPath;
    vector<string> supportingAlignmentPathList;
    string referencePath;
    bool dumpClusters;
    FindSupportingReadsConfig findSupportersConfig;
    ClusterRegistryConfig clusterRegistryConfig;

    ClusterCandidatesSettings() :
        dumpClusters(false) { }
};

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of InFusion pipeline. It analyses clusters input breakpoint candidates to form fusions");

    string usageLine;

    usageLine.append(PROGRAM_NAME).append( " [OPTIONS]");

    addUsageLine(parser, usageLine);

    addSection(parser, "Options");

    // TODO: set all options as required?

    addOption(parser, ArgParseOption("l", "local", "Paths to breakpoint candidates from local alignment, comma-separated", ArgParseArgument::STRING) );
    addOption(parser, ArgParseOption("p", "paired", "Path to breakpoint candidates from paired alignment", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption("o", "output", "Path to output", ArgParseArgument::STRING));
    addOption(parser, ArgParseOption("s", "supporting", "Path to supporting alignments", ArgParseArgument::STRING));
    addOption(parser, ArgParseOption("r", "reference", "Path to reference", ArgParseArgument::STRING));
    addOption(parser, ArgParseOption("mrg","merge-intron-separated", "Try merging intron-separated clusters, default is false."));
    addOption(parser, ArgParseOption("mri","merge-intersecting", "Try merging intron-separated clusters, default is false."));
    addOption(parser, ArgParseOption("dmp","dump-clusters", "Dump clusters (for debugging purposes), default is false."));
    addOption(parser, ArgParseOption("dbg","debug-output", "A lot of debug output, default is false."));
    // TODO: strand-specificity should be used properly
    addOption(parser, ArgParseOption("lib", "strand-specificity", "RNA-seq library type: FR (strand-specific forward), RF(strand-specific reverse), NA(non-strand specific)", ArgParseArgument::STRING));
    addOption(parser, ArgParseOption("T", "cluster-tolerance", "Max distance between intervals to form a cluster.", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("I", "insert-size-tolearnce", "Insert size tolerance for clusters from paired alignments", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("F", "fuzzy-breakpoint-tolerance", "Max distance between alignments to consider them belonging to same cluster.", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("D", "min-breakpoint-distance", "Find supporting reads parameter, minimum distance to breakpoint to consider alignment valid.", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("N", "num-mismatches", "Find supporting reads parameter, number of allowed mismatches in redeemed part of the read.", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("C", "add-clipped-size", "Find supporting reads parameter, size of additional clipped region in read", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("M", "num-multimapped", "Find supporting reads parameter, maximum number of multimapping allowed for redeemed part of the read.", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("cs", "chunk-size", "Size of chunk of reads to analyze at once", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption("np", "num-cores", "Number of cores to use in parallel computations", ArgParseArgument::INTEGER));


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

void searchForSupportingAlignments(const string& supportingAlignmentsPath, const FindSupportingReadsConfig& cfg, ClusterRegistry* clusterRegistry) {

    CpuPerfTimer t1, t2;

    BamStream bamStreamIn(supportingAlignmentsPath.c_str());
    if (!isGood(bamStreamIn)) {
        cerr << "Failed to open file " << supportingAlignmentsPath << endl;
        exit(-1);
    }

    int readCount = 0, totalCount = 0;
    vector<ReadLocalAlignment> alns;
    alns.resize(cfg.chunkOfReadsSize);
    bool haveReads = true;
    BamAlignmentRecord record;

    vector<string> refNames;
    StringUtils::convertStringSetToVector(bamStreamIn._nameStore, refNames);

    while ( haveReads )
    {
        t1.reset();
        haveReads = !atEnd(bamStreamIn);
        if (haveReads && (readCount < cfg.chunkOfReadsSize)) {

            if (readRecord(record, bamStreamIn) != 0)
            {
                std::cerr << "Could not read alignment!" << std::endl;
                exit(-1);
            }

            ReadLocalAlignment seg;
           // TODO: add configuration parameter for the max additional clipped region size.
            seg.aln = LocalAlignmentStore::getLocalAlignment(record, refNames, cfg.maxAdditionalClippedRegionSize);
            if (seg.aln.refStart == -1) {
                continue;
            }

            if (!seg.aln.positiveStrand) {
                reverseComplement(record.seq);
            }

            seg.multiMapped = seqan::hasFlagMultiple(record);

            seg.seq = record.seq;
            seg.readName = record.qName;
            StringUtils::addReadMateIdentifier(seg.readName, cfg.firstMates ? '1' : '2');

            alns[readCount++] = seg;
            t1.drop();

        } else {

            t1.drop();
            t2.reset();
            clusterRegistry->findSupporters(alns, readCount, cfg);
            t2.drop();

            totalCount += readCount;
            cerr << "Analyzed " << totalCount << " reads" << endl;
            readCount = 0;
        }

    }

    cerr << "Total loading time: " << t1.getElapsed() << endl;
    cerr << "Total processing time:" << t2.getElapsed() << endl;


}


void checkPairedCandidatesChromosomes(vector<BreakpointCandidatePtr> &candidates) {


    cerr << "Filtering paired candidates based on chromosome name... "  << endl;

    ReferenceSeq* ref = GlobalContext::getGlobalContext()->getGenomeReference();
    vector<BreakpointCandidatePtr> newCandidates;

    int filtered = 0;

    foreach_( const BreakpointCandidatePtr& bp, candidates ) {
        const QueryAlignment& aln1 = bp->alns[0];
        if (!ref->includesSequence(aln1.refId)) {
            filtered++;
            continue;
        }
        const QueryAlignment& aln2 = bp->alns[1];
        if (!ref->includesSequence(aln2.refId)) {
            filtered++;
            continue;
        }

        newCandidates.push_back(bp);

    }

    cerr << "Removed " << filtered << " paired end candidates with unsupported chromosome"  << endl;

    candidates = newCandidates;


}


int performAnalysis(ClusterCandidatesSettings& cfg) {

    boost::scoped_ptr<ClusterRegistry> clusterRegistry( ClusterRegistry::createInstance() );
    clusterRegistry->setConfiguration(cfg.clusterRegistryConfig);

    CpuPerfTimer t1;

    for (size_t i = 0, count = cfg.localCandidatesPathList.size(); i< count; ++i) {
        vector<BreakpointCandidatePtr> candidates;
        const string& path = cfg.localCandidatesPathList[i];
        cerr << "Started analysis of local candidates from " << path << endl;
        if (!BreakpointCandidate::readCandidates(candidates, path.c_str())) {
            cerr << "Failed to ropead breakpoint candidates from " << path << endl;
            return -1;
        }
        cerr << "Loaded "  << candidates.size() << " breakpoint candidates from " << path << endl;

        clusterRegistry->processBreakpoints(candidates);

        cerr << "Finihsed analysis of local candidates from " << path << endl;

    }

    if (cfg.pairedCandidatesPath.size() > 0) {
        cerr << "Started analysis of paired candidates from " << cfg.pairedCandidatesPath << endl;
        vector<BreakpointCandidatePtr> candidates;
        if (!BreakpointCandidate::readCandidates(candidates, cfg.pairedCandidatesPath.c_str())) {
            cerr << "Failed to read candidates from " << cfg.pairedCandidatesPath << endl;
            return -1;
        }

        cerr << "Loaded "  << candidates.size() << " breakpoint candidates from " << cfg.pairedCandidatesPath << endl;

        checkPairedCandidatesChromosomes(candidates);

        clusterRegistry->processBreakpoints(candidates);
        cerr << "Finished analysis of paired candidates from " << cfg.pairedCandidatesPath << endl;

    }

    cerr << "Finalizing clusters..." << endl;

    clusterRegistry->finalizeClustering();

    t1.drop();

    cerr << "Clustering time: " << t1.getElapsed() << endl;


    assert(cfg.supportingAlignmentPathList.size() <= 2);
    for (size_t i = 0; i < cfg.supportingAlignmentPathList.size(); ++i) {
        const string& path = cfg.supportingAlignmentPathList.at(i);
        if (i == 0) {
            cerr << "Searching for supporting alignment (upstream mates) in " << path << endl;
            cfg.findSupportersConfig.firstMates = true;
        } else {
            cerr << "Searching for supporting alignment (downstream mates) in " << path << endl;
            cfg.findSupportersConfig.firstMates = false;
        }
        CpuPerfTimer t2;
        searchForSupportingAlignments(path, cfg.findSupportersConfig, clusterRegistry.get());
        t2.drop();

        cerr << "Read rescue time: " << t2.getElapsed() << endl;
    }

    if (cfg.dumpClusters) {
        //string folderPath = cfg.outputPath[0] == '/' ? StringUtils::splitFileName(cfg.outputPath)[0] : ".";

        string folderPath = boost::filesystem::absolute(cfg.outputPath).parent_path().string() + "/clusters";

        clusterRegistry->dumpClusters(folderPath);
    }


    cerr << "Finished clustering. Writing result fusions..." << endl;

    clusterRegistry->outputFusions(cfg.outputPath);
    //Fusion::writeFusionClusters(clusterRegistry->getFusions(), cfg.outputPath );

    return 0;
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return -1;
    }

    boost::scoped_ptr<GlobalContext> ctx( new GlobalContext());
    GlobalContext::setGlobalContext(ctx.get());

    ClusterCandidatesSettings s;

    string localCandidatesPaths;
    getOptionValue(localCandidatesPaths, parser, "l");
    s.localCandidatesPathList = StringUtils::split(localCandidatesPaths, ',');

    string supportingAlnPaths;
    getOptionValue(supportingAlnPaths, parser, "s");
    s.supportingAlignmentPathList = StringUtils::split(supportingAlnPaths, ',');

    getOptionValue(s.pairedCandidatesPath, parser, "p");
    getOptionValue(s.outputPath, parser, "o");
    getOptionValue(s.referencePath, parser, "r");
    getOptionValue(s.dumpClusters, parser, "dmp");
    getOptionValue(s.clusterRegistryConfig.mergeIntronSeparated, parser, "mrg");
    getOptionValue(s.clusterRegistryConfig.mergeIntersecting, parser, "mri");
    getOptionValue(s.clusterRegistryConfig.pairedClusterOverlapTolerance, parser, "T");
    getOptionValue(Cluster::insertSizeTolerance, parser, "I");
    getOptionValue(Cluster::maxFuzzyBreakpointDistance, parser, "F");
    getOptionValue(s.findSupportersConfig.minDistanceToBreakpoint, parser, "D");
    getOptionValue(s.findSupportersConfig.numMismatches, parser, "N");
    getOptionValue(s.findSupportersConfig.maxAdditionalClippedRegionSize, parser, "C");
    getOptionValue(s.findSupportersConfig.maxMultiMappedAssignments, parser, "M");
    getOptionValue(s.findSupportersConfig.chunkOfReadsSize, parser, "cs");
    getOptionValue(s.findSupportersConfig.numCores, parser, "np");
    getOptionValue(s.findSupportersConfig.debugOutput, parser, "dbg");

    string protocol;
    getOptionValue(protocol, parser, "lib");
    if (protocol == "FR") {
        cerr << "Set strand specific forward library type" << endl;
        s.clusterRegistryConfig.protocol = StrandSpecificForward;
    } else if (protocol == "RF") {
        cerr << "Set strand specific reverse library type" << endl;
        s.clusterRegistryConfig.protocol = StrandSpecificReverse;
    } else if (protocol == "NA") {
        cerr << "Set non strand specific library type." << endl;
        s.clusterRegistryConfig.protocol = NonStrandSpecific;
    } else {
        if (protocol.size() > 0) {
            cerr << "Unknown strand specificity option: " << protocol << endl;
        }
        cerr << "Setting protocol as non strand-specific by default" << endl;
    }

    s.clusterRegistryConfig.pairedReads = s.pairedCandidatesPath.size() > 0;

    if (s.outputPath.length() == 0) {
        cerr << "Output path is not set!" << endl;
        return -1;
    }


    if ( s.referencePath.length() > 0 ) {
        cerr << "Loading reference from " << s.referencePath << endl;
        std::unique_ptr<ReferenceSeq> ref( new ReferenceSeqIndexed() );
        if (!ref->load(s.referencePath)) {
            cerr << "Failed to load reference sequence from " << s.referencePath << endl;
            return -1;
        }

        GlobalContext::getGlobalContext()->setGenomeReference(ref.release());

        s.clusterRegistryConfig.referenceSeqAvailable = s.referencePath.length() > 0;
    }


    if (supportingAlnPaths.length() > 0)  {
        if (s.referencePath.length() == 0) {
            cerr << "Reference is not set!" << endl;
            return -1;
        }
    }

    cerr << "Options" << endl;
    cerr << "Input local candidates: ";
    foreach_( const string& path, s.localCandidatesPathList)  {
        cerr << path << " ";
    }
    cerr << endl;
    cerr << "Input paired candidates " << s.pairedCandidatesPath << endl;
    foreach_( const string& path, s.supportingAlignmentPathList) {
        cerr << path << " ";
    }
    cerr << "Output path: " << s.outputPath << endl;
    cerr << "Max distance between alignments to form cluster: " << s.clusterRegistryConfig.pairedClusterOverlapTolerance << endl;
    cerr << "Insert size tolerance: " << Cluster::insertSizeTolerance << endl;
    cerr << "Fuzzy breakpoint tolerance: " << Cluster::maxFuzzyBreakpointDistance << endl;

    cerr << "\nRecue reads options" << endl;
    cerr << "Path to reference: " << s.referencePath << endl;
    cerr << "Minimum distance to breakpoint: " << s.findSupportersConfig.minDistanceToBreakpoint << endl;
    cerr << "Number of mismatches in redeemed read alignment: " << s.findSupportersConfig.numMismatches << endl;
    cerr << "Maximum multimapped assignments of redeemed read alignment: " << s.findSupportersConfig.maxMultiMappedAssignments << endl;
    cerr << "Chunk of reads size: " << s.findSupportersConfig.chunkOfReadsSize << endl;
    cerr << "Number of cores to use: " << s.findSupportersConfig.numCores << endl;


    return performAnalysis(s);

}

