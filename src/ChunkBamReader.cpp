#include "StringUtils.h"
#include "ChunkBamReader.h"

using namespace std;
using namespace seqan;

ChunkBamReader::ChunkBamReader(const char * inFile,  const ChunkBamReaderConfig& config)
    : cfg(config), bamStreamIn(inFile, BamStream::READ)
{


}

bool ChunkBamReader::init()
{
    if (cfg.maxChunkSize < 2) {
        cerr << "ChumkBamReader error: minimum chunk size is 2!" << endl;
        return false;
    }

    return isGood(bamStreamIn);
}


bool ChunkBamReader::loadNextChunk(std::vector<BamAlignmentRecord>& result)
{

    int bunchSize = 0;

    if (length(prevRecord.qName) > 0) {
        result.push_back(prevRecord);
        prevRecord.qName = "";
        bunchSize = 1;
        if (atEnd(bamStreamIn)) {
            return true;
        }
    }

#ifdef DEBUG
    clock_t start = clock();
#endif

    bool dataIsLeft = false;
    BamAlignmentRecord record;

    while ( !atEnd(bamStreamIn) )
    {

        if (readRecord(record, bamStreamIn) != 0)
        {
            std::cerr << "Could not read alignment!" << std::endl;
            return false;
        }

        if (cfg.skipUnmapped && hasFlagUnmapped(record)) {
            continue;
        }

        bunchSize++;

        if (cfg.removeMateId) {
            record.qName = StringUtils::removeReadMateIdentifer(record.qName);
        }

        if (cfg.reverseAlignmentStrand) {
            record.flag = hasFlagRC(record) ? record.flag & ~BAM_FLAG_RC : record.flag | BAM_FLAG_RC;
        }

        if ( bunchSize >= cfg.maxChunkSize && prevRecord.qName != record.qName ) {
            prevRecord = record;
            assert(length(prevRecord.qName) > 0);
            dataIsLeft = true;
            break;
        }

        result.push_back(record);
        prevRecord = record;

    }

#ifdef DEBUG
    double overall = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
    cerr << "Reading chunk of reads time is " << overall << endl;
#endif

    if (!dataIsLeft) {
        prevRecord.qName = "";
    }

    return true;

}

bool ChunkBamReader::isEof()
{
    return ( atEnd(bamStreamIn) && ( length(prevRecord.qName) == 0 ) );
}


