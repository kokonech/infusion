#include "InsertSizeDistr.h"
#include "StringUtils.h"

#include <fstream>

using namespace std;

bool DistrParams::loadFromFile(DistrParams& params, const string &filePath)
{
    std::ifstream infile;

    infile.open(filePath);
    if ( !infile.is_open())  {
        return false;
    }

    string line;

    while (getline(infile,line)) {
        if (StringUtils::trim(line).length() == 0 || line[0] == '#' ) {
            continue;
        }

        if (!StringUtils::startsWith(line,"Modified")) {
            continue;
        }

        string paramStr = StringUtils::split(line,':').at(1);

        vector<string> paramList = StringUtils::split(paramStr, ',');

        bool ok = false;

        string sizeStr = StringUtils::split(paramList.at(0), '=').at(1);
        params.size  = StringUtils::parseNumber<size_t>(sizeStr, ok );
        assert(ok);

        string meanStr = StringUtils::split(paramList.at(1), '=').at(1);
        params.mean  = StringUtils::parseNumber<float>(meanStr, ok );
        assert(ok);

        string stdStr = StringUtils::split(paramList.at(2), '=').at(1);
        params.std  = StringUtils::parseNumber<float>(stdStr, ok );
        assert(ok);


        string p25Str = StringUtils::split(paramList.at(3), '=').at(1);
        params.p25  = StringUtils::parseNumber<float>(p25Str, ok );
        assert(ok);

        string p50Str = StringUtils::split(paramList.at(4), '=').at(1);
        params.median  = StringUtils::parseNumber<float>(p50Str, ok );
        assert(ok);

        string p75Str = StringUtils::split(paramList.at(5), '=').at(1);
        params.p75  = StringUtils::parseNumber<float>(p75Str, ok);
        assert(ok);

        return ok;
    }

    return false;

}
