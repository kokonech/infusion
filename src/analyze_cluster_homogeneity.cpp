#include <iostream>

#include <seqan/arg_parse.h>

#include "AnalyzeClusterHomogeneityTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "analyze_cluster_homogeneity"

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It creates metaclusters out of fuison regions and analyzes their homogeneity");

    string usageLine;
    usageLine.append(" [OPTIONS] -i fusionClusters.txt -o updatedClusters.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");

    vector<ArgParseOption> options;

    options.push_back( ArgParseOption( "i", "input", "Path to input fusion clusters. Use - to for standard input.", ArgParseArgument::STRING ));
    options.push_back(ArgParseOption( "o", "output", "Path to output updated fusion clusters. Use - to for standard output", ArgParseArgument::STRING ));
    options.push_back(ArgParseOption( "r", "report", "Path to output reported metaclusters.", ArgParseArgument::STRING ));

    foreach_ (ArgParseOption& opt, options) {
        setRequired(opt, true);
        addOption(parser, opt);
    }

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    AnalyzeClusterHomogeneityTaskConfig cfg;

    getOptionValue(cfg.inPath, parser, "i");
    getOptionValue(cfg.outPath, parser, "o");
    getOptionValue(cfg.reportPath, parser, "r");

    AnalyzeClusterHomogeneityTask task(cfg);

    return task.run();

}




