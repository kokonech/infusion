#include "CleanupFusionClusters.h"


using namespace std;

struct BreakpointStats {
    int position;
    float score;
    BreakpointStats() : position(-1), score(0) {}
    BreakpointStats(int pos) : position(pos), score(1) {}
};



void CleanupFusionClustersTask::cleanupDiscordantRescuedReads()
{

    static const int NUM_FUSION_PARTS = 2;

    foreach_ (Fusion& f, fusions ) {

        const vector<BreakpointCandidatePtr>& bps = f.getBreakpoints();
        vector<BreakpointCandidatePtr> rescuedBreakpoints, toRemove;
        int numLocalNotRedeemed = 0;
        vector<BreakpointStats> bpLists[NUM_FUSION_PARTS];

        foreach_ (const BreakpointCandidatePtr bp, bps) {

            if (bp->isRedeemed()) {
                rescuedBreakpoints.push_back( bp );

                for (int i = 0; i < NUM_FUSION_PARTS; ++i) {
                    bool found = false;
                    int bpos = bp->alns.at(i).getPointClosestToBreakpoint(f.getDirection(i) == Cluster::DirectionRight);
                    foreach_ (BreakpointStats& stats, bpLists[i]) {
                        if (abs(bpos - stats.position) <= cfg.maxDistToBreakpoint) {
                            stats.score += 1;
                            found = true;
                            break;
                        }

                    }
                    if (!found) {
                        bpLists[i].push_back( BreakpointStats(bpos));
                    }
                }

            } else {
                if (bp->isOriginLocal()) {
                    numLocalNotRedeemed += 1;
                }
            }

        }

        if (rescuedBreakpoints.size() == 0) {
            continue;
        }

        int breakpointPositions[NUM_FUSION_PARTS];

        if (numLocalNotRedeemed == 0) {

            for (int i = 0; i < NUM_FUSION_PARTS; ++i) {
                const vector<BreakpointStats>& bpList = bpLists[i];
                int index = 0;
                float max_score = bpList[index].score;
                for (int j = 1; j < bpList.size(); j++) {
                    if (bpList[j].score > max_score) {
                        index = j;
                        max_score = bpList[j].score;
                    }
                }
                breakpointPositions[i] = bpList[index].position;
            }

        } else {
            breakpointPositions[0] =  f.getBreakpointPos(0);
            breakpointPositions[1] =  f.getBreakpointPos(1);
        }

        foreach_ (const BreakpointCandidatePtr bp, rescuedBreakpoints) {
            for (int i = 0; i < NUM_FUSION_PARTS; ++i) {
                int fusionBreakpointPos = breakpointPositions[i];
                int bPos = bp->alns.at(i).getPointClosestToBreakpoint(f.getDirection(i) == Cluster::DirectionRight);

                if (abs(bPos - fusionBreakpointPos) > cfg.maxDistToBreakpoint) {
                    toRemove.push_back(bp);
                    break;
                }
            }
        }

        if (toRemove.size()  > 0) {

            foreach_ (const BreakpointCandidatePtr& bp, toRemove ) {
                f.removeBreakpoint(bp);
            }

            f.updateIntervals(true);
        }


    }

}

int CleanupFusionClustersTask::run()
{
    if (!Fusion::readFusionClusters(fusions, cfg.inputPath, true)) {
        cerr << "Failed to read cluster from file " << cfg.inputPath << endl;
        return -1;
    }

    cleanupDiscordantRescuedReads();

    if (!Fusion::writeFusionClusters(fusions,cfg.outputPath)) {
        cerr << "Failed to write clusters to " << cfg.outputPath << endl;
        return -1;
    }

    return 0;

}
