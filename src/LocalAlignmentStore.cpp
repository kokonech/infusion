#include "LocalAlignmentStore.h"
#include "StringUtils.h"
#include "ChunkBamReader.h"

using namespace std;
using namespace seqan;


QueryAlignment getLocalAlignmentFromRead(const seqan::BamAlignmentRecord &r, int maxAdditionalClippedRegionSize) {

    QueryAlignment localAlignment;

    auto it = begin(r.cigar);
    auto itEnd = end(r.cigar);
    int posInRead = 0;
    size_t readSize = 0;
    int mappedRegionCount = 0, lengthInRead = 0;
    int firstClippedRegionSize = 0, lastClippedRegionSize = 0, clippedRegionCount = 0;
    char prevOp = 0;
    while ( it != itEnd ) {

        CigarElement<> c = *it;

        switch(c.operation) {

        case 'M':
            if (localAlignment.queryStart == -1) {
                localAlignment.queryStart = posInRead;
                firstClippedRegionSize = lastClippedRegionSize;
            }
            localAlignment.length += c.count;
            lengthInRead += c.count;
            readSize += c.count;
            if (prevOp != 'D' && prevOp != 'I') {
                mappedRegionCount += 1;
            }
            break;

        case 'S':
        case 'H':
            readSize += c.count;
            lastClippedRegionSize = c.count;
            clippedRegionCount += 1;
            break;

        case 'I':
            readSize += c.count;
            lengthInRead += c.count;
            break;

        case 'D':
            // TODO: issue-402
            //localAlignment.length += c.count;
            break;
        }


        posInRead += c.count;
        prevOp = c.operation;
        ++it;
    }

    assert(mappedRegionCount == 1);
    int additionalClippedRegionSize = clippedRegionCount > 1 ? min(firstClippedRegionSize,lastClippedRegionSize) : 0;

    if ( mappedRegionCount != 1 || additionalClippedRegionSize > maxAdditionalClippedRegionSize) {
        return localAlignment;
    }

    localAlignment.refStart = r.beginPos;
    if (!hasFlagRC(r)) {
        localAlignment.positiveStrand = true;
    } else {
        localAlignment.queryStart = readSize - (localAlignment.queryStart + lengthInRead - 1) - 1;
        /*if (localAlignment.queryStart < 0) {
            cerr << r.qName << endl;
        }*/
        assert(localAlignment.queryStart >= 0);
        localAlignment.positiveStrand = false;
    }


    bool ok = false;
    localAlignment.score = LocalAlignmentStore::getIntTagValue(r, "AS", ok);

    return localAlignment;
}


QueryAlignment LocalAlignmentStore::getLocalAlignment(const seqan::BamAlignmentRecord &r, const StringSet<CharString> &refNames, int maxAdditionalClippedRegionSize)
{
    QueryAlignment localAlignment = getLocalAlignmentFromRead(r,  maxAdditionalClippedRegionSize);

    localAlignment.refId = toCString(refNames[r.rID]);

    return localAlignment;
}

QueryAlignment LocalAlignmentStore::getLocalAlignment(const BamAlignmentRecord &r, const vector<string> &refNames, int maxAdditionalClippedRegionSize)
{
    QueryAlignment localAlignment = getLocalAlignmentFromRead(r,  maxAdditionalClippedRegionSize);
    localAlignment.refId = refNames[r.rID];

    return localAlignment;
}

int LocalAlignmentStore::getIntTagValue(const BamAlignmentRecord &r, const char* tag, bool& ok)
{
    BamTagsDict tags;
    setHost(tags, r.tags);
    unsigned tagIdx = 0;
    ok = false;
    int score = 0;
    if (findTagKey(tagIdx, tags, tag)) {
        ok = extractTagValue(score, tags, tagIdx);
    }

    return score;

}


vector<BreakpointCandidatePtr> LocalAlignmentStore::findCandidatesForOneRead(const CharString& readName, const vector<BamAlignmentRecord>& alignmentRecords, vector<int>& notUsedAlignmentIndexes) {

    int numAlignments = alignmentRecords.size();

    vector<BreakpointCandidatePtr> candidates;
    vector<bool> alignmentsUsage;
    alignmentsUsage.resize(numAlignments, false);

    //cerr << "analyzing " << readName << "... num alignments: " << numAlignments << endl;
    /*if (readName == "CHRFAM7A-USP33.fa.fastq.000000115/2") {
        cerr << "BINGO!" << endl;
    }*/


    vector<QueryAlignment> alignments;
    foreach_ (const BamAlignmentRecord& r, alignmentRecords) {
        // skip reads that are completely aligned
        if (ChunkBamReader::readCompletelytAligned(r)) {
            return candidates;
        }
        QueryAlignment aln = LocalAlignmentStore::getLocalAlignment( r, refNames, settings.maxAdditionalClippedRegionSize );
        alignments.push_back(aln);
    }

    // it could be possible splicing between 2 exons
    bool splicing = false;

    // breakpoint must consist of at least 2 alignments
    if (numAlignments > 1) {

        BreakpointConstraints bc;
        // TODO: add tests for each constraint
        bc.alignmentsCanIntersectLocally = true;
        bc.minDistBetweenAlignInRef = settings.minAlignDistanceInRef;
        bc.maxDistBetweenAlignInRead = settings.alignmentToleranceInRead;
        bc.maxIntersectionSizeInRead = settings.maxAlignIntersectionInRead;
        bc.ignoredSequences = settings.ignoredSequences;

        for (int i = 0; i < numAlignments; ++i) {
            if (alignments[i].refStart == -1) {
                continue;
            }
            for (int j = i + 1; j < numAlignments; ++j) {
                QueryAlignment localAlign1 = alignments[i];
                QueryAlignment localAlign2 = alignments[j];
                if (localAlign2.refStart == -1) {
                    continue;
                }
                if (!BreakpointCandidate::passConstraints(localAlign1, localAlign2, bc, splicing)) {
                    continue;
                }

                if (splicing) {
                    break;
                }

                BreakpointCandidatePtr c(new BreakpointCandidate() );
                c->readName = readName;

                StringUtils::addReadMateIdentifier(c->readName, settings.antisenceStrand ? '2' : '1');

                if (settings.antisenceStrand ) {
                    QueryAlignment::revertStrand(localAlign1);
                    QueryAlignment::revertStrand(localAlign2);
                    std::swap(localAlign1.queryStart, localAlign2.queryStart);
                    c->flags |= BreakpointFlags_OriginLocalSecond;
                } else {
                    c->flags |= BreakpointFlags_OriginLocalFirst;
                }


                // "Left" breakpoint in read coordinates is always first!

                if ( localAlign1.queryStart < localAlign2.queryStart ) {
                    c->alns.push_back(localAlign1);
                    c->alns.push_back(localAlign2);
                } else {
                    c->alns.push_back(localAlign2);
                    c->alns.push_back(localAlign1);
                }

                candidates.push_back(c);
                alignmentsUsage[i] = true;
                alignmentsUsage[j] = true;

            }


        }
    }


    if (splicing) {
        candidates.clear();
    } else {

        for (int i = 0; i < numAlignments; ++i) {
            if (!alignmentsUsage.at(i) && alignments.at(i).refStart != -1) {
                notUsedAlignmentIndexes.push_back(i);
            }
        }
    }


    return candidates;

}

bool LocalAlignmentStore::init(const seqan::BamHeader& header)
{
    // Prepare unused local alignments stream
    bool writeUnusedAlignments = settings.outUnalignedPath.size() > 0;

    if (writeUnusedAlignments) {
        if (!analysisHelper.initUnusedAlignmentsStream(settings.outUnalignedPath, header) ) {
            return false;
        }
    }

    return true;

}

vector<BreakpointCandidatePtr> LocalAlignmentStore::constructCandidates(QueryAlignmentMap &alignmentsMap)
{

    vector<BreakpointCandidatePtr> candidates;
    QueryAlignmentMap::const_iterator iter;

    for ( iter = alignmentsMap.begin(); iter != alignmentsMap.end(); ++iter) {

        vector<int> unusedAlignmentIndexes;
        vector<BreakpointCandidatePtr> candidatesForOneRead =  findCandidatesForOneRead( iter->first, iter->second, unusedAlignmentIndexes );

        vector<BamAlignmentRecord>& data = alignmentsMap[iter->first];
        bool multimapped = (unusedAlignmentIndexes.size() + candidatesForOneRead.size()) > 1;

        if (multimapped) {
            foreach_ (BreakpointCandidatePtr bp, candidatesForOneRead) {
                bp->flags |= BreakpointFlags_Multimapped;
            }
        }

        if (candidatesForOneRead.size() > 0) {
            candidates.insert(candidates.end(), candidatesForOneRead.begin(), candidatesForOneRead.end());
        }

        if (unusedAlignmentIndexes.size() > 0) {
            analysisHelper.writeUnusedLocalAlignments( data, unusedAlignmentIndexes, settings.antisenceStrand, multimapped );
        }

    }

    return candidates;
}

// SplitReadAnalysisHelper

SplitReadAnalysisHelper::~SplitReadAnalysisHelper()
{
    if (unusedAlignmentsStreamOk) {
        close(alnOutStream);
    }

}

bool SplitReadAnalysisHelper::initUnusedAlignmentsStream(const std::string& outUnalignedPath, const BamHeader& header)
{
    bool bamFormat = (string::npos == outUnalignedPath.find(".sam"));
    open(alnOutStream, outUnalignedPath.c_str(),BamStream::WRITE, bamFormat ? BamStream::BAM : BamStream::SAM);
    if (!isGood(alnOutStream)) {
        cerr << "Failed to create out file for unused alignments: " << outUnalignedPath << endl;
        return false;
    } else {
        alnOutStream.header = header;
    }

    unusedAlignmentsStreamOk = true;
    return true;

}

#define MIN_SUPPORT_SIZE 3

void SplitReadAnalysisHelper::writeUnusedLocalAlignments(vector<BamAlignmentRecord> &alignments, const vector<int>& indexes, bool readsFromAntisenceStrand, bool multiMapped) {

    if (!unusedAlignmentsStreamOk) {
        return;
    }

    foreach_ ( int idx, indexes) {

         BamAlignmentRecord& rec = alignments[idx];

         /*if (rec.qName == "SRR201779.3006122") {
             cerr << "BINGO!" << endl;
         }*/

         // Discard alignments that have score less than minScorePerentage of the maximum score

         int readSize = length(rec.seq);

         int maxScore = readSize*matchBonus;
         bool ok = false;
         float alnScore = LocalAlignmentStore::getIntTagValue(rec, "AS", ok);
         int numMismatches = LocalAlignmentStore::getIntTagValue(rec, "NM", ok);
         if (numMismatches > maxNM || alnScore < minScorePercentageForUnused*maxScore) {
             continue;
         }

         // Discard reads that are completely (or almost completely) mapped
         if (readSize - getAlignmentLengthInRef(rec) < MIN_SUPPORT_SIZE ) {
             //cerr << "Skipping supporting alignment" << rec.qName << endl;
             continue;
         }

         if (readsFromAntisenceStrand) {
             rec.flag = hasFlagRC(rec) ? rec.flag & ~BAM_FLAG_RC : rec.flag | BAM_FLAG_RC;
         }

         if (multiMapped) {
             CharString samTags;
             assignTagsBamToSam(samTags, rec.tags);
             samTags += "\tYM:i:1";
             assignTagsSamToBam(rec.tags, samTags);

             /* This is a little bit obscure, but fast way to mark multimapped reads */
             rec.flag = rec.flag | BAM_FLAG_MULTIPLE;
         }

         int res = writeRecord(alnOutStream, rec);
         if (res != 0) {
             std::cerr << "Error writing unused alignment" << rec.qName << endl;
         }
    }
}

