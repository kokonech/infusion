#include <iostream>
#include <vector>

#include <seqan/arg_parse.h>

#include "FindSupportingMatesTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "find_paired_mates"


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It takes fusion clusters as input and performs search of mate pairs for local alignments contributing to clusters");

    string usageLine;
    usageLine.append(" [OPTIONS] FUSION_CLUSTERS");
    addUsageLine(parser, usageLine);

    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "fusionClusters.txt"));

    std::vector<string> extentions;
    setValidValues(parser, 0 , extentions );

    addSection(parser, "Options");

    addOption(parser, ArgParseOption( "out", "output", "Path to output updated fusion clusters", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "gtf", "annotations", "Path to file with Ensemble-formatted annotations in GTF format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "g1", "genomic_upstream", "Path to upstream local genomic alignments in BAM format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "g2", "genomic_downstream", "Path to downstream local genomic alignments in BAM format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "t1", "transcript_upstream", "Path to upstream transcriptomic alignments in BAM format", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "t2", "transcript_downstream", "Path to downstream transcriptomic alignments in BAM format", ArgParseArgument::STRING ));

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    FindSupportingMatesConfig cfg;

    getArgumentValue(cfg.pathToFusions, parser, 0);

    getOptionValue(cfg.pathToOutputFusions, parser, "out");
    getOptionValue(cfg.pathToGtf, parser, "gtf");
    getOptionValue(cfg.pathToGenomicAlns1, parser, "g1");
    getOptionValue(cfg.pathToGenomicAlns2, parser, "g2");
    getOptionValue(cfg.pathToTranscriptomeAlns1, parser, "t1");
    getOptionValue(cfg.pathToTranscriptomeAlns2, parser, "t2");

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input file: " << cfg.pathToFusions << endl;
    cerr << "Output file: " << cfg.pathToOutputFusions << endl;
    cerr << "Annotations file: " << cfg.pathToGtf << endl;
    cerr << "Upstream transcriptome alignments file: " << cfg.pathToTranscriptomeAlns1 << endl;
    cerr << "Downstream transcritpome alignments file: " << cfg.pathToTranscriptomeAlns2 << endl;
    cerr << "Upstream genomic alignments file: " << cfg.pathToGenomicAlns1 << endl;
    cerr << "Downstream genomic alignments file: " << cfg.pathToGenomicAlns2 << endl;

#endif

    FindSupportingMatesTask findTask(cfg);

    return findTask.performAnalysis();

}



