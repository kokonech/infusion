#include <iostream>
#include <vector>

#include <seqan/arg_parse.h>
#include <boost/algorithm/string/erase.hpp>

#include "FilterFusionsTask.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME                    "filter_fusion_candidates"




void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It takes fusion clusters as input and performs various filtering upon input to increase specificity of predictions");

    string usageLine;
    usageLine.append(" [OPTIONS] FUSION_CLUSTERS");
    addUsageLine(parser, usageLine);

    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "fusionClusters.txt"));

    std::vector<string> extentions;
    setValidValues(parser, 0 , extentions );

    addSection(parser, "Options");

    addOption(parser, ArgParseOption("minSplit", "min-split-reads", "Min number of split reads to consider fusion realiable.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "minSpan", "min-span-pairs", "Min number of spanning pairs to consider fusion reliable", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "minFrag", "min-fragments", "Min number of fragments (sum of split reads and spanning pairs", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "minNR", "min-not-rescued", "Min number of not-rescued split reads that fusion should contain. Default is 0.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "minNJ", "min-splits-novel-junction", "Min number of split reads for fusion with novel junction (includes intronic and intergenic). Default is 3.",
                                      ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "minPAPR", "min-pap-rate", "Min rate of properly aligned pairs (according to insert size distribution). Default is 0.5", ArgParseArgument::DOUBLE ));
    addOption(parser, ArgParseOption( "minUAR", "unque-alignment-rate", "Min rate of unique split alignments. Default is 0.5", ArgParseArgument::DOUBLE ));
    addOption(parser, ArgParseOption( "minUSR", "unque-split-reads", "Min number of non-multimapped split reads with unique alignment. Default is 0.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "minHW", "min-homogeneity-weight", "Min weight of a fusion region in a metacluster. Default is 0.1", ArgParseArgument::DOUBLE ));
    addOption(parser, ArgParseOption( "reqHW", "required-homogeneity-weight", "Required minimum homogeneity weight for one of the fusions. Default is 0.4.", ArgParseArgument::DOUBLE ));
    addOption(parser, ArgParseOption( "splitPosDev", "split-pos-deviation", "Allowed deviation of the normalized split position from 0.5. Min is 0.1, max is 0.5, default is 0.1", ArgParseArgument::DOUBLE ));
    addOption(parser, ArgParseOption( "minBckgReads", "min-background-reads", "Minimum number of reads supporting blocks forming the fusion. Default is 1", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "p", "paired-end", "The dataset is paired-end" ));
    addOption(parser, ArgParseOption( "excl", "exclude-biotypes", "List of bioytypes to exclude from fusions", ArgParseArgument::STRING));
    addOption(parser, ArgParseOption( "nc", "allow-non-coding", "Allow fusions of non protein coding regions" ));
    addOption(parser, ArgParseOption( "ig", "allow-intergenic", "Allow to form fusions with one of the parts originating from an intergenic (non-annotated) region." ));
    addOption(parser, ArgParseOption( "in", "allow-intronic", "Allow to form fusions with one or both of the parts originating from an intronic region." ));
    addOption(parser, ArgParseOption( "out", "output", "Path to output file with filtered fusions", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "pg", "paralogs", "If path is provided, paralogs will be filtered.", ArgParseArgument::STRING ));

    // TODO: add min homology percentage

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    FilterFusionsTaskSettings settings;
    string inFile;

    getArgumentValue(inFile, parser, 0);

    getOptionValue(settings.outputPath, parser, "out");
    getOptionValue(settings.minSupportConfig.minSplitReads, parser, "minSplit");
    getOptionValue(settings.minSupportConfig.minSpanPairs, parser, "minSpan");
    getOptionValue(settings.minSupportConfig.minFragments, parser, "minFrag");
    getOptionValue(settings.minSupportConfig.minNotRescuedReads, parser, "minNR");
    getOptionValue(settings.minSupportConfig.minSplitReadsNovelJunction, parser, "minNJ");
    getOptionValue(settings.minBckgReads, parser, "minBckgReads");
    getOptionValue(settings.isPaired, parser, "p");
    getOptionValue(settings.allowNonCoding, parser, "nc");
    getOptionValue(settings.allowIntergenic, parser, "ig");
    getOptionValue(settings.allowIntronic, parser, "in");
    getOptionValue(settings.minProperlyAlignedPairsRate, parser, "minPAPR");
    getOptionValue(settings.splitPosDev, parser, "splitPosDev");
    if (settings.splitPosDev < 0.1 || settings.splitPosDev > 0.5) {
        settings.splitPosDev = 0.3;
    }

    getOptionValue(settings.minUniqueAlignmentRate, parser, "minUAR");
    getOptionValue(settings.minUniqueSplitReads, parser, "minUSR");
    getOptionValue(settings.minHomogeneityWeight, parser, "minHW");
    getOptionValue(settings.requiredHomogeneityWeight, parser, "reqHW");
    getOptionValue(settings.paralogsPath, parser, "pg");
    getOptionValue(settings.excludedBioTypes, parser, "excl");

    boost::erase_all(settings.excludedBioTypes, "\"");


    cerr << "Options" << endl;
    cerr << "Input file: " << inFile << endl;
    cerr << "Output fusions file: " << settings.outputPath << endl;
    cerr << "Paired data: " << settings.isPaired << endl;
    cerr << "Min split reads: " << settings.minSupportConfig.minSplitReads << endl;
    cerr << "Min span pairs: " << settings.minSupportConfig.minSpanPairs << endl;
    cerr << "Min fragments: " << settings.minSupportConfig.minFragments << endl;
    cerr << "Min non-rescued reads: " << settings.minSupportConfig.minNotRescuedReads << endl;
    cerr << "Min novel junction split reads: " << settings.minSupportConfig.minSplitReadsNovelJunction << endl;
    cerr << "Min unique split reads: " << settings.minUniqueSplitReads << endl;
    cerr << "Min unique alignmenet rate: " << settings.minUniqueAlignmentRate << endl;
    cerr << "Maximum split position deviation: " << settings.splitPosDev << endl;
    cerr << "Min properly aligned pairs rate: " << settings.minProperlyAlignedPairsRate << endl;

    cerr << "Min background reads: " << settings.minBckgReads << endl;

    cerr << "Allow non-coding: " << settings.allowNonCoding << endl;
    cerr << "Allow intergenic: " << settings.allowIntergenic << endl;
    cerr << "Allow intronic: " << settings.allowIntronic << endl;

    FilterFusionsTask filterFusionsTask(inFile, settings);
    return filterFusionsTask.run();

}


