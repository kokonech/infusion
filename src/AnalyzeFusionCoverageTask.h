#ifndef ANALYZE_FUSION_COVERAGE_TASK_H
#define ANALYZE_FUSION_COVERAGE_TASK_H

#include <fstream>

#include "Global.h"
#include "Fusion.h"
#include "IntervalTree.h"



class FusionBpRegion;
typedef boost::shared_ptr<FusionBpRegion> FusionBpRegionPtr;

struct CoverageAnnotation {
    int fusionCoveringReads[2];
    CoverageAnnotation() {
        fusionCoveringReads[0] = -1;
        fusionCoveringReads[1] = -1;
    }
    static string toStr(CoverageAnnotation& ann);
    static bool fromStr(const string& str, CoverageAnnotation& ann);
};

class FusionBpRegion {
    Fusion& parent;
    int intervalIndex;
    int numIntersectingReads;
    vector<string> readNames;

public:
    FusionBpRegion(Fusion& f, int idx) : parent(f), intervalIndex(idx), numIntersectingReads(0) {}
    const Fusion& getFusion() const { return parent; }
    int getIntervalIdx() const { return intervalIndex; }
    Fusion& getFusion() { return parent; }
    int getNumCoveringReads() const { return numIntersectingReads; }
    void addIntersectingRead(const char* readName ) { readNames.push_back(readName);}
    const vector<string>& getIntersectingReadNames() { return readNames; }
    void incNumCoveringReads() { numIntersectingReads++; }
};



typedef map<string,IntervalTree<FusionBpRegionPtr> > FusionIntervalTreeMap;

struct AnalyzeFusionCoverageTaskConfig {
    string inputFusionsPath;
    string inputCoverageData;
    string inputBamFile;
    string outputReport, coveringReadsReport, outputFusionsPath;

    int limit,downstreamRegionSize;

    AnalyzeFusionCoverageTaskConfig() {
        limit = 50;
        downstreamRegionSize = 50;
    }
};


class CoverageArray {
    std::map<int,int> coverageData;
public:
    void setCoverage(int pos, int coverageLevel) {
        coverageData[pos] = coverageLevel;
    }

    int getCoverage(int pos) {
        auto iter = coverageData.find(pos);
        if (iter != coverageData.end()) {
            return iter->second;
        } else {
            return 0;
        }
    }

};


class AnalyzeFusionCoverageTask
{
    AnalyzeFusionCoverageTaskConfig cfg;
    vector<Fusion> fusions;
    map<string,CoverageArray> coverageArrayMap;
    FusionIntervalTreeMap intervalTreeMap;

    void reportFusionCoverage(std::ofstream& outReport, const Fusion& fusion, int idx);
    bool createFusionCoverageReport();
    bool loadCoverageData();
    bool outputFusionCoveringReads();

    bool processCoverageDownstreamFusionBreakpoint();
    void createBpRegionsIntervalTree();
    void addRegionToIntervalTree(Fusion& f, int intervalIndex);

public:
    AnalyzeFusionCoverageTask(AnalyzeFusionCoverageTaskConfig& config) : cfg(config) {}
    int run();
};

#endif // ANALYZE_FUSION_COVERAGE_TASK_H
