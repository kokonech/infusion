#ifndef INSERT_SIZE_DISTR_H
#define INSERT_SIZE_DISTR_H

#include <math.h>

#include "Global.h"
#include "StringUtils.h"

struct DistrParams {

    DistrParams() : size(0), mean(0), std(0), median(0), p25(0), p75(0) {}
    size_t size;
    float mean, std;
    float median, p25, p75;

    static bool loadFromFile(DistrParams& params, const string& path);

    static float parsePapRateFromAnnotation(const string& annStr, bool& ok) {
        if (annStr.length() == 0) {
            return 0;
        }

        vector<string> items = StringUtils::split(annStr, ';');
        string rateStr = StringUtils::split(items.at(1), '=').at(1);

        return StringUtils::parseNumber<float>(rateStr, ok);

    }

    static void calcParamsFromSortedDistr(DistrParams& result, const vector<int>& distr) {

        // ASSUMPTION! the distr vector is sorted!

        size_t sum = 0;
        size_t sumOfSquares = 0;
        foreach_ (int val, distr) {
            sum += val;
            sumOfSquares += val*val;
        }

        size_t N = distr.size();
        result.size = N;

        if (N == 0) {
            return;
        }

        result.mean = sum / (float) N;
        result.p25 = distr[ N / 4 ];
        result.median = distr[ N / 2 ];
        result.p75 = distr[ (N * 3) / 4 ];

        if (N > 1) {
            float variance = ( 1. / (N - 1) ) * ( sumOfSquares - (sum*sum) / (float) N  );
            result.std = sqrt(variance);

        }




    }

};

inline std::ostream& operator<<(std::ostream& os, const DistrParams& p)
{
   os << "N=" << p.size << ", ";
   os << "mean=" << p.mean << ", ";
   os << "std=" << p.std << ", ";
   os << "p25=" << p.p25 << ", ";
   os << "p50=" << p.median << ", ";
   os << "p75=" << p.p75;

   return os;
}


#endif // INSERT_SIZE_DISTR_H
