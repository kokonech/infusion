#include "StringUtils.h"

#include "RescueLocalAlignmentsTask.h"
#include "PerfTimer.h"

using namespace std;

boost::mutex RescueLocalAlignmentsTask::guard;

RescueLocalAlignmentsTask::RescueLocalAlignmentsTask(const vector<ReadLocalAlignment>& localAlignments,
                                                     GenomicOverlapDetector<ClusterPtr>& detector,
                                                     const FindSupportingReadsConfig& c, int start, int end,
                                                     ResultStore* store)
    :localAlns(localAlignments), overlapDetector(detector), cfg(c), resultStore(store), startIdx(start), endIdx(end)
{

    assert(store);
}


static inline int inferLocalIndexFromCluster(const ClusterPtr& cluster, const QueryAlignment& aln) {
    return cluster->getDirection() == Cluster::DirectionLeft ? aln.positiveStrand ? 0 : 1 : aln.positiveStrand ? 1 : 0;
}

#define MIN_SUPPORTING_PATTERN_SIZE 3

BreakpointCandidatePtr RescueLocalAlignmentsTask::testForBreakpoint( const ReadLocalAlignment &seg, const ClusterPtr& targetCluster,
                                                                     const ClusterPtr& sourceCluster, int queryIndex,
                                                                     const FindSupportingReadsConfig& cfg )
{

    BreakpointCandidatePtr bp;

    Cluster::Direction targetDirection = targetCluster->getDirection();

    if (cfg.debugOutput) {
        cerr << "Trying to align for target " << targetCluster << endl;
    }

    // Checking if the alignment in the cluster is close enough to breakpoint position

    int minDistanceToSourceClusterBp = cfg.minDistanceToBreakpoint;

    // TODO: move this to upper level -> this has to be done only once for source cluster
    int sourceBp = sourceCluster->getBreakpointPos();
    if (sourceCluster->getNumLocalAlignments() == 0) {
        if (sourceCluster->getDirection() == Cluster::DirectionRight) {
            sourceBp -= Cluster::insertSizeTolerance;
        } else {
            sourceBp += Cluster::insertSizeTolerance;
        }
        minDistanceToSourceClusterBp = Cluster::insertSizeTolerance;
    }


    int alignmentBreakpoint =  sourceCluster->getDirection() == Cluster::DirectionRight ? seg.aln.refStart : seg.aln.inRefEnd();

    if (abs(sourceBp - alignmentBreakpoint) > minDistanceToSourceClusterBp ) {
        return bp;
    }


    assert(targetCluster->isUptodate());
    int minDistanceToTargetClusterBp = targetCluster->getNumLocalAlignments() == 0 ? cfg.minDistanceToBreakpoint + Cluster::insertSizeTolerance
                                                                                   : cfg.minDistanceToBreakpoint;

    const CharString& targetSeq =  targetCluster->getSequence();

    if (length(targetSeq) == 0) {
        cerr << "Failed to load segment of cluster " << targetCluster->getIdx() << endl;
        return bp;
    }

    int queryStart = queryIndex == 0 ? 0 : seg.aln.inQueryEnd() + 1;

    // when having 2 clipped regions we need to make sure that the select region is concordant with alignment
    // consider as an example read alignment 18S28M4S
    // we can not test remaining 4 nucleotides since the 18 first are not aligned properly, however the reverse sitatuation is possible

    if (queryIndex == 0) {
        int testLen = length(seg.seq) - seg.aln.inQueryEnd() - 1;
        if ( testLen  > cfg.maxAdditionalClippedRegionSize) {
            return bp;
        }
    } else {
        if (seg.aln.queryStart > cfg.maxAdditionalClippedRegionSize) {
            return bp;
        }
    }

    seqan::CharString patternSeq;
    if (queryIndex == 0) {
        patternSeq = seqan::prefix(seg.seq, seg.aln.queryStart);
    } else {
        patternSeq =  seqan::suffix(seg.seq, queryStart);
    }

    //cerr << "Pattern: " << patternSeq << endl;
    //cerr << "Target seq:" << targetSeq << endl;


    size_t patternLength = length(patternSeq);

    if (length(targetSeq) < patternLength + minDistanceToTargetClusterBp) {
        return bp;
    }

    CharString targetSeqSegment;
    int segmentOffset = 0;
    if (targetCluster->getDirection() == Cluster::DirectionRight) {
        int diff = targetCluster->getNumLocalAlignments() > 0 ?
                    ( targetCluster->getBreakpointPos() - targetCluster->getEncompassingInterval().begin ) : 0;
        targetSeqSegment = seqan::prefix(targetSeq, patternLength + diff + minDistanceToTargetClusterBp - 1);
    } else {
        int diff = targetCluster->getNumLocalAlignments() > 0 ?
                    ( targetCluster->getEncompassingInterval().end -targetCluster->getBreakpointPos() ) : 0;
        segmentOffset = length(targetSeq) - (patternLength + minDistanceToTargetClusterBp + diff);
        targetSeqSegment = seqan::suffix(targetSeq, segmentOffset );
    }

    //cerr << "Target seq segment: " << targetSeqSegment << endl;

    if (patternLength >= MIN_SUPPORTING_PATTERN_SIZE) {

        const SimpleInterval& cIv = targetCluster->getEncompassingInterval();
        bool revCompl = !Cluster::alnShouldHavePositiveStrandInCluster(targetDirection, queryIndex == 0);
        vector<SearchResult> results = StringUtils::findDnaPattern(patternSeq, targetSeqSegment, (-1)*cfg.numMismatches, revCompl);

        if  (results.size() > 0 ) {

            // sort results by score
            sort(results.begin(), results.end(), CompareSearchResults());

            const SearchResult& res = results.front();

            QueryAlignment supportingAln;
            supportingAln.queryStart = queryStart;
            supportingAln.length  = patternLength;
            supportingAln.refStart = cIv.begin + res.pos + segmentOffset;
            supportingAln.positiveStrand = res.isPositiveStrand;
            supportingAln.refId = targetCluster->getRefId();
            supportingAln.score = patternLength*cfg.matchBonus + cfg.mismatchCost*abs(res.score);

            bp.reset( new BreakpointCandidate() );
            if (queryIndex == 0) {
                bp->alns.push_back(supportingAln);
                bp->alns.push_back(seg.aln);
            } else {
                bp->alns.push_back(seg.aln);
                bp->alns.push_back(supportingAln);
            }
            bp->readName = seg.readName;
            if (cfg.firstMates) {
                bp->flags |= BreakpointFlags_OriginLocalFirst;
            } else {
                bp->flags |= BreakpointFlags_OriginLocalSecond;
            }

            if (seg.multiMapped) {
                bp->flags |= BreakpointFlags_Multimapped;
            }

            bp->flags |= BreakpointFlags_RedeemedReadAlignment;
        }

    }

    return bp;

}


static bool sameAlignment(const QueryAlignment& aln1,const QueryAlignment& aln2) {
    if (aln1.refId == aln2.refId) {
        if ( (aln1.refStart == aln2.refStart) && (aln1.length == aln2.length) && (aln1.positiveStrand == aln2.positiveStrand) ) {
            return true;
        }
    }

    return false;
}

static bool sameBreakpointCandidate(const BreakpointCandidatePtr& bp1, const BreakpointCandidatePtr& bp2) {
    if (bp1->readName == bp2->readName) {
        if ( sameAlignment(bp1->alns[0], bp2->alns[0]) && sameAlignment(bp1->alns[1], bp2->alns[1]) ) {
            return true;
        }
    }

    return false;
}


void RescueLocalAlignmentsTask::run()
{
    int resultCount = 0, count = 0;
    CpuPerfTimer t1,t2;
    for (int i = startIdx; i <= endIdx; ++i) {

        const ReadLocalAlignment& readAln = localAlns.at(i);
        if (cfg.debugOutput) {
            cerr << "Analyzing read : " << readAln.readName << endl;
        }

        /*if (readAln.readName == "SRR018269.3873466/1") {
            cerr << "BINGO!" << endl;
        }*/

        const QueryAlignment& aln = readAln.aln;

        vector<ClusterPtr> overlappingClusters;
        overlapDetector.findIntervals( overlappingClusters, aln.refId, aln.refStart, aln.inRefEnd() );
        int numAssignments = 0;

        // If there are more than one cluster always select one with local alignments!
        if (overlappingClusters.size() > 1) {
            vector<ClusterPtr> clustersWithLocalAlignments;
            foreach_ (const ClusterPtr& cluster, overlappingClusters) {
                if (cluster->getNumLocalAlignments() > 0) {
                    clustersWithLocalAlignments.push_back(cluster);
                }
            }

            if (clustersWithLocalAlignments.size() > 0) {
                overlappingClusters = clustersWithLocalAlignments;
            }
        }

        foreach_(const ClusterPtr& sourceCluster, overlappingClusters) {

            if (sourceCluster->getNumSegments() == 0 || length(sourceCluster->getSequence()) == 0) {
                continue;
            }

            int sourceIndex = inferLocalIndexFromCluster(sourceCluster, aln);
            int queryIndex = 1 - sourceIndex;
            t1.reset();

            if (cfg.debugOutput) {
                cerr << "Found overlapping cluster: " << sourceCluster << endl;
            }

            vector<ClusterPtr> partners;
            std::copy(sourceCluster->getUniquePartners().begin(), sourceCluster->getUniquePartners().end(),
                      back_inserter(partners ) );

            sort(partners.begin(),partners.end(), Cluster::MaxNumberOfBreakpoints());
            //const std::set<ClusterPtr>& partners = sourceCluster->getUniquePartners();

            t1.drop();

            t2.reset();

            vector<RescueAlignmentsTaskResult> resultsForOneRead;
            // TODO: use this map!
            //map<ClusterPtr,int> numLocalNotRescuedCandidates;

            foreach_ (const ClusterPtr& partnerCluster, partners) {

                if (partnerCluster->getNumSegments() == 0 ||  length(partnerCluster->getSequence()) == 0 ) {
                    continue;
                }

                BreakpointCandidatePtr bp = testForBreakpoint( readAln, partnerCluster, sourceCluster, queryIndex, cfg );
                if (bp) {

                    bool skipCandidate = false;
                    foreach_ (RescueAlignmentsTaskResult& r, resultsForOneRead) {
                        if (sameBreakpointCandidate( r.bp, bp )  ) {
                            int numLocalCandidates1 = partnerCluster->getNumLocalAlignments();
                            int numLocalCandidates2 = r.partnerCluster->getNumLocalAlignments();
                            if (numLocalCandidates1 > numLocalCandidates2) {
                                r.sourceCluster = sourceCluster;
                                r.sourceIdx = sourceIndex;
                                r.partnerCluster = partnerCluster;
                                r.queryIdx = r.queryIdx;
                            }
                            skipCandidate = true;
                            break;
                        }

                    }

                    if (skipCandidate) {
                        continue;
                    }

                    ++resultCount;
                    RescueAlignmentsTaskResult r;
                    r.bp = bp;
                    r.sourceCluster = sourceCluster;
                    r.sourceIdx = sourceIndex;
                    r.partnerCluster = partnerCluster;
                    r.queryIdx = queryIndex;
                    resultsForOneRead.push_back(r);

                    // TODO: issue-378

                    numAssignments++;
                    if (numAssignments >= cfg.maxMultiMappedAssignments) {
                        break;
                    }
                }
            }

            if (resultsForOneRead.size() > 1) {
                foreach_ (RescueAlignmentsTaskResult& r, resultsForOneRead) {
                    r.bp->flags |= BreakpointFlags_Multimapped;
                }
            }

            resultStore->insert(resultStore->end(), resultsForOneRead.begin(), resultsForOneRead.end() );

            t2.drop();
        }
        ++count;
    }

    //cerr << "Num reads processed: " << count << endl;
    //cerr << "Num results: " << resultCount << endl;

    //cerr << "Time getting unique partners: " << t1.getElapsed() << endl;
    //cerr << "Time testing for breakpoint: " << t2.getElapsed() << endl;






}
