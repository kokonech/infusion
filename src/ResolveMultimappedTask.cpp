#include "ResolveMultimappedTask.h"

using namespace std;

int ResolveMultimappedTask::performAnalysis()
{
    vector<Fusion> fusions;

    if (!Fusion::readFusionClusters(fusions, cfg.input)) {
        cerr << "Failed to read fusion clusters from " << cfg.input << endl;
        return -1;
    }

    foreach_ (Fusion& f, fusions) {
        f.calcScore(cfg.splitBpWeight, cfg.bridgeBpWeight);
    }

    MultimappedBreakpointDict mmBreakpointsMap;

    for ( size_t idx = 0, count = fusions.size(); idx < count; ++idx) {
        const Fusion& f = fusions.at(idx);
        foreach_ (BreakpointCandidatePtr bp, f.getBreakpoints()) {
            if (bp->isMultimapped()) {
                mmBreakpointsMap[toCString(bp->readName)].push_back( MultimappedBreakpoint(bp,idx) );
            }
        }
    }


    for (auto iter = mmBreakpointsMap.cbegin(); iter != mmBreakpointsMap.cend(); ++iter) {
        //const MultimappedBreakpointList& mmBreakpoints = iter->second;
        const MultimappedBreakpointList& mbpList = iter->second;
        if (mbpList.size() == 1) {
            BreakpointCandidatePtr bp = mbpList.front().bp;
            bp->flags &= ~BreakpointFlags_Multimapped;
        } else {
            resolutionStrategy->resolve(fusions, iter->first, iter->second);
        }
    }


    foreach_ ( Fusion& f, fusions) {
        f.updateIntervals(true);
    }

    if (!Fusion::writeFusionClusters(fusions,cfg.output)) {
        cerr << "Failed to write updated clusters to " << cfg.output << endl;
        return -1;
    }


    if (cfg.pathToFusionSeqs.length() > 0) {
        Fusion::writeFusionSeqsAsFasta(fusions, cfg.pathToFusionSeqs, 1000);
    }

    return 0;
}


void ContextBasedStrategy::resolve(vector<Fusion> &fusions, const string& readName, const MultimappedBreakpointList &multimappedBreakpoints)
{


    /*if (readName == "ZHX3-022-LAMA5-003.fa.fastq.000000245/1" ) {
        cerr << "BINGO!" << endl;
    }*/


    // Select among breakpoint candidates with proper mates



    int numBreakpointsWithProperMates = 0;

    foreach_ (const MultimappedBreakpoint& mbp, multimappedBreakpoints) {
        if (mbp.bp->hasProperPair() || mbp.bp->isOriginPaired() ) {
            ++numBreakpointsWithProperMates;
        }
    }

    set<int> fusionsHavingLocalBreakpoint;

    // TODO: is this really helping?
    // if there is only one fusion having local [but not rescued] candidate - pick it
    foreach_ (const MultimappedBreakpoint& mbp, multimappedBreakpoints) {
        const Fusion& f = fusions.at(mbp.fusionIdx);
        if (numBreakpointsWithProperMates > 0) {
            if (mbp.bp->isOriginLocal() && (!mbp.bp->hasProperPair()) ) {
                continue;
            }
        }

        auto bps = f.getBreakpoints();
        foreach_ (const BreakpointCandidatePtr& bp, bps) {
            if (bp->isOriginLocal() && !bp->isRedeemed()) {
                fusionsHavingLocalBreakpoint.insert(mbp.fusionIdx);
                break;
            }
        }
    }


    int maxScore = -1;
    size_t targetIdx = 0;

    // find one to keep
    for( size_t i = 0, count = multimappedBreakpoints.size(); i < count; ++i ) {
        const MultimappedBreakpoint& mbp = multimappedBreakpoints.at(i);

        if (numBreakpointsWithProperMates > 0) {
            if (mbp.bp->isOriginLocal() && (!mbp.bp->hasProperPair()) ) {
                continue;
            }
        }

        if (fusionsHavingLocalBreakpoint.size() > 0) {
            if (fusionsHavingLocalBreakpoint.count(mbp.fusionIdx) == 0) {
                continue;
            }
        }

        const Fusion& f = fusions.at(mbp.fusionIdx);
        int score = f.getScore();
        if (score > maxScore) {
            maxScore = f.getScore();
            targetIdx = i;
        }

    }


    // kill others
    for( size_t i = 0, count = multimappedBreakpoints.size(); i < count; ++i ) {
        if (i != targetIdx) {
            Fusion& f = fusions[ multimappedBreakpoints.at(i).fusionIdx  ];
            f.removeBreakpoint(readName);
        }
    }

}
