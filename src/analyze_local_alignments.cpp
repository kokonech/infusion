#include <iostream>
#include <algorithm>

#include <seqan/basic.h>
#include <seqan/sequence.h>
#include <seqan/file.h>     

#include <seqan/stream.h>
#include <seqan/bam_io.h>
#include <seqan/arg_parse.h>
#include <seqan/modifier.h>

#include "QueryAlignment.h"
#include "LocalAlignmentStore.h"
#include "StringUtils.h"
#include "MiscUtils.h"
#include "AnnotationModel.h"
#include "AlignmentUtils.h"

using namespace seqan;
using namespace std;
using boost::unordered_set;


#define PROGRAM_NAME                    "analyze_local_alignments"


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);

    addDescription(parser, "This tool is part of the InFusion pipeline. It analyses input local alignment from Bowtie2 to find possible fusion candidates using split-read approach");

    string usageLine;
    usageLine.append(" [OPTIONS] BAMFILE");
    addUsageLine(parser, usageLine);

    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "alignment.sam"));
    
    std::vector<string> extentions;
    extentions.push_back("sam");
    extentions.push_back("bam");
    setValidValues(parser, 0 , extentions );

    addSection(parser, "Options");

    addOption(parser, ArgParseOption("a", "min-anchor-length", "Min anchor length.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption("r", "antisence-strand", "The reads are coming from antisence strand, and thus all output breakpoints have to be reverted" ));
    addOption(parser, ArgParseOption("xrd", "min-dist-in-read", "Tolerance for distance between aligments within read.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption("Xrd", "max-dist-in-read", "Max intersection size between aligments within read.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "xrf", "min-dist-in-ref", "Min distance between alignments within reference.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "o", "out-breakpoints", "Path to file with possible breakpoints.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "u", "out-unused-alignments", "Set path to output unused local alignments, which do not form any breakpoint but larger than min-anchor-length.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "mm", "multi-mapped-count", "Max number of possible breakpoint candidates for multimapped breakpoints. Default is 0 (only unique alignments).", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption( "cs", "chunk-size", "Size of chunk of reads", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption( "ic", "ignore-chr", "Comma separted names of chromosome sequences to ignore", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "Xnm", "max-num-mismatches", "Max number of mismatches to keep an unused local alignment for further rescue. Default is 2", ArgParseArgument::INTEGER));
    addOption(parser, ArgParseOption( "p", "min-score-perecentage", "Min score percentage to keep an unused local alignment. Default is 0.5", ArgParseArgument::DOUBLE ));
    addOption(parser, ArgParseOption( "t", "transcriptome", "Path to transcriptome. If given, means that alignment is performed to transcriptome.", ArgParseArgument::STRING) );
    addOption(parser, ArgParseOption( "mbp", "min-bp-score-percentage", "Total breakpoint score percentage. Used to filter out low-quality SPLIT-reads during transcriptome analysis.", ArgParseArgument::DOUBLE) );


    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

QueryAlignment convertTransAlignmentToGenomic(const QueryAlignment& tAln, const TranscriptSet& transcripts, bool firstOfPair, bool& resultOk ) {

    QueryAlignment gAln;

    TranscriptSet::const_iterator it = transcripts.find(tAln.refId);
    if ( it  == transcripts.end()) {
        //cerr << "Failed to find transcript sequence " << transcriptId << ", ignoring read " << tAln. << endl;
        resultOk = false;
        return gAln;
    }


    const TranscriptPtr& transcript = it->second;

    vector<GenomicInterval> intervals = transcript->getGenomicIntervals(tAln.refStart, tAln.length);
    assert(intervals.size() > 0);


    bool genomicStrandIsPositive = tAln.positiveStrand == transcript->isPositiveStranded();

    GenomicInterval intervalCloseToBP = AlignmentUtils::pickNearestToBreakpointInterval(intervals, firstOfPair, genomicStrandIsPositive/*transcript->isPositiveStranded()*/);

    // Very small interval can be misleasing since the alignment can be wrong.
    // That is why it's better to ignore it

    // TODO: adapt min anchor size, based on read size?
    const int minAnchorSize = 3;
    if (intervalCloseToBP.length() <= minAnchorSize) {
        resultOk = false;
        return gAln;
    }

    gAln.refStart = intervalCloseToBP.begin;

    gAln.queryStart = tAln.queryStart;
    gAln.length = intervalCloseToBP.length();
    gAln.refId = transcript->getRefName();
    gAln.positiveStrand = transcript->isPositiveStranded() ? tAln.positiveStrand : !tAln.positiveStrand;
    gAln.score = tAln.score;

    resultOk = true;

    return gAln;

}



vector<BreakpointCandidatePtr> convertTranscriptomicCandidatesToGenomic(const vector<BreakpointCandidatePtr>& candidates, const TranscriptSet& transcripts, bool firstOfPair ) {

    vector<BreakpointCandidatePtr> convertedCandidates;

    foreach_ (const BreakpointCandidatePtr& c, candidates) {

        BreakpointCandidatePtr cNovel(new BreakpointCandidate() );

        cNovel->flags = c->flags;
        cNovel->readName = c->readName;

        bool resultOk = false;


        /*if (c->readName == "BAX-201-RAB22A-002-iso3.fa.fastq.000000019/1") {
            cerr << "BINGO!" << endl;
        }*/

        //TODO: only 2 alignments always? Shouls be more actually...

        const QueryAlignment& aln1 = c->alns[0];
        QueryAlignment convertedAln1 = convertTransAlignmentToGenomic(aln1, transcripts, firstOfPair, resultOk);
        if (!resultOk) {
            continue;
        }

        const QueryAlignment& aln2 = c->alns[1];
        QueryAlignment convertedAln2 = convertTransAlignmentToGenomic(aln2, transcripts, firstOfPair, resultOk);
        if (!resultOk) {
            continue;
        }

        cNovel->alns.push_back(convertedAln1);
        cNovel->alns.push_back(convertedAln2);

        convertedCandidates.push_back(cNovel);

    }

    return convertedCandidates;

}


void filterConvertedCandidates(vector<BreakpointCandidatePtr>& candidates, int minScore) {

    unordered_set<SplitBreakpoint> splitCandidates;


    cerr << "Initial number of candidates " << candidates.size() << endl;

    int idx = 0;
    foreach_ (const BreakpointCandidatePtr& c, candidates) {

        int score = c->alns[0].score + c->alns[1].score;
        if (score < minScore) {
            continue;
        }

        SplitBreakpoint spb(idx);
        spb.bp = c;
        idx++;
        splitCandidates.insert(spb);
    }


    cerr << "Number of fitered candidates " << splitCandidates.size() << endl;

    vector<BreakpointCandidatePtr> filteredCandidates;
    map<CharString,int> alignmentCount; // this map is requried to fix the multimapped flag

    // getting unique breakpoints based on genome alignment

    foreach_ (const SplitBreakpoint& spb, splitCandidates) {
        BreakpointCandidatePtr c = candidates[spb.idx];
        filteredCandidates.push_back(c);
        int count = MiscUtils::getMapValWithDef(alignmentCount, c->readName, 0);
        alignmentCount[c->readName] = count + 1;

    }

    // fixing multimapped flags

    double multimappedBP = 0;
    foreach_ (BreakpointCandidatePtr bp, filteredCandidates) {
        int count = alignmentCount[bp->readName];
        if (count == 1) {
            bp->flags &= ~BreakpointFlags_Multimapped;
        } else {
            //cerr << "Multimapped read: " << bp->readName << endl;
            multimappedBP++;
        }
    }

    cerr << "Proportion of multimapped breakpoint candidates: " << ( (multimappedBP / filteredCandidates.size() ) ) << endl;

    candidates = filteredCandidates;


}



int performAnalysis(const CharString& inFile, const SplitReadAlgoSettings& settings, int maxChunkSize) {
    
    QueryAlignmentMap alignmentRecordMap;
    BamAlignmentRecord record;
    int bunchSize = 0;
    int readSize = -1;
    CharString prevRecordName;
    vector<BreakpointCandidatePtr> candidates;

    BamStream bamStreamIn(toCString(inFile), BamStream::READ);

    if (!isGood(bamStreamIn)) {
        cerr << "Can not open input BAM file: " << inFile << endl;
        return false;
    }

    LocalAlignmentStore alnStore(settings, bamStreamIn._nameStore);
    if (!alnStore.init(bamStreamIn.header)) {
        cerr << "Failed to initialize local alignment analysis." << endl;
        return 1;
    }

    while (!atEnd(bamStreamIn))
    {
        
        if (readRecord(record, bamStreamIn) != 0)
        {
            std::cerr << "Could not read alignment!" << std::endl;
            return 1;
        }

        if (hasFlagUnmapped(record)) {
            continue;
        }
        
        if (getAlignmentLengthInRef(record) < settings.minAnchorLength) {
            continue;    
        }

        bool ok = false;
        int numMismatches = LocalAlignmentStore::getIntTagValue(record, "NM", ok);
        if (numMismatches > settings.maxNumMismatches ) {
            continue;
        }


        /*if (record.qName == "CHRFAM7A-USP33.fa.fastq.000000115/2") {
            cerr << "BINGO!" << endl;
        }*/

        if (readSize == -1) {
            // it is expected that all reads have the same size
            readSize = length(record.seq);
        }


        if ( bunchSize >= maxChunkSize && prevRecordName != record.qName ) {
            vector<BreakpointCandidatePtr> bunchCandidates = alnStore.constructCandidates(alignmentRecordMap);
            candidates.insert(candidates.end(), bunchCandidates.begin(), bunchCandidates.end());
            alignmentRecordMap.clear();
            bunchSize = 0;
        }

        vector<BamAlignmentRecord>& records = alignmentRecordMap[record.qName];
        if (records.size() < settings.maxMultimappedCount) {
            records.push_back ( record );
            bunchSize++;
        }

        prevRecordName = record.qName;

    }

    vector<BreakpointCandidatePtr> bunchCandidates = alnStore.constructCandidates(alignmentRecordMap);
    candidates.insert(candidates.end(), bunchCandidates.begin(), bunchCandidates.end());

    if (candidates.size() == 0) {
        cerr << "No breakpoint candidates found" << endl;
    }

    if (settings.transcriptomeAnalysis) {

        AnnotationModel model;

        cerr << "Loading transcript model to convert transcriptomic SPLIT reads to genomic..." << endl;
        if ( !model.loadTranscripts(settings.gtfPath) ) {
            cerr << "Failed to load transcripts from " << settings.gtfPath << endl;
            return -1;
        }

        int minScore = (readSize*settings.maxAlnScorePerBP) * settings.minBreakpointScorePercentage;

        cerr << "Performing conversion..." << endl;

        vector<BreakpointCandidatePtr> convertedCandidates =  convertTranscriptomicCandidatesToGenomic(candidates, model.getTranscripts(), !settings.antisenceStrand );

        cerr << "Filtering same converted breakpoint candidates (due to several transcripts of the same gene)... " << endl;

        filterConvertedCandidates(convertedCandidates, minScore);

        cerr << "Writing results..." << endl;

        BreakpointCandidate::writeCandidates(convertedCandidates, settings.outBreakPath.c_str());


    } else {

        cerr << "Writing results..." << endl;

        BreakpointCandidate::writeCandidates(candidates, settings.outBreakPath.c_str());

    }

    return 0;

}


int main(int argc, char const ** argv)
{
    
    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);
    
    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    SplitReadAlgoSettings s;
    CharString inFile;
    int chunkSize = 1;

    getArgumentValue(inFile, parser, 0);
    getOptionValue(s.minAnchorLength, parser, "a");
    getOptionValue(s.antisenceStrand, parser, "r");
    getOptionValue(s.alignmentToleranceInRead, parser, "xrd");
    getOptionValue(s.maxAlignIntersectionInRead, parser, "Xrd");
    getOptionValue(s.minAlignDistanceInRef, parser, "xrf");
    getOptionValue(s.outBreakPath, parser, "o");
    getOptionValue(s.outUnalignedPath, parser, "u");
    getOptionValue(s.maxMultimappedCount, parser, "mm");
    getOptionValue(s.maxNumMismatches, parser, "Xnm");
    getOptionValue(s.minScorePercentage, parser, "p");
    getOptionValue(s.gtfPath, parser, "t");
    getOptionValue(s.minBreakpointScorePercentage, parser, "mbp");
    getOptionValue(chunkSize, parser, "cs");

    string ignoredSeqs;
    getOptionValue(ignoredSeqs,parser, "ic");
    if (ignoredSeqs.size() > 0) {
        s.ignoredSequences = StringUtils::split(ignoredSeqs, ',');
    }


#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input file: " << inFile << endl;
    cerr << "Min-anchor-length: " << s.minAnchorLength << endl;
    cerr << "Alignment-tolerance-in-read: " << s.alignmentToleranceInRead << endl;
    cerr << "Max-intersection-size-in-read: " << s.maxAlignIntersectionInRead << endl;
    cerr << "Min-distance-between-alignments-in-ref: " << s.minAlignDistanceInRef << endl;

#endif

    if (s.outBreakPath.length() == 0) {
        cerr << "Path to output breakpoints is not set." << endl;
    } else {
        cerr << "Path to output breakpoints: " << s.outBreakPath << endl;
    }

    if (s.antisenceStrand ) {
        cerr << "Reads are from antisence strand!" << endl;
    }

    if (s.outUnalignedPath.length() == 0) {
        cerr << "Path to unused alignments is not set." << endl;
    } else {
        cerr << "Path to unused alignments: " << s.outUnalignedPath << endl;
    }


    if (s.gtfPath.length() != 0) {
        cerr << "Tanscriptome analysis activated." << endl;
        cerr << "Path to annotations: " << s.gtfPath << endl;
        s.transcriptomeAnalysis = true;
    }

    return performAnalysis(inFile,s, chunkSize);
}


