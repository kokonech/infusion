#include "ClusterRegistry.h"
#include "ClusterRegistryImpl.h"

using namespace std;


ClusterRegistry *ClusterRegistry::createInstance(const std::string& implName)
{

    if (implName == DEFAULT_IMPL) {
        return new ClusterRegistryImpl();
    } else {
        return NULL;
    }

}



void ClusterRegistry::writeFusionsGff(const char *path)
{
    ofstream out;
    out.open(path, ios_base::out);

    for (size_t i = 0, sz = fusions.size(); i < sz; ++i ) {

        if (fusions.at(i).isEmpty()) {
            continue;
        }
        //const ClusterBreakpoint& bp = clusters.at(i).getClusterBreakpoint();

        stringstream is;
        is << "cluster_" << i+1;
        string clusterName = is.str();

        //out << "fusion_border\t";
        //out << bp.ref1 << "\t" << bp.pos1 << "\t" << bp.ref2 << "\t" << bp.pos2 << endl;

        auto breakpoints = fusions.at(i).getBreakpoints();
        foreach_( const BreakpointCandidatePtr& c, breakpoints ) {
            foreach_(const QueryAlignment& aln, c->alns) {
                out << aln.refId << "\tinfusion\t" << clusterName << "\t";
                out << aln.refStart << "\t" << aln.inRefEnd() << "\t0\t" << BreakpointCandidate::strandToChar(aln.positiveStrand);
                out << "\t.\t" << "read_name \"" << c->readName << "\";\n";
            }


        }
    }

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


