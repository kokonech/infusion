#ifndef CLUSTER_REGISTRY_IMPL_H
#define CLUSTER_REGISTRY_IMPL_H

#include "ClusterRegistry.h"
#include "ClusterTree.h"
#include "BreakpointCandidate.h"
#include "GenomicInterval.h"
#include "GenomicOverlapDetector.h"
#include "ClusterTree.h"

typedef std::pair<int,int> ClusterPair;

class ClusterRegistryImpl : public ClusterRegistry
{
    std::vector<ClusterPtr> clusters;

    typedef std::pair<clusternode*,Cluster::Direction> ClusterIndex;

    struct IntervalInfo {
        BreakpointCandidatePtr bp;
        int localIdx;
        IntervalInfo(BreakpointCandidatePtr breakPoint, int localIndex) : bp(breakPoint), localIdx(localIndex) {}
    };

    typedef  boost::shared_ptr<ClusterTree<IntervalInfo> > ClusterTreePtr;

    map<string,ClusterTreePtr> clusterTreeMap;

    std::map<ClusterPair,Fusion> fusionMap;
    GenomicOverlapDetector<ClusterPtr> clusterOverlapDetector;
    bool loadedClusterSequences;

    ClusterPair getFusionMapIdx(const ClusterSegmentPtr& segment, const ClusterSegmentPtr& partner) {
        int idx1 = segment->getParentCluster()->getIdx();
        int idx2 = partner->getParentCluster()->getIdx();
        if (config.protocol == LibraryProtocol::NonStrandSpecific) {
            return idx1 < idx2 ? ClusterPair(idx1, idx2) : ClusterPair(idx2, idx1);
        } else {
            return segment->isFirst() ? ClusterPair(idx1,idx2) : ClusterPair(idx2,idx1);
        }
    }

    ClusterSegmentPtr createSegment(const BreakpointCandidatePtr& bp, int localIdx);
    void addIntervalToTree(const BreakpointCandidatePtr& bp, int localIdx);
    bool loadClusterRegionSequences(int additionalReserveSize);
    void dumpClusters(const string& fileName, const string& rName, bool dumpSegments);
    static bool clustersWithinMaxIntronLength(const ClusterPtr& prevCluster, const ClusterPtr& curCluster);
    void mergeIntronSepartedClusters();
    void mergeCrossingOverClusters();
    void mergeIntersectingClusters();
    void splitClusterByBreakpoint();
    void cleanupRescuedClusters();
    void createClusterGenomicOverlapper();
    void findSupportersParallel(const vector<ReadLocalAlignment>&alns, int count, const FindSupportingReadsConfig& cfg);
    inline bool canBeMerged(const ClusterPtr& cluster1 , const ClusterPtr& cluster2 );
    bool performClusterOverCrossing(const ClusterPtr& firstCluster, const ClusterPtr& secondCluster);
    bool haveCommonPartnersWithinIntronLength(const ClusterPtr& cluster1, const ClusterPtr& cluster2);


public:

    ClusterRegistryImpl() : loadedClusterSequences(false) {}
    virtual void processBreakpoints(const std::vector<BreakpointCandidatePtr>& bpntList);
    virtual void outputFusions(const string &outFileName);
    virtual void finalizeClustering();
    virtual void findSupporters(const vector<ReadLocalAlignment>&alns, int count, const FindSupportingReadsConfig& cfg);
    virtual void dumpClusters(const string &pathToFolder);


    virtual ~ClusterRegistryImpl() {}
};

#endif // CLUSTER_REGISTRY_IMPL_H
