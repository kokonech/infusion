#include <iostream>
#include <algorithm>
#include <ctime>

#include <seqan/arg_parse.h>

#include "PairedAlignmentsStore.h"
#include "BreakpointCandidate.h"
#include "AnnotationModel.h"
#include "ChunkBamReader.h"
#include "StringUtils.h"

using namespace std;
using namespace seqan;

#define PROGRAM_NAME "analyze_paired_alignments"

struct AnalyzePairedAlignmentsSettings {

    AnalyzePairedAlignmentsSettings() :
        transcriptomeAlignment(true), maxMismatchRate(1), minDistanceInRef(100000), chunkSize(1000000) {}

    bool transcriptomeAlignment;
    int maxMismatchRate, minDistanceInRef, chunkSize;
    PairedAlignmentStoreConfig cfg;
    string inputPath;
    string outputPath;
    string gtfPath;
    string insertSizeDistributionPath;
    string expressionDataPath;
    vector<string> ignoredChromosomes;
};


void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It analyses input paired alignments to find possible fusion candidates using bridge-read approach");

    string usageLine;

    usageLine.append("[OPTIONS] [UPSTREAM_BAMFILE] [DOWNSTREAM_BAMFILE]");

    addUsageLine(parser, usageLine);

    addArgument(parser, ArgParseArgument(ArgParseArgument::INPUTFILE, "bamfile"));

    addSection(parser, "Options");

    addOption(parser, ArgParseOption( "t", "transcriptome", "Path to transcriptome. If given, means that alignment is performed to transcriptome.", ArgParseArgument::STRING) );
    addOption(parser, ArgParseOption( "o", "outfile", "Path to file with possible breakpoints.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "cs", "chunk-size", "Size of chunk of reads to be loaded into memory.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "ic", "ignore-chr", "Comma separted names of chromosome sequences to ignore", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "isz", "insert-size-outfile", "Path to output file with insert size distribution.", ArgParseArgument::STRING ));
    addOption(parser, ArgParseOption( "x", "min-anchor-size", "Minimum size to consider a translated part of a genomi alignment", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "N", "num-reads", "Number of reads in the experiment applied to compute RPKM values.", ArgParseArgument::INTEGER ));
    addOption(parser, ArgParseOption( "gc", "gene-counts-outfile", "Path to output file with gene counts.", ArgParseArgument::STRING ));

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

static inline int getNumMismatches(const BamAlignmentRecord& r) {
    // TODO: implement this
    return 0;
}


/*static bool contains(const StringSet<CharString>& stringSet, const CharString& item ) {

    cerr << "Searching for " << item << endl;
    for (auto it = begin(stringSet); it != end(stringSet); ++it) {
        cerr << "Comparing with " << value(it) << endl;
        if ( value(it) == item ) {
            return true;
      }
    }

    return false;
}*/


int performAnalysis(AnalyzePairedAlignmentsSettings& settings) {

    ChunkBamReaderConfig cfg;
    cfg.maxChunkSize = settings.chunkSize;
    cfg.removeMateId = true;

    ChunkBamReader bamReader(settings.inputPath.c_str(), cfg);

    if (!bamReader.init()) {
        cerr << "Failed to open BAM file: " << settings.inputPath << endl;
        return -1;
    }

    AnnotationModel model;

    cerr << "Loading transcript model" << endl;
    if ( !model.loadTranscripts(settings.gtfPath) ) {
        cerr << "Failed to load transcripts from " << settings.gtfPath << endl;
        return -1;
    }

    settings.cfg.computeCounts = settings.expressionDataPath.length() > 0;
    cerr << "Preparing the analysis..." << endl;
    PairedAlignmentsStore pairedAlignmentsMap(model.getTranscripts(), bamReader.getRefNames(), settings.cfg);
    if (!pairedAlignmentsMap.initBreakpointCandidatesOutput(settings.outputPath)) {
        cerr << "Failed to open output file for writing: " << settings.outputPath << endl;
        return -1;
    }

    cerr << "Starting processing alignments" << endl;
    BreakpointConstraints bc;
    bc.minDistBetweenAlignInRef = settings.minDistanceInRef;
    bc.ignoredSequences = settings.ignoredChromosomes;

    long totalReadsProcessed = 0;
    while (!bamReader.isEof()) {

        vector<BamAlignmentRecord> records;
        if (!bamReader.loadNextChunk(records)) {
            cerr << "Error reading data from BAM file: " << settings.inputPath << endl;
        }

        size_t numRecords = records.size();
        for (size_t i = 0; i < numRecords; ++i) {
            BamAlignmentRecord& r =  records[i];
            assert(hasFlagFirst(r) || hasFlagLast(r));

            if (getNumMismatches(r) > settings.maxMismatchRate) {
                //cerr << "Skipping record " << record.qName << endl;
                continue;
            }

            pairedAlignmentsMap.addAlignment(r,  hasFlagFirst(r) );

        }

        vector<BreakpointCandidatePtr> candidates;
        pairedAlignmentsMap.findBreakpointCandidates(candidates,bc);

        pairedAlignmentsMap.writeBreakpointCandidates(candidates);

#ifdef DEBUG
        totalReadsProcessed += records.size();
        cerr << "Analyzed " << totalReadsProcessed << " reads..." << endl;
#endif

        pairedAlignmentsMap.clear();

    }

    cerr << "Writing insert size..." << endl;
    if (settings.insertSizeDistributionPath.length() > 0) {
        pairedAlignmentsMap.writeInsertSizeDistribution(settings.insertSizeDistributionPath);
        pairedAlignmentsMap.writeInsertSizeDistributionParams(settings.insertSizeDistributionPath + ".params");
    }

    cerr << "Writing counts..." << endl;
    if (settings.cfg.computeCounts) {
        pairedAlignmentsMap.writeGeneCounts(settings.expressionDataPath);
    }

    return 0;
}





int main(int argc, char const ** argv)
{
    
    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);
    
    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return -1;
    }

    AnalyzePairedAlignmentsSettings s;

    getArgumentValue(s.inputPath, parser, 0);

    getOptionValue(s.gtfPath, parser, "t");
    getOptionValue(s.outputPath, parser, "o");
    getOptionValue(s.chunkSize, parser, "cs");
    getOptionValue(s.cfg.minAnchorSize, parser, "x");
    getOptionValue(s.insertSizeDistributionPath, parser, "isz");
    getOptionValue(s.expressionDataPath, parser, "gc");
    getOptionValue(s.cfg.numReads, parser, "N");
    string ignoredChromosomes;
    getOptionValue(ignoredChromosomes, parser, "ic");
    if (ignoredChromosomes.size() > 0 ) {
        s.ignoredChromosomes = StringUtils::split(ignoredChromosomes, ',');
    }

    if (s.outputPath.length() == 0) {
        cerr << "Output path is not set!" << endl;
        return -1;
    }

#ifdef DEBUG
    cerr << "Options" << endl;
    cerr << "Input alignment: " << s.inputPath << endl;
    cerr << "Transcriptome aligment available: " << s.transcriptomeAlignment << endl;
    cerr << "Min distance in reference: " << s.minDistanceInRef << endl;
    cerr << "Maximum mismatch rate: " << s.maxMismatchRate << endl;
    cerr << "Ignored aligment to following chromosomes: " << ignoredChromosomes << endl;
    cerr << "Chunk size: " << s.chunkSize << endl;
    cerr << "Output path: " << s.outputPath << endl;
    if (s.insertSizeDistributionPath.length() > 0) {
        cerr << "Insert size distripution output path: " << s.insertSizeDistributionPath << endl;
    }
    if (s.expressionDataPath.length() > 0 ) {
        cerr << "Gene counts output path: " << s.expressionDataPath << endl;
    }

#endif

    return performAnalysis(s);
}


