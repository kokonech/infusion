#ifndef FUSION_H
#define FUSION_H

#include <iostream>
#include <boost/shared_ptr.hpp>

#include "Global.h"
#include "Cluster.h"
#include "AnnotationModel.h"

#define ANNOTATION_FILTERS              "filters"
#define ANNOTATION_GENE_1               "gene_1"
#define ANNOTATION_GENE_2               "gene_2"
#define ANNOTATION_EXPR_1               "expr_1"
#define ANNOTATION_EXPR_2               "expr_2"
#define ANNOTATION_TRANSCRIPTS_1        "transcript_1"
#define ANNOTATION_TRANSCRIPTS_2        "transcript_2"
#define ANNOTATION_NUM_SPLIT_READS      "num_split"
#define ANNOTATION_NUM_SPANNING_PAIRS   "num_pairs"
#define ANNOTATION_NUM_SPLIT_WITH_PAIR  "num_split_paired"
#define ANNOTATION_NUM_SPLITS_RESCUED   "num_splits_rescued"
#define ANNOTATION_SPLIT_POS_STATS      "split_pos_stats"
#define ANNOTATION_NUM_UNIQUE_STARTS    "num_unique_starts"
#define ANNOTATION_NUM_UNIQUE_SPLITS    "num_unique_split_reads"
#define ANNOTATION_HOMOLOGY_1           "homology_1"
#define ANNOTATION_HOMOLOGY_2           "homology_2"
#define ANNOTATION_LOCAL_HOMOLOGY       "local_homology"
#define ANNOTATION_HOMOLOGOUS_GENES     "homologous_genes"
#define ANNOTATION_INSERT_SIZE          "insert_size"
#define ANNOTATION_STRANDNESS           "strandness"
#define ANNOTATION_METACLUSTER_PROPS    "metacluster_properties"
#define ANNOTATION_FUSION_COVERAGE      "coverage"
#define ANNOTATION_SPLICE_TYPE          "splice_type"


struct FusionInterval : public GenomicInterval {
    int breakpointPos;
    Cluster::Direction direction;
    CharString sequence;
    std::set< boost::shared_ptr<Exon>, TranscriptNameSort > overalppingExons;
    std::set< boost::shared_ptr<Gene> > overlappingGenes;
    FusionInterval() {}
    FusionInterval(const ClusterPtr& cluster)  {
        const SimpleInterval& iv = cluster->getEncompassingInterval();
        this->refId = cluster->getRefId();
        this->begin = iv.begin;
        this->end = iv.end;
        this->isPositiveStrand = true;
        breakpointPos = cluster->getBreakpointPos();
        direction = cluster->getDirection();
        sequence = cluster->getSequence();
    }

    void updateSequence(int newBegin, int newEnd);

};


enum FusionType {
    FusionType_Unset,
    FusionType_InterChromosomal,
    FusionType_IntraChromosomal,
    FusionType_ReadThroughIntersect,
    FusionType_ReadThroughAdjacent
};

std::ostream& operator<<(std::ostream& os, const FusionInterval& iv);

struct BreakpointByName {
    bool operator()(const BreakpointCandidatePtr& l, const BreakpointCandidatePtr& r) {
        return l->readName < r->readName;
    }
};

class Fusion
{
    std::vector<BreakpointCandidatePtr> supportingBreakpoints;
    std::vector<FusionInterval> intervals;
    // TODO: reuse boost::variant and maybe some other stuff
    std::map<std::string,std::string> annotations;
    std::vector<int> firstSegmentIndexes;
    int idx;
    FusionType type;
    int score;

    static const string FT_UNSET;
    static const string FT_INTER_CHROMOSOMAL;
    static const string FT_INTRA_CHROMOSOMAL;
    static const string FT_READTHROUGH_INTERSECTING;
    static const string FT_READTHROUGH_ADJACENT;

    static const int DEFAULT_MATCH_BONUS = 2;

public:
    class Score {
        int numSupporters;
        int numLocalAlignments;
        int numRescuedAlignments;
    };

    Fusion() : idx(-1), type(FusionType_Unset), score(0) {}
    Fusion(int index) : idx(index), type(FusionType_Unset), score(0) {}

    void addBreakpoint(const BreakpointCandidatePtr& bp) {
        supportingBreakpoints.push_back(bp);
    }

    void clear() {
        supportingBreakpoints.clear();
        annotations.clear();
    }

    bool isEmpty() const {
        return supportingBreakpoints.size() == 0;
    }

    FusionType getType() const {
        return type;
    }

    void setType(FusionType newType) {
        type = newType;
    }

    const string& getTypeAsString() const;

    void setTypeFromString(const string& typeStr);

    const std::vector<BreakpointCandidatePtr>& getBreakpoints() const {return supportingBreakpoints; }

    const std::map<std::string,std::string>& getAnnotationsMap() const { return annotations; }

    int getBreakpointPos(size_t idx) const;
    const FusionInterval& getEncompassingInterval(size_t idx) const;
    FusionInterval& getEncompassingInterval(size_t idx);
    const CharString& getSequence(size_t idx) const;
    Cluster::Direction getDirection(size_t idx) const;

    void setIntervals(const std::vector<FusionInterval>& intervals ) {
        assert(intervals.size() >= 2);
        this->intervals = intervals;
    }

    int getNumLocalAlignments() const;

    size_t getNumSupporters() const { return supportingBreakpoints.size(); }

    void updateIntervals(bool updateSequence);

    const string& getAnnotationValue(const string& annotationName, const string& defaultValue = "") const {

        auto it = annotations.find(annotationName);
        if ( it != annotations.end()) {
            return it->second;
        } else {
            return defaultValue;
        }
    }

    bool hasAnnotation(const string& annotationName) const {
        return annotations.count(annotationName) > 0;
    }

    void setAnnotationValue(const string& annotationName, const string& value) {
        annotations[annotationName] = value;
    }

    void swapAnnotationValues(const string& name1, const string& name2) {

        string val1 = annotations.count(name1) > 0 ? annotations[name1] : "";
        string val2 = annotations.count(name2) > 0 ? annotations[name2] : "";

        if (val2.empty()) {
            annotations.erase(name1);
        } else {
            annotations[name1] = val2;
        }

        if (val1.empty()) {
            annotations.erase(name2);
        } else{
            annotations[name2] = val1;
        }

    }


    int getIndex() const {
        return idx;
    }

    void setIndex(int index) {
        idx = index;
    }

    int getScore() const { return score; }

    void calcScore(float splitBpWeight, float bridgeBpWeight, int matchBonus = DEFAULT_MATCH_BONUS );

    void sortSupportersByName() {
        sort(supportingBreakpoints.begin(), supportingBreakpoints.end(), BreakpointByName());
    }


    void removeBreakpoint(const string& readName);

    void removeBreakpoint(const BreakpointCandidatePtr& bp);

    static string getGeneStr(const string& geneAnnotation);

    static bool readFusionClusters(std::vector<Fusion>& results, std::istream& in, bool skipFusionsWithoutSequence);

    static bool readFusionClusters(std::vector<Fusion>& results, const string& filePath, bool skipFusionsWithoutSequence = false);

    static void writeSingleFusionCluster(const Fusion& f, std::ostream& outStream);

    static bool writeFusionClusters(const std::vector<Fusion>& fusions, const string& path, bool skipEmpty = true) {
        return writeFusionClusters(fusions, path.c_str(), skipEmpty);
    }

    static bool writeFusionClusters(const std::vector<Fusion>& fusions, const char* path, bool skipEmpty);

    static bool writeFusionClusters(const std::vector<Fusion>& fusions, std::ostream& outStream, bool skipEpmty);

    static bool writeFusionsReport(const std::vector<Fusion>& fusions, const string& outPath, bool filterResults);

    static bool writeDetailedFusionsReport( const std::vector<Fusion>& fusions, const string& outPath, bool filterResults );

    static bool writeFusionSeqsAsFasta(const std::vector<Fusion>& fusions, const string& outPath, size_t maxSeqLen = 100, bool writeRevCompl = false);

    static bool writeFusionTranscripts(const std::vector<Fusion>& fusions, const string& outPath, int sampleSize, bool insertSep );

    static bool writeHtmlReport(const std::vector<Fusion>& fusions, const string& path, bool filterResutls);


};

#endif // FUSION_H
