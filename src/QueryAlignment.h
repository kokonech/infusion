#ifndef QUERY_ALIGNMENT_H
#define QUERY_ALIGNMENT_H

#include <string>
#include <iostream>

#include "GenomicInterval.h"

// This class represents an alignment to reference
// which consist of a single block probably with several mismatches or a small indel



enum AlignmentStrand {
    AlignmentStrand_Forward,
    AlignmentStrand_Reverse,
    AlignmentStrand_Unknown
};


struct QueryAlignment
{

    std::string refId;
    int refStart;
    bool positiveStrand;
    int queryStart;
    // This is length in reference
    int length;
    int score;

    bool operator<(const QueryAlignment& other) const {

        if (refId == other.refId) {
            return refStart < other.refStart;
        } else {
            return refId < other.refId;
        }
    }

    QueryAlignment() :
        refStart(-1), positiveStrand(true), queryStart(-1), length(0), score(0)
    { }

    // TODO: this is wrong sometimes, since there length represents the lenght of alignment in reference
    int inQueryEnd() const { return queryStart + length  - 1; }
    int inRefEnd() const { return refStart + length - 1; }
    static void revertStrand(QueryAlignment& aln) {
        aln.positiveStrand = !aln.positiveStrand;
    }

    int getPointClosestToBreakpoint(bool breakpointDirRight) const {
        return breakpointDirRight ? refStart : inRefEnd();
    }

    GenomicInterval toGenomicInterval() const {
        return GenomicInterval(refId, refStart, refStart + length - 1);
    }

    char strandAsChar() const {
        return positiveStrand ? 'f' : 'r';
    }

};


std::ostream& operator<<(std::ostream& os, const QueryAlignment& aln);

#endif // QUERY_ALIGNMENT_H
