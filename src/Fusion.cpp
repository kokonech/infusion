#include <fstream>
#include "Fusion.h"
#include "StringUtils.h"
#include "InsertSizeDistr.h"
#include "AnalyzeClusterHomogeneityTask.h"
#include "FusionGeneAnnotation.h"

#include <seqan/modifier.h>
#include <boost/format.hpp>

using namespace std;

using boost::format;


const string Fusion::FT_UNSET("unset");
const string Fusion::FT_INTER_CHROMOSOMAL("inter-chromosomal");
const string Fusion::FT_INTRA_CHROMOSOMAL("intra-chromosomal");
const string Fusion::FT_READTHROUGH_INTERSECTING("readthrough-intersecting");
const string Fusion::FT_READTHROUGH_ADJACENT("readthrough-adjacent");



void FusionInterval::updateSequence(int newBegin, int newEnd) {

    int seqLen = seqan::length(sequence);

    if (seqLen == 0 || newBegin < begin || newEnd > end) {
        return;
    }

    int startOffset = newBegin - begin;
    int newLen = newEnd - newBegin + 1;
    auto newSeq = seqan::infix(sequence, startOffset, startOffset + newLen);
    sequence = CharString(newSeq);

}

const string &Fusion::getTypeAsString() const
{
    switch(type) {
    case FusionType_InterChromosomal:
        return FT_INTER_CHROMOSOMAL;
    case FusionType_IntraChromosomal:
        return FT_INTRA_CHROMOSOMAL;
    case FusionType_ReadThroughIntersect:
        return FT_READTHROUGH_INTERSECTING;
    case FusionType_ReadThroughAdjacent:
        return FT_READTHROUGH_ADJACENT;
    default:
        return FT_UNSET;
    }


}

void Fusion::setTypeFromString(const string &typeStr)
{
    if (typeStr == FT_INTER_CHROMOSOMAL) {
        type = FusionType_InterChromosomal;
    } else if (typeStr == FT_INTRA_CHROMOSOMAL) {
        type = FusionType_IntraChromosomal;
    } else if (typeStr == FT_READTHROUGH_ADJACENT) {
        type = FusionType_ReadThroughAdjacent;
    } else if (typeStr == FT_READTHROUGH_INTERSECTING) {
        type = FusionType_ReadThroughIntersect;
    } else {
        type = FusionType_Unset;
    }
}

int Fusion::getBreakpointPos(size_t idx) const
{
    assert( intervals.size() > idx );

    return intervals.at(idx).breakpointPos;
}


const FusionInterval& Fusion::getEncompassingInterval(size_t idx) const
{
    assert( intervals.size() > idx );
    return intervals.at(idx);

}

FusionInterval& Fusion::getEncompassingInterval(size_t idx)
{
    assert( intervals.size() > idx );
    return intervals[idx];

}


const CharString& Fusion::getSequence(size_t idx) const
{
    assert( intervals.size() > idx );
    return intervals.at(idx).sequence;
}

Cluster::Direction Fusion::getDirection(size_t idx) const
{
    assert( intervals.size() > idx);
    return intervals.at(idx).direction;
}

int Fusion::getNumLocalAlignments() const
{
    // TODO: cache the value
    int numLocalAlignments = 0;
    foreach_ (const BreakpointCandidatePtr& bp, supportingBreakpoints) {
        if (bp->isOriginLocal()) {
            numLocalAlignments++;
        }
    }

    return numLocalAlignments;
}


void Fusion::updateIntervals(bool updateSequence)
{

    // Assumption: the alignments in each breakpoint candidate are sorted according to intervals

    for (int i = 0; i < 2; ++i) {

        vector<int> localAlnPos;

        FusionInterval& prevIv = intervals[i];
        SimpleInterval interval(-1, 0);
        Cluster::Direction dir = prevIv.direction;

        foreach_ (const BreakpointCandidatePtr& bp, supportingBreakpoints) {

            if ( (i == 0 && bp->isFirstSeparatedByIntron()) || (i == 1 && bp->isSecondSeparatedByIntron() ) ) {
                continue;
            }

            const QueryAlignment& aln = bp->alns[i];
            assert( aln.refId == prevIv.refId);

            if (interval.begin == -1) {
                interval.begin = aln.refStart;
                interval.end = aln.inRefEnd();
            } else {
                interval.begin = aln.refStart < interval.begin ? aln.refStart : interval.begin;
                interval.end = aln.inRefEnd() > interval.end ? aln.inRefEnd() : interval.end;
            }

            //assert(interval.begin >= prevIv.begin && interval.end <= prevIv.end );

            prevIv.breakpointPos = dir == Cluster::DirectionRight ? interval.begin : interval.end;

            if (bp->isOriginLocal()) {
                localAlnPos.push_back( dir == Cluster::DirectionRight ? aln.refStart : aln.inRefEnd() );
            }

        }

        if (interval.begin == -1) {
            continue;
        }

        //TODO: copy-paste from clusterptr
        if (localAlnPos.size() > 2) {
            // We use median for fuzzy breakpoint estimation
            if (dir == Cluster::DirectionRight) {
                std::sort(localAlnPos.begin(), localAlnPos.end(), std::less<int>()  );
            } else {
                std::sort(localAlnPos.begin(), localAlnPos.end(), std::greater<int>()  );
            }
            prevIv.breakpointPos = localAlnPos[ localAlnPos.size() / 2 - 1 ];
        }

        if (updateSequence) {
            prevIv.updateSequence(interval.begin, interval.end);
        }

        prevIv.begin = interval.begin;
        prevIv.end = interval.end;
    }

}

void Fusion::calcScore( float splitBpWeight, float bridgeBpWeight, int matchBonus )
{
    score = 0;
    foreach_ (const BreakpointCandidatePtr& bp, supportingBreakpoints) {
        int bpScore = 0;
        size_t numLocalAlignments = bp->alns.size();
        for (size_t i = 0; i < numLocalAlignments; ++i) {
            const QueryAlignment& aln = bp->alns[i];
            if (aln.score == 0) {
                bpScore += aln.length * matchBonus;
            } else {
                bpScore += aln.score;
            }
        }
        if (bp->isOriginLocal()) {
            score += splitBpWeight*bpScore;
        } else if (bp->isOriginPaired()) {
            score += bridgeBpWeight*bpScore;
        }
    }

}

void Fusion::removeBreakpoint(const string &readName)
{
    int toRemoveIdx = -1;

    for (size_t i = 0, count = supportingBreakpoints.size(); i < count; ++i ) {
        if (supportingBreakpoints.at(i)->readName == readName) {
            toRemoveIdx = i;
            break;
        }
    }

    if (toRemoveIdx != -1) {
        // TODO: reuse cool idea
        // swap(pList[i], pList.back());
        // pList.pop_back();

        supportingBreakpoints.erase(supportingBreakpoints.begin() + toRemoveIdx);
    }

}

void Fusion::removeBreakpoint(const BreakpointCandidatePtr &bp)
{
    int toRemoveIdx = -1;

    for (size_t i = 0, count = supportingBreakpoints.size(); i < count; ++i ) {
        if (supportingBreakpoints.at(i) == bp) {
            toRemoveIdx = i;
            break;
        }
    }

    if (toRemoveIdx != -1) {
        // TODO: reuse cool idea
        // swap(pList[i], pList.back());
        // pList.pop_back();

        supportingBreakpoints.erase(supportingBreakpoints.begin() + toRemoveIdx);
    }

}



static bool parseRegion(const string& regionStr, FusionInterval& iv) {
    bool ok = true;
    auto items = StringUtils::split(regionStr, ',');
    assert(items.size() == 3);
    iv.begin = StringUtils::parseNumber<int>(items[0], ok);
    if (!ok) {
        return false;
    }
    iv.end = StringUtils::parseNumber<int>(items[1], ok);
    if (!ok) {
        return false;
    }

    const string& dir = items[2];
    if (!dir.length() == 1) {
        return false;
    }
    iv.direction = Cluster::directionFromChar(dir.at(0));


    return true;
}


static bool parseFusionInterval(const string& refId, const string& bpPosStr, const string& regionStr, FusionInterval& iv) {
    iv.refId = refId;
    bool ok = true;
    iv.breakpointPos = StringUtils::parseNumber<int>(bpPosStr, ok);

    if (!ok) {
        return false;
    }

    return parseRegion(regionStr, iv);
}

bool Fusion::readFusionClusters(std::vector<Fusion>& results, const string& filePath, bool skipFusionsWithoutSequence)
{
    if (filePath == "-") {
        return readFusionClusters(results, std::cin, skipFusionsWithoutSequence);
    } else {
        std::ifstream infile;

        infile.open(filePath);
        if ( !infile.is_open())  {
            return false;
        }
        return readFusionClusters(results, infile,skipFusionsWithoutSequence);
    }
}


bool Fusion::readFusionClusters(std::vector<Fusion>& results, istream& infile, bool skipFusionsWithoutSequence)
{

    Fusion f;
    vector<FusionInterval> intervals;
    string line;

    while (getline(infile,line)) {
        if (StringUtils::trim(line).length() == 0 || line[0] == '#' ) {
            continue;
        }
        if (StringUtils::startsWith( line, "[START" ) ) {
            const string substr = StringUtils::substrAfterPattern(line, "CLUSTER");
            const string clusterIdStr = StringUtils::substrBeforePattern( substr , "]" );
            bool ok = true;
            int idx = StringUtils::parseNumber<int>(clusterIdStr, ok);
            if (!ok) {
                cerr << "Failed to parser cluster start" << endl;
                return false;
            }
            f.setIndex(idx);
            f.clear();
            intervals.clear();
            intervals.resize(2);

        } else if ( StringUtils::startsWith(line, "[END") ) {
                f.setIntervals(intervals);
                if (skipFusionsWithoutSequence) {
                    if (length(f.getSequence(0)) == 0 || length(f.getSequence(1)) == 0) {
                        continue;
                    }
                }
                results.push_back(f);
        } else if ( StringUtils::startsWith(line, "sequence")) {
                auto seqItems = StringUtils::split(line, '\t');
                assert(seqItems.size() == 3);
                if (intervals.size() == 3) {
                    cerr << "Failed to parse sequence for cluster " << f.getIndex() << endl;
                    return false;
                }
                intervals[0].sequence = seqItems.at(1);
                intervals[1].sequence = seqItems.at(2);

        } else if ( StringUtils::startsWith(line, "fusion") ) {
            auto items = StringUtils::split(line, '\t');
            assert( items.size() > 6 );
            if (!parseFusionInterval(items[1], items[2], items[3], intervals[0]) ) {
                cerr << "Failed to parse first region for fusion " << f.getIndex() <<  endl;
                return false;
            }
            if (!parseFusionInterval(items[4], items[5], items[6], intervals[1]) ) {
                cerr << "Failed to parse second region for fusion " << f.getIndex() <<  endl;
                return false;
            }
            if (items.size() == 8) {
                f.setTypeFromString(items[7]);
            }

        } else if ( StringUtils::startsWith(line, "annotation") ) {
            auto items = StringUtils::split(line, '\t');
            assert(items.size() == 3);
            f.setAnnotationValue(items[1], items[2]);
        } else  {
            auto bp = BreakpointCandidate::parseCandidateRecord( line );
            assert(bp);
            if (bp) {
                f.addBreakpoint(bp);
            }
        }

    }

    return true;
}


#define TAB "\t"

std::ostream& operator<<(std::ostream& os, const FusionInterval& iv)
{
   os << "[" << iv.begin + 1 << "," << iv.end + 1 <<  "]";
   return os;
}


bool Fusion::writeDetailedFusionsReport(const vector<Fusion> &fusions, const string& outPath, bool filterResults) {

    ofstream out;
    out.open(outPath, ios_base::out);

    if (!out.is_open()) {
        return false;
    }

    out << "#id\tref1\tbreak_pos1\tregion1\tref2\tbreak_pos2\tregion2\tnum_split\tnum_paired\t";
    out << "num_split_with_pair\tnum_split_rescued\tnum_uniq_starts\tpap_rate\tmean_split_pos\tsplit_pos_std\t";
    out << "homogeneity\tcoverage_context\tssp\t";
    out << "fusion_class\tbreak_on_exon\t";
    out << "feature_1\tgene_1\ttranscript_1\tgene_1_strand\tbiotype_1\texpression_1\t";
    out << "feature_2\tgene_2\ttranscript_2\tgene_2_strand\tbiotype_2\texpression_2\t";
    out << "splice_motif\tfilters" << endl;

    foreach_ (const Fusion& f, fusions) {

        if (filterResults && f.hasAnnotation(ANNOTATION_FILTERS)) {
            continue;
        }

        const FusionInterval& iv1 = f.getEncompassingInterval(0);
        const FusionInterval& iv2 = f.getEncompassingInterval(1);

        GenomicAnnotation a1 = GenomicAnnotation::parseAnnotation(f.getAnnotationValue(ANNOTATION_GENE_1,""));
        GenomicAnnotation a2 = GenomicAnnotation::parseAnnotation(f.getAnnotationValue(ANNOTATION_GENE_2,""));
        string isAnn = f.getAnnotationValue(ANNOTATION_INSERT_SIZE);
        bool ok = false;
        float papRate = DistrParams::parsePapRateFromAnnotation(isAnn, ok);
        if (!ok) {
            papRate = 0;
        }
        MetaClusterAnnotation ann;
        MetaClusterAnnotation::fromStr(f.getAnnotationValue(ANNOTATION_METACLUSTER_PROPS), ann);

        string meanSplitPosStr = "0";
        string splitPosStdStr = "0.00e";
        string annStr = f.getAnnotationValue(ANNOTATION_SPLIT_POS_STATS);

        if (annStr.size() > 0) {
            auto items = StringUtils::split(annStr, ';');
            meanSplitPosStr = items.at(0);
            splitPosStdStr = items.at(1);
        }

        string sspStr = f.getAnnotationValue(ANNOTATION_STRANDNESS);
        string ssp1 = ".", ssp2 = ".";
        vector<string> sspItems = StringUtils::split(sspStr, ';');
        if (sspItems.size() == 2 ) {
            ssp1 = sspItems.at(0);
            ssp2 = sspItems.at(1);
        }

        string exonBreak1 = a1.breakOnExon ? "yes" : "no";
        string exonBreak2 = a2.breakOnExon ? "yes" : "no";

        string spliceMotif = f.getAnnotationValue(ANNOTATION_SPLICE_TYPE, "");

        if (spliceMotif.length() > 0) {
            spliceMotif = StringUtils::split(spliceMotif, ';').front();
        } else {
            spliceMotif = "0";
        }

        out <<  f.getIndex() << TAB << iv1.refId << TAB << iv1.breakpointPos + 1 << TAB << iv1  << TAB <<
                iv2.refId << TAB << iv2.breakpointPos + 1 << TAB << iv2 << TAB <<
                f.getAnnotationValue(ANNOTATION_NUM_SPLIT_READS) << TAB <<
                f.getAnnotationValue(ANNOTATION_NUM_SPANNING_PAIRS) << TAB <<
                f.getAnnotationValue(ANNOTATION_NUM_SPLIT_WITH_PAIR) << TAB <<
                f.getAnnotationValue(ANNOTATION_NUM_SPLITS_RESCUED) << TAB <<
                f.getAnnotationValue(ANNOTATION_NUM_UNIQUE_STARTS, "0") << TAB <<
                papRate << TAB << meanSplitPosStr << TAB << splitPosStdStr << TAB <<
                ann.w1 << "|" << ann.w2 << TAB <<
                f.getAnnotationValue(ANNOTATION_FUSION_COVERAGE, "-1;-1") << TAB <<
                ssp1 << "|" << ssp2 << TAB <<
                f.getTypeAsString() << TAB << exonBreak1 << "|" << exonBreak2 << TAB <<
                a1.featureType <<TAB<< a1.getGenesStr() <<TAB<< a1.getTranscriptsStr() <<TAB<< a1.getStrand() <<TAB<<
                a1.getBioTypesStr() <<TAB<< f.getAnnotationValue(ANNOTATION_EXPR_1, "0.0") <<TAB<<
                a2.featureType <<TAB<< a2.getGenesStr() <<TAB<< a2.getTranscriptsStr() <<TAB<< a2.getStrand() <<TAB<<
                a2.getBioTypesStr() <<TAB<< f.getAnnotationValue(ANNOTATION_EXPR_2, "0.0") <<TAB<<
                spliceMotif <<TAB<<
                f.getAnnotationValue(ANNOTATION_FILTERS, "pass") << endl;
    }

    return true;

}


bool Fusion::writeFusionsReport(const vector<Fusion> &fusions, const string& outPath, bool filterResults)
{
    ofstream outfile;
    outfile.open(outPath, ios_base::out);

    if (!outfile.is_open()) {
        return false;
    }

    outfile << "#id\tref1\tbreak_pos1\tregion1\tref2\tbreak_pos2\tregion2\tnum_span\tnum_paired\tgenes_1\tgenes_2\tfusion_class" << endl;

    foreach_ (const Fusion& f, fusions ) {
        if (filterResults && f.hasAnnotation(ANNOTATION_FILTERS)) {
            continue;
        }
        outfile << f.getIndex() << "\t";
        const FusionInterval& iv1 = f.getEncompassingInterval(0);
        outfile << iv1.refId << "\t" << iv1.breakpointPos + 1 << "\t" << iv1 << "\t";
        const FusionInterval& iv2 = f.getEncompassingInterval(1);
        outfile << iv2.refId << "\t" << iv2.breakpointPos + 1 << "\t" << iv2 << "\t";

        GenomicAnnotation a1 = GenomicAnnotation::parseAnnotation( f.getAnnotationValue(ANNOTATION_GENE_1, "") );
        GenomicAnnotation a2 = GenomicAnnotation::parseAnnotation( f.getAnnotationValue(ANNOTATION_GENE_2, "") );

        outfile << f.getAnnotationValue(ANNOTATION_NUM_SPLIT_READS) << "\t";
        outfile << f.getAnnotationValue(ANNOTATION_NUM_SPANNING_PAIRS) << "\t";
        outfile << a1.getGenesStr() << "\t";
        outfile << a2.getGenesStr() << "\t";
        outfile << f.getTypeAsString() << endl;
    }

    return true;

}


void Fusion::writeSingleFusionCluster(const Fusion &fusion, std::ostream &out) {

    out << "[START CLUSTER " << fusion.getIndex() << "]" << endl;

    auto left = fusion.getEncompassingInterval(0);
    auto right = fusion.getEncompassingInterval(1);

    out << "fusion\t";
    out << left.refId << "\t" << fusion.getBreakpointPos(0) << "\t" << left.begin << "," << left.end << "," << Cluster::directionToChar(left.direction);
    out << "\t" << right.refId << "\t" << fusion.getBreakpointPos(1) << "\t" <<  right.begin << "," << right.end << "," << Cluster::directionToChar(right.direction);
    if (fusion.getType() != FusionType_Unset) {
        out << "\t" << fusion.getTypeAsString();
    }
    out << endl;

    const std::map<std::string,std::string>& annotations = fusion.getAnnotationsMap();

    for (auto iter = annotations.begin(); iter != annotations.end(); ++iter) {
        out << "annotation\t" << iter->first << "\t" << iter->second << endl;
    }


    if (length(fusion.getSequence(0)) > 0 && length(fusion.getSequence(1)) > 0 ) {
        if (left.length() >= 1000000) {
            out << "comment\tThe upstream cluster sequence is larger than 1MB, skipping output..." << endl;
        } else if (right.length() >= 1000000) {
            out << "comment\tDownstream cluster sequence is larger than 1MB, skipping output..." << endl;
        } else {
            out << "sequence\t" << fusion.getSequence(0) << "\t" << fusion.getSequence(1) << endl;
        }
    }


    auto breakpoints = fusion.getBreakpoints();
    foreach_( const BreakpointCandidatePtr& c, breakpoints ) {
        BreakpointCandidate::writeCandidate(c, out);

    }
    out << "[END CLUSTER]" << endl;

}



bool Fusion::writeFusionClusters(const std::vector<Fusion>& fusions, std::ostream& out, bool skipEmpty) {

    for (size_t i = 0, sz = fusions.size(); i < sz; ++i ) {
        const Fusion& fusion = fusions.at(i);

        if (fusion.isEmpty() && skipEmpty) {
            continue;
        }

        writeSingleFusionCluster(fusion, out);

    }

    return true;

}

bool Fusion::writeFusionClusters( const std::vector<Fusion>& fusions, const char* outfile, bool skipEmpty) {

    if (strcmp(outfile, "-") == 0) {
        return writeFusionClusters(fusions, std::cout, skipEmpty);
    } else {
        ofstream out;
        out.open(outfile, ios_base::out);

        if (!out.is_open()) {
            return false;
        }
        return writeFusionClusters(fusions, out, skipEmpty);
    }

}

#define MIN_LENGTH  10

seqan::CharString getLimitedSequence(const CharString& seq,  Cluster::Direction dir, size_t maxLen) {
    using namespace seqan;
    CharString result;
    if (length(seq) <= maxLen) {
        result = seq;
    } else {
        if (length(seq) > maxLen) {
            if (dir == Cluster::DirectionLeft) {
                result = suffix(seq, length(seq) - maxLen);
            } else {
                result = prefix(seq, maxLen);
            }
        }

    }

    return result;
}

bool Fusion::writeFusionSeqsAsFasta(const vector<Fusion> &fusions, const string &outPath, size_t maxLen, bool writeRevCompl) {

    using namespace seqan;
    ofstream out;
    out.open(outPath, ios_base::out);

    if (!out.is_open()) {
        return false;
    }

    foreach_ (const Fusion& f, fusions) {

        if (f.isEmpty()) {
            continue;
        }

        CharString seq1 = getLimitedSequence(f.getSequence(0),f.getDirection(0), maxLen);
        if (length(seq1) >= MIN_LENGTH) {
            out << ">" << f.getIndex() << "/1" << endl;
            out << seq1 << endl;
            if (writeRevCompl){
                seqan::reverseComplement(seq1);
                out << ">" << f.getIndex() << "_rev_comp/1" << endl;
                out << seq1 << endl;
            }
        }



        CharString seq2 = getLimitedSequence(f.getSequence(1),f.getDirection(1), maxLen);
        if (length(seq2) >= MIN_LENGTH) {
            out << ">" << f.getIndex() << "/2" << endl;
            out << seq2 << endl;
            if (writeRevCompl){
                reverseComplement(seq2);
                out << ">" << f.getIndex() << "_rev_comp/2" << endl;
                out << seq2 << endl;
            }
        }
    }

    return true;

}


string Fusion::getGeneStr(const string& geneAnnotation) {
    GenomicAnnotation ann = GenomicAnnotation::parseAnnotation(geneAnnotation);
    return ann.getGenesStr();

}

bool Fusion::writeFusionTranscripts(const vector<Fusion> &fusions, const string &outPath, int sampleSize, bool insertSep)
{
    using namespace seqan;
    ofstream out;
    out.open(outPath, ios_base::out);

    if (!out.is_open()) {
        cerr << "Failed to start writing fusion transcripts" << endl;
        return false;
    }

    foreach_ (const Fusion& f, fusions) {

        if (f.isEmpty()) {
            continue;
        }

        CharString seq1 = f.getSequence(0);
        int len1 = length(seq1);
        if (len1 == 0 ) {
            continue;
        }

        if (f.getDirection(0) == Cluster::DirectionRight) {
            reverseComplement(seq1);
        }

        CharString seq2 = f.getSequence(1);
        int len2 = length(2);
        if (len2 == 0) {
            continue;
        }

        if (f.getDirection(1) == Cluster::DirectionLeft) {
            reverseComplement(seq2);
        }

        if (sampleSize > 0) {
            if (len1 > sampleSize) {
                seq1 = suffix(seq1, length(seq1) - sampleSize);
            }
            if (len2 > sampleSize) {
                seq2 = prefix(seq2, sampleSize);
            }
        }

        string gene1 = getGeneStr(f.getAnnotationValue(ANNOTATION_GENE_1));
        string gene2 = getGeneStr(f.getAnnotationValue(ANNOTATION_GENE_2));

        out << ">" << f.getIndex() << " size=" << len1 + len2 << " gene1=" << gene1 << " gene2=" << gene2 << endl;

        out << seq1;
        if (insertSep) {
            out << "\t";
        }
        out << seq2 << endl;

    }

    return true;

}



bool Fusion::writeHtmlReport(const vector<Fusion> &fusions, const string &outPath, bool filterResults)
{
    ofstream out;
    out.open(outPath, ios_base::out);

    if (!out.is_open()) {
        return false;
    }

    out << "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Strict//EN\">" << endl;

    out << "<html>";
    out << "<head><title>InFusion Report</title>" << endl;

    foreach_ (const Fusion& f, fusions) {
        if (filterResults && f.hasAnnotation(ANNOTATION_FILTERS)) {
            continue;
        }

        CharString seq1(f.getSequence(0));
        CharString seq2(f.getSequence(1));



        out << "Seq1:" << seq1 << endl;

        out << "Seq2:" << seq2 << endl;



    }


    return true;


}


