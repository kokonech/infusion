#include <iostream>

#include <seqan/arg_parse.h>

#include "CleanupFusionClusters.h"

using namespace seqan;
using namespace std;

#define PROGRAM_NAME    "cleanup_clusters"

void setupArgumentParser( ArgumentParser& parser )
{
    setVersion(parser, INFUSION_VERSION);


    addDescription(parser, "This tool is part of the InFusion pipeline. It removes discordant reads from the cluster");

    string usageLine;
    usageLine.append(" [OPTIONS] -i fusionClusters.txt -o updatedClusters.txt");
    addUsageLine(parser, usageLine);

    addSection(parser, "Options");

    vector<ArgParseOption> options;

    options.push_back( ArgParseOption( "i", "input", "Path to input fusion clusters. Use - to for standard input.", ArgParseArgument::STRING ));
    options.push_back(ArgParseOption( "o", "output", "Path to output updated fusion clusters. Use - to for standard output", ArgParseArgument::STRING ));

    foreach_ (ArgParseOption& opt, options) {
        setRequired(opt, true);
        addOption(parser, opt);
    }

    addOption(parser, ArgParseOption( "d", "max-dist", "Max distance to breakpoint position to consider a rescued read as valid.", ArgParseArgument::INTEGER ));

    addTextSection(parser, "References");
    addText(parser, "Konstantin Okonechnikov <okonechnikov@mpiib-berlin.mpg.de>");
}

int main(int argc, char const ** argv)
{

    ArgumentParser parser(PROGRAM_NAME);
    setupArgumentParser(parser);

    ArgumentParser::ParseResult res = parse(parser, argc, argv);
    if (res != ArgumentParser::PARSE_OK) {
        return 1;
    }

    CleanupFusionClustersConfig cfg;

    getOptionValue(cfg.inputPath, parser, "i");
    getOptionValue(cfg.outputPath, parser, "o");
    getOptionValue(cfg.maxDistToBreakpoint, parser, "d");

    CleanupFusionClustersTask task(cfg);

    return task.run();

}





