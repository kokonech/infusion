# this script's home dir is trunk/install

PRODUCT_NAME="InFusion"
VERSION=`cat ../infusion.version`
RELEASE_DIR=../build/Release
OUT_DIR=_release
APP_DIR_NAME="${PRODUCT_NAME}-${VERSION}"
TARGET_APP_DIR=$OUT_DIR/$APP_DIR_NAME
EXTERNAL_BIN_DIR=$TARGET_APP_DIR/external
EXTERNAL_COMPARISON_DIR=$TARGET_APP_DIR/comparison

BOWTIE2_PATH=/home/okonechn/tools/bowtie2-2.0.5
SAMTOOLS_PATH=/home/okonechn/tools/samtools-1.2

echo cleaning previous bundle
rm -rf ${TARGET_APP_DIR}
rm -rf $OUT_DIR/*.tar.gz
mkdir -p $TARGET_APP_DIR

SKIP_INFUSION_BUILD=1

if [ -z $SKIP_INFUSION_BUILD ]
then
    echo
    echo building latest version
    pushd .
    cd $RELEASE_DIR
    make clean
    cmake ../../src -DCMAKE_BUILD_TYPE=Release 
    make -j4
    popd
fi

echo
echo copying external binaries

mkdir -p $EXTERNAL_BIN_DIR
cp -v $BOWTIE2_PATH/bowtie2 "$EXTERNAL_BIN_DIR"
cp -v $BOWTIE2_PATH/bowtie2-align "$EXTERNAL_BIN_DIR"
cp -v $BOWTIE2_PATH/bowtie2-build "$EXTERNAL_BIN_DIR"
cp -v $SAMTOOLS_PATH/samtools "$EXTERNAL_BIN_DIR"


echo
echo copying comparison scripts

mkdir -p $EXTERNAL_COMPARISON_DIR
cp -v ../comparison/run_comparison.py "$EXTERNAL_COMPARISON_DIR"
cp -v ../comparison/compare_with_design.py "$EXTERNAL_COMPARISON_DIR"
cp -v ../comparison/compare_samples.py "$EXTERNAL_COMPARISON_DIR"

echo
echo copying InFusion binaries

INFUSION_TOOLS="analyze_local_alignments analyze_paired_alignments cluster_candidates
    merge_mate_alignments build_fasta_index filter_fusion_candidates output_fusion_alignments 
    find_paired_mates filter_repeat_alignments resolve_multimapped dump_clusters
    join_intron_separated_fusions annotate_fusions extract_fusion_sequences
    remove_homologs analyze_cluster_homogeneity cleanup_clusters compute_read_stats
    analyze_fusion_coverage"

for toolname in $INFUSION_TOOLS
do
    cp -v $RELEASE_DIR/$toolname "$TARGET_APP_DIR"
done

cp -v $RELEASE_DIR/libcore.so "$TARGET_APP_DIR"


cp -v ../infusion $TARGET_APP_DIR
cp -v ../infusion.version $TARGET_APP_DIR
cp -v ../fusion.py $TARGET_APP_DIR
cp -v ../setup_reference_dataset.py $TARGET_APP_DIR
cp -v ../argparse.py $TARGET_APP_DIR

echo
echo copying README file
cp -v ../README $TARGET_APP_DIR
cp -v ../HISTORY $TARGET_APP_DIR

echo
echo copying LICENSE file
cp -v ../LICENSE "$TARGET_APP_DIR"

PACKAGE_TYPE="linux"

ARCH=`uname -m`

PACKAGE_NAME="$PRODUCT_NAME"-"$VERSION"-"$PACKAGE_TYPE"-"$ARCH"

cd $OUT_DIR
tar -cf $PACKAGE_NAME.tar $APP_DIR_NAME/
gzip -v $PACKAGE_NAME.tar
cd ..

rm -rf $TARGET_APP_DIR

echo 
echo done creating bundle
echo
