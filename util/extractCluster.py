import argparse
import sys

def parseIds(filterFile):
    ids = []
    file = open(filterFile)
    for line in file:
        if line.startswith("#") or not line.strip():
            continue

        cluster_id = int( line.split()[0] )
        ids.append(cluster_id)

    return ids

if __name__ == "__main__":


    descriptionText = "The script extracts and output a specific cluster"
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("clustersFile", help="The file with clusters")

    parser.add_argument("-n", dest="clusterIds",  default="", help="Cluster numbers to extract.")

    parser.add_argument("-f", dest="filterFile",  default="",
                        help="File with which contains the ids to analyze in the first column. For example: fusions.txt")

    args = parser.parse_args()

    outputLines = False
    out_stream = sys.stdout

    ids = []

    if args.clusterIds:
        ids = [ int(fId) for fId in args.clusterIds.split(",") ]
    elif args.filterFile:
        ids = parseIds(args.filterFile)


    for line in open(args.clustersFile):
        if line.startswith("[START CLUSTER"):
            clusterNumber = int(line.split("CLUSTER")[-1].split("]")[0])
            if clusterNumber  in ids:
                outputLines = True

        if outputLines:
            out_stream.write(line)
            if line.startswith("[END CLUSTER"):
                outputLines = False
