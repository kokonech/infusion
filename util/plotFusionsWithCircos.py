__author__ = 'okonechnikov@mpiib-berlin.mpg.de'

import os
import sys

dirpath = os.path.dirname(__file__)
sys.path.append( os.path.join(dirpath, "../comparison") )

import run_comparison
import argparse
import shutil
#import subprocess


def runCircos(outDir):
    
    print "Running circos in " + os.path.abspath(outDir) 
    print
    os.chdir(outDir)
    cmd =  "circos --no-svg --conf circos.conf"
    os.system( cmd )





if __name__ == "__main__":

    descriptionText = "The script creates circos configuration and runs it for fusion results. More about circos here: http://circos.ca/"

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("fusionsFile", help="Fusion discovery tool output")

    parser.add_argument("--out", default="", dest="reportDir", help="directory with circos configuration file. Default: fusions_circos_plot")
    parser.add_argument("--tool", default="infusion", dest="toolId", help="Fusion file format. Default: infusion")
    args = parser.parse_args()

    tool = run_comparison.createTool(args.toolId)
    
    sys.stderr.write( "Loading %s results\n" % tool.name )
    tool.expectedFusionsOutput = args.fusionsFile
    fusions = tool.getFusionsOutput()
    
    sys.stderr.write( "Setup output dir\n")
    outDir = args.reportDir

    if not outDir:
        outDir = "fusions_circos_plot"

    if not os.path.exists(outDir):
        os.mkdir(outDir)
    
    linksFile = open(outDir + "/" + "links.txt", "w")

    for f in fusions:
        linksFile.write("hs%s %d %d " % (f.i1.ref,f.i1.startPos,f.i1.endPos ) )
        linksFile.write("hs%s %d %d\n" % (f.i2.ref,f.i2.startPos,f.i2.endPos ) )
    linksFile.close()

    shutil.copyfile(dirpath + "/circos_config/circos.conf", outDir + "/" + "circos.conf")
    shutil.copyfile(dirpath + "/circos_config/ideogram.conf", outDir + "/" + "ideogram.conf")
    shutil.copyfile(dirpath + "/circos_config/ticks.conf", outDir + "/" + "ticks.conf")
       
    runCircos( outDir  )




    


