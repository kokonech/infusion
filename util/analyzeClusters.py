import argparse
import sys
import os
from scipy import stats
import numpy as np


dirpath = os.path.dirname(__file__)
sys.path.append( os.path.join(dirpath, "..") )
import fusion

import pylab

SINGLE_END = 41
PAIRED_END = 44

MIN_KS_SAMPLE_SIZE = 6
MAX_READ_SIZE = 120

class ReadData:
    def __init__(self):
        self.id = ""
        self.location = []


def plot_reads(cluster_name, plot_caption, fusion_border, read_ids, location_db, freq_db, out_dir, draw_legend):
    labels_done = []
    read_info = []
    for read_id in read_ids:
        location = location_db[read_id]
        freq = freq_db[read_id]
        read_info.append( (location, freq) )
    read_info.sort(reverse=True)
    min_freq = min(l[-1] for l in read_info)
    max_freq = max(l[-1] for l in read_info)
    freq_cmap = pylab.get_cmap('Blues')
    pylab.clf()
    for rindex, (location, freq) in enumerate(read_info):
        if max_freq != min_freq:
            freq_percent = float(freq - min_freq) / float(max_freq - min_freq)
        else:
            freq_percent = 1
        color = freq_cmap(freq_percent)
        if freq in [min_freq, max_freq]:
            label = "Frequency: %s" % (freq)
        else:
            label = None
        if label and label not in labels_done:
            labels_done.append(label)
        else:
            label = None
        if 2 == len(location):
            start,end = location
            pylab.barh(rindex, end - start, left=start, height=0.8, color=color, label=label)
        elif 4 == len(location):
            start1,end1,start2,end2 = location
            pylab.barh(rindex, end1 - start1, left=start1, height=0.8, color=color, label=label)
            pylab.barh(rindex, end2 - start2, left=start2, height=0.8, color=color)
            pylab.hlines(rindex + 0.4,end1,start2)
        else:
            assert 0, "Wrong location!"

    pylab.axvline(fusion_border)
    pylab.title(plot_caption)
    pylab.xlabel("Coordinates")
    pylab.ylabel("Reads")
    if draw_legend:
        pylab.legend(loc='upper right')
    pylab.yticks(())
    out_file = os.path.join(out_dir, "%s.png" % (cluster_name) )
    pylab.savefig(out_file)
    #pylab.show()


def getFusionsById(fusions, clusterIds):

    if not clusterIds:
        return fusions

    result = []


    for f in fusions:
        if f.id in clusterIds:
            result.append(f)

    return result


def addRead(locations, read_start, read_end, read_data ):

    assert type(read_start) == int
    assert type(read_end) == int
    assert read_start < read_end
    assert read_start >= 0, "Read [%s,%d,%d] is out of bounds " % (read_data,read_start,read_end)

    if (read_start,read_end) in locations:
        locations[ (read_start,read_end)].append( read_data )
    else:
        locations[ (read_start,read_end) ] = [ read_data ]


def plot_coverage(cluster_name, plot_caption, coverage, fusion_border, out_dir):
    pylab.clf()

    x = np.arange( len(coverage) )

    pylab.title(plot_caption)
    pylab.axvline(fusion_border)
    pylab.xlabel("Coordinates")
    pylab.ylabel("Coverage")

    pylab.plot(x, coverage, color='g')
    pylab.fill_between(x, coverage, color='r')

    out_file = os.path.join(out_dir, "%s_coverage.png" % (cluster_name) )
    pylab.savefig(out_file)


def charcterizeDistr(d):

    mean = np.mean(d)
    std = np.std(d)
    p25 = np.percentile(d, 25)
    p50 = np.percentile(d, 50)
    p75 = np.percentile(d, 75)

    return (mean, std, p25, p50, p75)


def testKSOnSubsample(expected_insert_size):
    print "Testing Kolmogorov-Smirnov on subsamples..."

    for i in range(10):
        sample_indexes = np.random.randint(0, len(expected_insert_size),10)
        sample = [ expected_insert_size[idx] for idx in  sample_indexes ]
        isD,isPval = stats.ks_2samp(sample, expected_insert_size)
        print "Sample %d -> D=%f, p-value=%f" % ( i, isD, isPval )



def loadInsertSizeDistr(isDistrFile):

    expected_insert_size = []
    insert_size = []
    cur_is = 0

    for line in open(isDistrFile):
        count = int(line)
        if count:
            insert_size += [cur_is]*count
        cur_is += 1

    features = charcterizeDistr(insert_size)
    print "Original distribution: mean=%f, std=%f, p25=%f, p50=%f, p75=%f" % features

    for val in insert_size:
        if val < features[3]*2:
            expected_insert_size.append(val)

    features2 = charcterizeDistr(expected_insert_size)
    print "Modified distribution: mean=%f, std=%f, p25=%f, p50=%f, p75=%f" % features2


    return (expected_insert_size,features2)

WINDOW_SIZE = 50

def getUniquePosProportion(numSpan, read_starts, read_ends):
    uniqReadStarts = 0
    uniqReadEnds = 0

    if numSpan > 0:
        srs = set(read_starts)
        sre = set(read_ends)
        uniqReadStarts = len(srs) / float(numSpan)
        uniqReadEnds = len(sre) / float(numSpan)

    return uniqReadStarts, uniqReadEnds



def ksTestSpanningAlignments(spanning_read_starts, read_sizes):
    D = 1.0
    pval = 0.0042
    if len(spanning_read_starts) > MIN_KS_SAMPLE_SIZE:
        mean_read_size = np.mean(read_sizes)
        #print mean_read_size
        a = len(f.i1) - mean_read_size
        a = 0 if a < 0 else a
        b = len(f.i1) + mean_read_size
        if a != b:
            normalized_read_starts = [ (val - a) / float( b - a ) for val in spanning_read_starts ]
            D,pval = stats.kstest(normalized_read_starts, "uniform")

    return D,pval

def processFusion(f, outDir, resultingStats, expected_insert_size, visualize, only_split_reads, no_legend):

    locations = {}

    spanning_read_starts = []
    spanning_read_ends = []
    insert_size = []
    numPAP = 0
    numSpan = 0

    read_sizes = []

    if expected_insert_size:
        expected_is_distr = expected_insert_size[0]
        (expected_mean, expected_std, expected_p25, expected_median, expected_p75) = expected_insert_size[1]
    else:
        expected_is_distr = []
        (expected_mean, expected_std, expected_p25, expected_median, expected_p75) = (0,0,0,0,0)

    cov1 = [0] * WINDOW_SIZE
    cov2 = [0] * WINDOW_SIZE

    min_anchor = 3
    split_positions = []

    print "Analyzing fusion ", f.id

    for bp_name, bp_type, aln1, aln2 in f.breakpoints:

        if bp_type == fusion.BP_SPAN:
            read_start = aln1.startPos - f.i1.startPos
            read_end = aln2.startPos - f.i2.startPos + len(aln2)

            split_position = ( len(aln1) - min_anchor ) / float( len(aln1) + len(aln2) - 2*min_anchor )
            split_positions.append(split_position)

            if f.i1.dir == fusion.FUSION_DIRECTION_DOWNSTREAM:
                end_pos = read_start + len(aln1)
                read_start = len(f.i1) - end_pos

            if f.i2.dir == fusion.FUSION_DIRECTION_UPSTREAM:
                start_pos = read_end - len(aln2)
                read_end = len(f.i2) - start_pos

            window_start = len(f.i1) - WINDOW_SIZE
            start_idx = read_start - window_start
            if start_idx < 0:
                start_idx = 0

            end_idx = read_end if read_end < WINDOW_SIZE else WINDOW_SIZE

            read_end += len(f.i1)

            if read_end - read_start >= MAX_READ_SIZE:
                print "WARNING: breakpoint spanning read %s is too long [l=%d]" % (bp_name, read_end - read_start )

                aln1_end = read_start + len(aln1)
                if f.i1.dir == fusion.FUSION_DIRECTION_DOWNSTREAM:
                    aln1_end = read_start + len(aln1)

                aln2_start = aln2.startPos - f.i2.startPos
                read_end = aln2_start + len(aln2)

                if f.i2.dir == fusion.FUSION_DIRECTION_UPSTREAM:
                    aln2_start = len(f.i2) - read_end
                    read_end = aln2_start + len(aln2)

                aln2_start += len(f.i1)
                read_end += len(f.i1)

                read = ReadData()
                read.id = bp_name
                read.location = [read_start, aln1_end, aln2_start, read_end]

                fragment_start = min (read_start,aln2_start)
                fragment_end= max(aln1_end, read_end )

                addRead(locations, fragment_start, fragment_end, read)

            else:
                for i in range(start_idx, WINDOW_SIZE):
                    cov1[i] += 1

                for i in range(0, end_idx):
                    cov2[i] += 1

                read = ReadData()
                read.id = bp_name
                read.location = [read_start, read_end]

                spanning_read_starts.append(read_start)
                spanning_read_ends.append(read_end)

                addRead(locations, read_start, read_end, read)
                read_sizes.append(read_end - read_start)

                numSpan += 1

        else:

            if only_split_reads:
                continue

            read1_start = aln1.startPos - f.i1.startPos
            aln1_end = read1_start + len(aln1)

            if f.i1.dir == fusion.FUSION_DIRECTION_DOWNSTREAM:
                read1_start = len(f.i1) - aln1_end
                aln1_end = read1_start + len(aln1)

            aln2_start = aln2.startPos - f.i2.startPos
            read2_end = aln2_start + len(aln2)
            if f.i2.dir == fusion.FUSION_DIRECTION_UPSTREAM:
                aln2_start = len(f.i2) - read2_end
                read2_end = aln2_start + len(aln2)


            aln2_start += len(f.i1)
            read2_end += len(f.i1)

            read = ReadData()
            read.id = bp_name
            read.location = [read1_start, aln1_end, aln2_start, read2_end]

            if read1_start < aln2_start:
                pair_is = aln2_start - aln1_end
            else:
                pair_is = read1_start - read2_end

            #print read.id,pair_is
            insert_size.append( pair_is )

            if pair_is >= expected_p25 and pair_is <= expected_p75:
                numPAP += 1


            fragment_start = min (read1_start,aln2_start)
            fragment_end= max(aln1_end, read2_end )

            addRead(locations, fragment_start, fragment_end, read)

    read_ids = []
    location_db = {}
    freq_db = {}


    coverage = np.zeros( len(f.i1) + len(f.i2))

    for (start_pos,end_pos),reads in locations.iteritems():
        read_id = reads[0].id
        read_ids.append(read_id)
        location_db[read_id] = reads[0].location
        freq_db[read_id] = len(reads)
        coverage[ start_pos : end_pos ] += 1


    uniqStartsProp, uniqEndsProp = getUniquePosProportion(numSpan, spanning_read_starts, spanning_read_ends)

    #D, pval = ksTestSpanningAlignments(spanning_read_starts, read_sizes)
    splitPosD, splitPosPval = stats.kstest(split_positions, "uniform") if len(split_positions) > 5 else (1,1)
    splitPosMean = np.mean(split_positions)
    splitPosStd = np.std(split_positions)

    if expected_is_distr and len(insert_size) >= MIN_KS_SAMPLE_SIZE:
        isD,isPval = stats.ks_2samp(insert_size, expected_is_distr)
    else:
        isD,isPval = [1,0]

    clusterId = str(f.id)
    plot_caption =  "Fusion %s [ %s:%d-%d | %s:%d-%d ]" % (clusterId,f.i1.ref,f.i1.startPos,f.i1.endPos,
                                                                    f.i2.ref,f.i2.startPos,f.i2.endPos)
       
    if read_ids:
        if visualize:
            draw_legend = False if no_legend else True
            plot_reads(clusterId, plot_caption, len(f.i1), read_ids, location_db, freq_db, outDir, draw_legend)
            plot_coverage(clusterId, plot_caption, coverage, len(f.i1), outDir)
    else:
        print "ERROR: Cluster %d has 0 supporting reads!" % f.id

    numPaired = len(insert_size)

    PAPRate = float(numPAP) / len(insert_size) if numPaired > 0 else 0

    #if numPaired > 0:
    #    print "Observed IS in %d: N=%d, mean=%f, p25=%f, median=%f, p75=%f, PAPRate=%f" % \
    #          ( f.id, len(insert_size), np.mean(insert_size), np.percentile(insert_size, 25),
    #            np.median(insert_size), np.percentile(insert_size, 75), PAPRate)

    meanCov1 = sum(cov1) / float(WINDOW_SIZE)
    meanCov2 = sum(cov2) / float(WINDOW_SIZE)
    spanProp = 1
    if meanCov1 > 0 or meanCov2 > 0:
        spanProp =  min(meanCov1, meanCov2) / max(meanCov1,meanCov2)

    resultingStats.append ( [f.id, len(f.breakpoints), splitPosMean, splitPosStd,isD, isPval, PAPRate, meanCov1, meanCov2,
                             spanProp, uniqStartsProp, uniqEndsProp ]  )


def parseIds(filterFile):
    ids = []

    for line in open(filterFile):
        if line.startswith("#") or not line.strip():
            continue

        cluster_id = int( line.split()[0] )
        ids.append(cluster_id)

    return ids


if __name__ == "__main__":


    descriptionText = "The script analyzes and visualizes the alignment of reads of a given cluster. " \
                      "Visualization based on " \
                      "http://bcbio.wordpress.com/2009/04/29/finding-and-displaying-short-reads-clustered-in-the-genome/"
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("clustersFile", help="The file with clusters")

    parser.add_argument("-n", dest="clusterIds",  default="",
                        help="Clusters to visualize, ids separated by comma. By default all clusters are analyzed.")

    parser.add_argument("-f", dest="filterFile",  default="",
                        help="File with which contains the ids to analyze in the first column. For example: fusions.txt")

    parser.add_argument("--no-visualization", dest="noVisualization",  action="store_true", default=False,
                        help="Only report results")

    parser.add_argument("--no-legend", dest="noLegend", action="store_true", default=False, help="Skip legend for coverage plot")

    parser.add_argument("--old-mode", dest="oldMode",  action="store_true", default=False,
                        help="Compatibility with eariler format (no fusion border sorting)")
    
    parser.add_argument("--split-reads-only", dest="onlySplitReads",  action="store_true", default=False,
                        help="Plot only split reads coverage")

    parser.add_argument("-is", dest="insertSizeDist",  default="",
                        help="Insert size distribution")



    args = parser.parse_args()

    ids = []
    if args.clusterIds:
        ids = [ int(fId) for fId in args.clusterIds.split(",") ]
    elif args.filterFile:
        ids = parseIds(args.filterFile)

    fusions = fusion.loadFusionsFromClusters(args.clustersFile, True, ids, combatiblityMode=args.oldMode )

    expected_insert_size = []
    if args.insertSizeDist:
        expected_insert_size = loadInsertSizeDistr(args.insertSizeDist)

    fileDir = os.path.dirname(os.path.abspath(args.clustersFile))
    outDir = os.path.join(fileDir, "cluster_analysis")

    if not os.path.exists(outDir):
        os.mkdir(outDir)

    resultingStats = []

    visualize = False if args.noVisualization else True

    for f in fusions:
        if len(f.breakpoints) > 0:
            processFusion(f, outDir, resultingStats, expected_insert_size, visualize, args.onlySplitReads, args.noLegend)
        else:
            sys.stderr.write( "WARNING: fusion %s has zero supporting reads\n" % f.id )


    reportFile = open( os.path.join(outDir, "report.txt"), "w")
    reportFile.write("#ClusterId\tNumReads\tsplit_pos_mean\tsplit_pos_std\t")
    reportFile.write("KS_is_Dvalue\tKS_is_Pvalue\tPAPRate\tCov1\tCov2\tAlnRate\tUniqStarts\tUniqEnds\n")

    for record in resultingStats:
        reportFile.write( '\t'.join(map(str,record)) + "\n")



