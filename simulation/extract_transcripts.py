import argparse
from Bio import SeqIO
from Bio.Seq import Seq

def getRegions( seqName, rStart, startStr, lengthStr ):
    regions = []
    
    starts = startStr.split(",")
    lens = lengthStr.split(",")

    for i,s in enumerate(starts[0:-1]):
        startPos = rStart + int(s) 
        endPos = startPos + int(lens[i])
        regions.append ( (seqName, startPos, endPos) )

    return regions


if __name__ == "__main__":
    
    descriptionText = "The script extracts transcripts (including fusion genes) from a BED file. It also outputs the annotation of each exon in the transcript."
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("-b", action="store", required="true", dest="bedFile", help="Input file with list of genes in UCSC Bed format")
    parser.add_argument("-f", action="store", required="true", dest="fastaFile", help="Input sequence. ")
    parser.add_argument("-t", action="store_true", default=False, dest="transformBed", help="Set this flag to output a transformed BED file (required from SimpleRNASeqSim)")
    parser.add_argument("-o", action="store", dest="outFile", default="output.fa", help="Output file. Default is output.fa")
    
    args = parser.parse_args()
    
    print args
    
    bedFileName = args.bedFile
    fastaFileName = args.fastaFile
    outFileName = args.outFile
    outputTransformedBed = args.transformBed
    
    outBedFile = ""
    if outputTransformedBed:
        transformedBedName = outFileName.replace("fa","bed")
        outBedFile = open(transformedBedName, "w")

    # parse BED file

    features = {}

    for line in open(bedFileName):
        items = line.strip().split("\t")
        regions = []
        rStart = int(items[1])
        seqName = items[0]
        regions = getRegions( seqName, rStart, items[11], items[10])
        
        featureName = items[3]
        transcript = (regions, items[5] )
        #print "Saving transcript ", transcript
        if not featureName in features:
            features[ featureName] =  []

        features[ featureName].append( transcript)

      
    #print features   

    # load & save sequences 

    seqData = SeqIO.to_dict(SeqIO.parse(fastaFileName, "fasta"))
    
    outFile = open(outFileName, "w")

    for id in features:
       
        seq_rec = Seq("")
        
        transcripts = features[id]
        
        for t in transcripts:
            #print "Transcript: ", t
            regions = t[0]
            buf = Seq("")
            for r in regions:
                seqName = r[0]
                if seqName in seqData:
                    seq = seqData[ seqName ].seq[ r[1] : r[2] ]
                    #print "exon: ",seq,"\n"                   
                    buf += seq  
                else:
                    print "Can not locate sequence  %s in %s, skipping region..." % (seqName, fastaFileName)
            
            negativeStrand = True if t[1] == "-" else False
            if negativeStrand:
                buf = buf. reverse_complement()

            seq_rec += buf

        #print "Extracted ", id, ", number of parts: ", len( features[id] )
        if len(seq_rec) > 0:
            header = ">%s " % id
            outFile.write(header + "\n")
            outFile.write(str(seq_rec) + "\n")
        
        if outputTransformedBed:
            tLen = len(seq_rec)
            outBedFile.write("%s\t0\t%d\t%s\t0\t+\t0\t%d\t0\t1\t%d,\t0,\n" %  (id,tLen,id,tLen,tLen))
            

        #outFile.flush()

    outFile.close()


