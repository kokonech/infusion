import argparse
import ConfigParser
import GeneFusion
import subprocess
import logging
import os

logger = logging.getLogger()
fh = logging.FileHandler('run_simulation.log', mode='w')
fh.setFormatter( logging.Formatter('%(asctime)s\n%(levelname)s - %(message)s\n') )
logger.addHandler( fh )
logger.setLevel(logging.DEBUG)

def loadExpressedGenes(file):
    geneIds = []
    for line in open(file):
        if line.startswith("#") or len(line.strip()) == 0:
            continue
        geneIds.append(line.strip())

    return geneIds

def runCmd(args):

    logger.info( "Running " + " ".join(args) )

    child_process = subprocess.Popen( args, stdin=subprocess.PIPE,
        stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True,env=os.environ )

    stdout_str, stderr_str = child_process.communicate()

    return_code = child_process.returncode

    logger.info(stdout_str)

    if len(stderr_str) > 0:
        logger.error(stderr_str)


    if return_code:
        print stdout_str
        print stderr_str
        assert 0

def extractTranscripts(annotations,sequence,output):
    args = ["python", "extract_transcripts.py"]
    args.append("-b")
    args.append(annotations)
    args.append("-f")
    args.append(sequence)
    args.append("-o")
    args.append(output + "/transcripts.fa" )
    args.append("-t")

    runCmd(args)

def simulateReads(outdir, numReads):
    args = [os.path.abspath("simulate_pe_reads.sh"), outdir, numReads ]

    runCmd(args)


def loadGeneNames(bedFile):
    geneIds = []
    for line in open(bedFile):
        if line.startswith("#") or len(line.strip()) == 0:
            continue
        geneIds.append(line.strip().split()[3])

    return geneIds

def showEvidence(outdir, outAnnotationsName):

    args = ["python", "show_evidence.py"]
    args.append("-r")
    args.append( os.path.join(outdir,"sim_reads.filtered.tmp") )
    args.append("-f")
    args.append( outAnnotationsName )
    args.append("-t")
    args.append( os.path.join(outdir, "transcripts.fa"))
    args.append("-o")
    args.append( os.path.join(outdir, "evidence.txt") )

    runCmd(args)

if __name__ == "__main__":
    
    descriptionText = "The script simulates gene fusions"
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("config", help="Configuration file for simulation experiment")
    args = parser.parse_args()
    
    configFile = args.config

    cfgParser = ConfigParser.ConfigParser()

    cfgParser.read(configFile)

    logger.info("Started simulation")

    annotations = cfgParser.get("GLOBAL", "annotations")
    reference = cfgParser.get("GLOBAL", "reference")
    outdir = cfgParser.get("GLOBAL", "outdir")

    geneSubset = []
    if cfgParser.has_option("GLOBAL", "subset"):
        geneSubset = loadExpressedGenes(cfgParser.get("GLOBAL", "subset"))

    if cfgParser.has_option("GLOBAL", "include_all_genes"):
        geneSubset = loadGeneNames(annotations)

    numReads = cfgParser.get("GLOBAL", "numreads")

    if os.path.exists(outdir):
        logger.info(" Output dir already exist, contents will be rewritten...")
    else:
        os.mkdir(outdir)

    fusionGenes = []
    fusions = []

    for fusionName in cfgParser.options("FUSIONS"):
        fusionStr = cfgParser.get("FUSIONS", fusionName)
        f = GeneFusion.parseFusion(fusionStr)
        fusions.append(f)
        for segment in f:
            fusionGenes.append(segment[0])

    # parse genes
    print fusions

    outAnnotationsName = os.path.join( outdir, "expressed_genes.bed")

    GeneFusion.writeAnnotationWithFusions(fusions, fusionGenes, annotations, geneSubset, outAnnotationsName)

    extractTranscripts(outAnnotationsName, reference, outdir )

    simulateReads(outdir,numReads)

    showEvidence(outdir,outAnnotationsName)




