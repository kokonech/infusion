import argparse
import sys

from Bio import SeqIO
from Bio.Seq import Seq

from simlib import *


if __name__ == "__main__":
    
    descriptionText = "Extract fusion transcripts from fusim file."
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-d", action="store", required="true", dest="design", help="Simulation design (Fusim-like BED file)")
    parser.add_argument("-r", action="store", dest="ref", required="true",help="Reference sequence")
    parser.add_argument("-o", action="store", dest="outFile", default="", help="Output file. Default: stdout")

    args = parser.parse_args()
    
    fusionDesign = args.design

    outFile = sys.stdout
    if len(args.outFile) > 0:
        outFile = open(args.outFile, "w")

    designedFusions = parseFusionsDesign(args.design)
    #seqData = SeqIO.index( args.ref, "fasta" )
    seqData = SeqIO.to_dict(SeqIO.parse(args.ref, "fasta"))
    for fusion in designedFusions:
        fusionTranscript = Seq("")
        for part in fusion.parts:
            trPart = Seq("")
            chrom = seqData[part.seqName]
            for i in range(part.getNumExons()):
                # There is a bug in fusim pipeline, it adds +1 to the start of every exon in its file
                # which means start position has to be -2,
                # But there is not bug in InFusion design so it's only -1
                start = part.exonStarts[i] - 1
                end = part.exonEnds[i]
                exonSeq = chrom[start:end]
                #print "Exon (%s, %s,%d,%d)" % (part.transcriptName,part.seqName, start,end)
                trPart += exonSeq
            if part.strand == '-':
                trPart = trPart.reverse_complement()
            fusionTranscript += trPart
            fusionTranscript.id = fusion.name
            fusionTranscript.description = fusion.desc
        outFile.write(">%s %s\n" % (fusion.name, fusion.getDesc()) )
        outFile.write(str(fusionTranscript.seq) + "\n")

                



