import argparse
import sys
import HTSeq

from simlib import *

def pairSpansBreakpoint(aln1, aln2, bp):

    if aln1.start < aln2.start:
        return ( bp >= aln1.end and bp <= aln2.start )
    else:
        return (bp >= aln2.end and bp <= aln1.start )


def createBreakpointsList(designedFusions, outFile):
    outFile.write("#Gene1\tGene2\tChrom1\tChrom2\tPos1\tPos2\tName\n")
    for f in designedFusions:
        #TODO: fix for 3-part fusions

        assert len(f.parts) == 2, "Problem with fusion %s having length %d" % (f.name, len(f.parts) )

        outFile.write("%s\t%s\t" % (f.parts[0].geneName, f.parts[1].geneName) )
        outFile.write("%s\t%s\t" % (f.parts[0].seqName, f.parts[1].seqName) )
        outFile.write("%s\t%s\t%s\n" % (f.parts[0].getChromBreakpoint(True), f.parts[1].getChromBreakpoint(False), f.name) )




if __name__ == "__main__":

    descriptionText = " Show spanning and bridging reads for a fusion. Fusion break point has to be provided as a chromosome coordinate "
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument("-d", action="store", required="true", dest="design", help="Simulation design (Fusim-like BED file)")
    parser.add_argument("-a", action="store", dest="aln", default="", help="Simulated reads alignment")
    parser.add_argument("-o", action="store", dest="outFile", default="", help="Output file. Default: stdout")
    parser.add_argument("-r", action="store_true", dest="reportOnly", default=False,
                        help="Output only report: how many supporting reads each fusion has")
    parser.add_argument("-b", action="store_true", dest="createBreakpoints", default=False,
                        help="Create chromosome breakpoints report")
    args = parser.parse_args()
    
    #print args

    fusionDesign = args.design

    outFile = sys.stdout
    if len(args.outFile) > 0:
        outFile = open(args.outFile, "w")

    designedFusions = parseFusionsDesign(args.design)
    
    fusionDict = {}
    fusionStats = {}

    if args.createBreakpoints:
        createBreakpointsList(designedFusions, sys.stdout)
        exit(0)
    elif not args.aln:
        print "ERROR: the input alignment file (-a) is not provided!"
        exit(-1)


    for fusion in designedFusions:
        #print fusion.name
        fusionDict[fusion.name ] = fusion
        fusionStats[fusion.name ] = 0
    
    pairs = {}
    supportingReads = []

    alnFile = HTSeq.SAM_Reader(args.aln)
    
    for aln in alnFile:
        
        def getFusionName(aln):
            seqName = aln.iv.chrom
            return seqName

        fusionName = getFusionName(aln)
        fusion = fusionDict[fusionName]


        breakPositions = fusion.getTranscriptBreakpoints()
        
        for  bp in breakPositions:
            if aln.iv.start < bp and aln.iv.end > bp:
                mateId = 1 if aln.pe_which == "first" else 2
                supportingReads.append( "%s\t%s\t%d\tSPLIT_%d\tsplit_size=%d\n" % (aln.read.name, fusionName, bp, mateId, aln.iv.start + aln.iv.length - bp + 1) )
                fusionStats[fusionName] += 1
            elif aln.read.name in pairs:
                secondAln = pairs[aln.read.name]
                if pairSpansBreakpoint(aln.iv, secondAln.iv, bp):
                    supportingReads.append( "%s\t%s\t%d\tSPAN_PAIR\n" % (aln.read.name, fusionName, bp) )
                    fusionStats[fusionName] += 1
        else:
                pairs[aln.read.name] = aln


    if args.reportOnly:
        outFile.write("#Fusion\tNumSupporters\n")
        for fusionName in fusionStats:
            outFile.write( "%s\t%s\n" % (fusionName, fusionStats[fusionName]) )
    else:
        outFile.write("#Read\tFusion\tBreakpoint\tType\n")
        for rec in supportingReads:
            outFile.write("%s" % rec )


