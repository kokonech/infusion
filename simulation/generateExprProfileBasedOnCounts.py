import sys
import HTSeq
import argparse
import random

def loadGeneAnnotations(inputGtf, supported_biotypes = ("protein_coding") ):

    gene_isoforms = {}
    transcripts = {}

    gtf_file = HTSeq.GFF_Reader( inputGtf )

    for feature in gtf_file:
        biotype = feature.attr["gene_biotype"]
        if supported_biotypes and not biotype in supported_biotypes:
            continue

        if feature.type == 'exon':

            transcriptName = feature.attr[ "transcript_name" ]
            geneId = feature.attr["gene_id"]

            # add gene
            if geneId not in gene_isoforms:
                gene_isoforms[geneId] = set()
            gene_isoforms[geneId].add(transcriptName)

            # add transcript
            tr_features = transcripts.get(transcriptName, [] )
            if tr_features:
                saved_feature = tr_features[0]
                if saved_feature.attr["transcript_id"] != feature.attr["transcript_id"]:
                    #print "BAD GUY: %s" % transcriptName
                    continue
            tr_features.append(feature)
            transcripts[transcriptName] = tr_features

    return transcripts, gene_isoforms




def distributeCountsAmongIsoforms(gene_isoforms, count):

    isoform_counts = {}

    # peak one dominant isoform randomly
    num_isoforms = len(gene_isoforms)
    indexes = range(num_isoforms)
    indexes = indexes + [random.choice(indexes)]*num_isoforms*2

    while count > 0:
        # we randomly select one isoform out of all with new distribution
        idx = random.choice(indexes)
        isoform = gene_isoforms[idx]
        if isoform not in isoform_counts:
            isoform_counts[isoform] = 0
        isoform_counts[isoform] += 1
        count -= 1

    return isoform_counts


def getTranscriptLength(exons):
    tLen = 0
    for e in exons:
        tLen += e.iv.length
    return tLen

if __name__ == "__main__":
    
    descriptionText = "The script simulates gene fusions from GTF file based on given parameters."
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("-g" , action="store", dest="inputGtf",  required="true", help="Input file in GTF format")

    parser.add_argument("-o", action="store", dest="output", default="-", help="Output fusions file. Default is std out")

    parser.add_argument("-i", action="store", dest="inputCounts", required="true", help="Input counts file")

    parser.add_argument("-s", dest="seed", default=42, type=int, help="Seed for random generator")


    args = parser.parse_args()

    random.seed(args.seed)

    transcripts,gene_isoforms = loadGeneAnnotations(args.inputGtf)

    outputFile = sys.stdout if args.output == "-" else open(args.output, "w")
    outputFile.write("#Name\tLength\tCoverage\tNum_reads\n")

    for line in open(args.inputCounts):
        items = line.split()
        if len(items) != 2:
            continue

        gene_id = items[0]
        count = int(items[1])
        if gene_id not in gene_isoforms or count == 0:
            continue

        expressed_transcripts = distributeCountsAmongIsoforms(list(gene_isoforms[gene_id]), count)
        for tName,numReads in expressed_transcripts.iteritems():
            tLen = getTranscriptLength(transcripts[tName])
            outputFile.write("%s\t%d\t0X\t%d\n" % (tName,tLen,numReads))










