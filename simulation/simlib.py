
class GenePart:

    def __init__(self, geneName, transcriptName = "", seqName = "", strand = '.', exonStarts = (), exonEnds = (), exonIndexes = () ):
        self.geneName = geneName
        self.transcriptName = transcriptName
        self.seqName =seqName
        self.strand = strand
        self.exonStarts = exonStarts if len(exonStarts) > 0 else []
        self.exonEnds = exonEnds if len(exonStarts) > 0 else []
        self.exonIndexes = exonIndexes if len(exonIndexes) > 0 else []
        self.features  = []

        assert len(exonStarts) == len(exonEnds)
        if len(exonStarts) > 0:
            self.compLength()
        else:
            self.length = 0


    def __len__(self):
        return self.length
    
    def getNumExons(self):
        return len(self.exonStarts)

    def getChromBreakpoint(self, isFirst):
        if isFirst:
            return self.exonEnds[-1] if self.strand == '+' else self.exonStarts[0]
        else:
            return self.exonStarts[0] if self.strand == '+' else self.exonEnds[-1]

    def getDesc(self,idx):
        
        desc = "chrom%d=%s " % (idx, self.seqName)
        desc += "strand%d=%s " % (idx, self.strand )
        exonIndexStr = ",".join(map(str,self.exonIndexes))
        desc += "exonIndex%d=%s "  % (idx, exonIndexStr) 

        return desc

    def compLength(self):
        self.length = sum([i2 - i1 + 1 for i1, i2 in zip(self.exonStarts, self.exonEnds)])



class GeneInfo:
    def __init__(self, idx, name, desc, details):
        self.idx = idx
        self.name = name
        self.desc = desc
        self.details = details
        self.parts = []

    def __len__(self):
        l = 0
        for p in self.parts:
            l += len(p)

        return l

    def append(self, part):
        self.parts.append(part)
    

    def getTranscriptBreakpoints(self):
        res = []
        pos = 0
        for p in self.parts:
            pos += len(p)
            res.append(pos)

        return res[0:-1]
    
    def getDesc(self):
        desc = self.desc + " "
        idx = 1
        
        for p in self.parts:
            desc += p.getDesc(idx)
            idx += 1

        desc += " %s" % self.details

        return desc

def parseFusionsDesign(fusionsFile):

    fusions = {}
    count = 0

    for line in open(fusionsFile):
        if line.startswith("fusionGene"):
            continue

        idx = count / 2
        items = line.split()
        assert len(items) == 12, "Problematic line %d:\n %s" % (count, line)

        name = items[0]
        
        if idx not in fusions:
            fusions[idx] = GeneInfo(idx, name,items[10],items[11])

        geneName = items[1]
        transcriptName = items[2]
        seqName = items[3]
        strand = items[4]
        
        exonIndexes = map(int, items[7].split(","))
        exonStarts = map(int, items[8].split(","))
        exonEnds = map(int, items[9].split(",") )

        p = GenePart(geneName, transcriptName, seqName, strand, exonStarts, exonEnds, exonIndexes )
        fusions[idx].append(p)

        count += 1


    return fusions.values()
