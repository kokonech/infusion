import sys
import argparse
from Bio import SeqIO
from Bio.Seq import Seq
import HTSeq


def idsContainGiven(givenId, transcriptIds):
    for tId in transcriptIds:
        #if givenId.find(tId) != -1:
        if givenId == tId:
            return True

    return False


def loadIdList(filterFile):
    ids = set()
    for line in open(filterFile):
        if not line or line.startswith("#"):
            continue
        ids.add(line.split()[0])

    return ids


if __name__ == "__main__":
    
    descriptionText = "The script extracts transcripts from a GTF file. Note: exons have to be sorted according to exon number! This important for correct reverse transcribed cDNA sequences extraction."

    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument("-g", action="store", required="true", dest="gtfFile",
        help="Input file with list of genes in UCSC Bed format")
    parser.add_argument("-f", action="store", required="true", dest="fastaFile",
        help="Input genome sequence. ")
    parser.add_argument("-o", action="store", dest="outFile", default="output.fa",
        help="Output file. Default is output.fa")

    parser.add_argument("--filter", action="store", dest="filterStr", default="",
                        help="Comma-separted list of entries to filter from GTF file based on given attribute id")
    parser.add_argument("--filter-file", action="store", dest="filterFile", default="",
                        help="Column of ids to filter, similar to grep -f")


    parser.add_argument("--attr-id", action="store", dest="attrId", default="transcript_id",
                        help="Attribute to select the transcript. Default is \"transcript_id\"")

    parser.add_argument("--ignore-strange-chrom", action="store_true", default=False,
        dest="ignoreStrangeChromosomes", help="All chromosomes except numbered and X,Y,MT are ignored ")
    parser.add_argument("--output-exons", action="store_true", default=False,
        dest="writeExons", help="Output additionaly each exon for corresponding transcript sequence")

    args = parser.parse_args()
    
    print args
    
    gtfFileName = args.gtfFile
    fastaFileName = args.fastaFile
    outFileName = args.outFile

    # parse GTF file

    gtf_file = HTSeq.GFF_Reader( gtfFileName )

    transcripts = {}

    if args.filterFile:
        filtered_transcripts = loadIdList(args.filterFile)
    else:
        filtered_transcripts = set(args.filterStr.split(","))

    if filtered_transcripts:
        print "Enabled filtering. Expected to select %d records." % len(filtered_transcripts)

    for feature in gtf_file:
        if feature.type == 'exon':
            transcriptName = feature.attr[ args.attrId ]
            #print transcriptName
            #ys.stdin.readline()
            if len(filtered_transcripts) > 0 and transcriptName not in filtered_transcripts:
                continue
            if transcriptName in transcripts:
                transcripts[transcriptName].append(feature)
            else:
                transcripts[transcriptName] = [feature]

    if len(transcripts) == 0:
        print "No transcripts selected"
        sys.exit(0)

    # load & save sequences

    seqData = SeqIO.to_dict(SeqIO.parse(fastaFileName, "fasta"))
    
    outFile = open(outFileName, "w")

    for tid in transcripts:
       
        seq_rec = Seq("")
        transcript = transcripts[tid]

        print "Processing %s\n" % tid
        exonSeqs = []

        for exon in transcript:
            iv = exon.iv
            seqName = iv.chrom
            if seqName in seqData:
                #print "Exon (%s,%d,%d) " % (iv.chrom,iv.start,iv.end)
                buf = seqData[ iv.chrom ].seq[ iv.start  : iv.end ]
                if iv.strand == '-':
                    buf = buf.reverse_complement()
                if args.writeExons:
                    exonSeqs.append( (exon.attr["exon_number"], buf) )
                seq_rec += buf
            else:
                print "Can not locate sequence  %s in %s, skipping region..." % (seqName, fastaFileName)

         #print "Extracted ", id, ", number of parts: ", len( features[id] )
        if len(seq_rec) > 0:
            header = ">%s" % tid
            outFile.write(header + "\n")
            outFile.write(str(seq_rec) + "\n")
            if args.writeExons:
                for seq in exonSeqs:
                    header = ">%s_exon_%s" % (tid,seq[0])
                    outFile.write(header + "\n")
                    outFile.write(str(seq[1]) + "\n")
                    
        #outFile.flush()

    outFile.close()


