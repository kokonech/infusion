import sys
import argparse
import os
import random
import subprocess

from simlib import *
from Bio import SeqIO

def processTranscripts(transcripts):
    result = []

    count = 0
    for name, t in transcripts.iteritems():
        geneInfo = GeneInfo(count, name, "normal_transcript", "")

        genePart = GenePart(name)
        genePart.length = len(t)
        geneInfo.append(genePart)

        result.append(geneInfo)
        count += 1

    return result

def loadPredefinedExpression(genes, pathToProfile):

    gene_coverage = {}

    for line in open(pathToProfile):
        if line.startswith("#"):
            continue
        items = line.strip().split()
        name = items[0]
        coverage = int(items[2].lower().replace("x",""))
        num_reads = int(items[3])
        gene_coverage[name] = (coverage, num_reads )

    for g in genes:
        g.cov, g.numReads = gene_coverage[g.name]


def assignExpression(genes, fixedCovLevel = 0, minCovLevel=1, maxCovLevel=50 ):

    totalWeight = 0.

    for g in genes:
        if fixedCovLevel:
            g.cov = fixedCovLevel
        else:
            g.cov = random.randrange(minCovLevel, maxCovLevel)
        totalWeight += len(g)*g.cov

    assert totalWeight != 0

    for g in genes:
        g.weight = (len(g)*g.cov)/totalWeight

def parseFusionSeqs(fileName):
    seqDict = {}
    for record in SeqIO.parse( open(fileName), "fasta"):
        #fusionGene = record.description.split("fusionGene=")[1].split(" ")[0]
        fusionGene = record.id
        seqDict[fusionGene] = record

    return seqDict

def simulateSequencing(f, seq, outDir, numReads, readSize, fLenMean, fLenStd, seed, protocol):

    seqName = f.name + ".fa"
    seqFileName= os.path.join(outDir, seqName )
    seqFile = open(seqFileName, "w")
    
    seq.id = f.name

    SeqIO.write(seq, seqFile, "fasta")
    seqFile.close()


    runCmd = "mason illumina -N %d -sq -mp -rn 2 -pi 0 -pd 0 -n %d -ll %d -le %d -s %d" % \
             (numReads, readSize, fLenMean, fLenStd,seed)

    if protocol == 1:
        runCmd += " -f"
    elif protocol == 2:
        runCmd += " -r"

    runCmd += " %s" % seqName
    print runCmd
    #print
    args = runCmd.split()

    child_process = subprocess.Popen( args, stdin=subprocess.PIPE, cwd=outDir,
                                      stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True )

    stdout_str, stderr_str = child_process.communicate()

    return_code = child_process.returncode

    if return_code:
        print stdout_str
        print stderr_str
        assert 0
    
    #sys.exit(-1)

    os.remove(seqFileName)

    return (seqFileName + "_1.fastq", seqFileName + "_2.fastq", seqFileName + ".fastq.sam")


def createOutputDir(outDir):

    if os.path.exists(outDir):
        print "Warning! Output dir already exist, contents will be rewritten..."
        pass
    else:
        os.mkdir(outDir)

def mergeReadFiles(readsFile, outputFile):
    with open(readsFile) as infile:
        outputFile.write(infile.read())
    os.remove(readsFile)

def mergeAlignments(alnFile, headerData, outputFile):
    with open(alnFile) as infile:
        for line in infile:
            if line.startswith("@"):
                if line.startswith("@SQ"):
                    headerData.append(line)
            else:
                outputFile.write(line)
    os.remove(alnFile)



if __name__ == "__main__":
    
    descriptionText = "Simulate sequencing experiment for give fusion design "
    
    parser = argparse.ArgumentParser(description = descriptionText,formatter_class=argparse.RawDescriptionHelpFormatter)
    
    parser.add_argument(dest="fusionsFile", nargs='?', default="", help="File describing fusions design.")

    parser.add_argument("-t", dest="transcripts", default="", help="Input file with transcripts for background reads")
    parser.add_argument("-ft", dest="fusionTranscripts", help="Input file with fusion transcript sequences")
    parser.add_argument("-s", dest="seed", default=42, type=int, help="Seed for random generator")
    parser.add_argument("-l", dest="fragmentLength", default=300, type=int,
                        help="Mean insert size. Everything less that this size will be skipped, "
                             "since insert size won't be working.")
    parser.add_argument("-L", dest="readLength", default=75, type=int, help="Read length.")
    parser.add_argument("-n", dest="totalNumReads", default=0, type=int, help="Given total number of reads in the experiment")
    parser.add_argument("-i", dest="predefinedExpr", default="", help="Predefined expression profile")
    parser.add_argument("-p", dest="protocol", type=int, default=0,
                        help="Sequencing protocol. 0 - non-strand specific, 1 - strand-specific forward, 2 - strand-specific reverse. Default: 0")
    parser.add_argument("--fixed-cov", dest="fixedCoverage", default=0, type=int,
                        help="Set fixed coverage for each fusion instead of simulating it randomly ")

    parser.add_argument("-o", dest="outDir", default="run_output", help="Output directory")
    

    args = parser.parse_args()
    
    print args
    assert args.fixedCoverage >= 0

    if args.protocol < 0 or args.protocol > 2:
        sys.stderr.write("Wrong protocol settings!\n")
        exit(-1)

    random.seed(args.seed)

    if args.fusionsFile:
        fusions = parseFusionsDesign(args.fusionsFile)
        fusionSeqs = parseFusionSeqs( args.fusionTranscripts )
    else:
        fusions = []
        fusionSeqs = []
        print "Fusions file is not provided, processing normal transcripts only"
        assert(len(args.transcripts) > 0)

    tDict = {}
    normalGenes = []
    if len(args.transcripts) > 0:
        tDict = SeqIO.to_dict(SeqIO.parse(args.transcripts, "fasta"))
        normalGenes = processTranscripts(tDict)
        print len(normalGenes)

    genes = fusions + normalGenes

    if args.predefinedExpr:
        loadPredefinedExpression(genes, args.predefinedExpr)
    else:
        assignExpression(genes, args.fixedCoverage)

    createOutputDir(args.outDir)

    if not args.predefinedExpr:
        exprProfile = open(os.path.join(args.outDir, "expr.txt"), "w")

        if not args.totalNumReads:
            print "Total number of reads is not given, thus will be generated based on the required coverage values"

        exprProfile.write("#Name\tLength\tCoverage\tNum_reads\n")
        for gene in genes:
            if args.totalNumReads:
                gene.numReads = int(gene.weight*args.totalNumReads)
            else:
                tLen = len(fusionSeqs[gene.name])
                print "(%d*%d) / (2*%d)" % (gene.cov, tLen,args.readLength )
                gene.numReads = (gene.cov*tLen) / (args.readLength*2)
            if len(gene) >= args.fragmentLength:
                exprProfile.write( "%s\t%d\t%dx\t%d\n" % (gene.name, len(gene), gene.cov, gene.numReads ) )
            else:
                exprProfile.write( "%s\t%d\t%dx\t%d\tsize_skipped\n" % (gene.name, len(gene), 0, 0 ) )

        exprProfile.close()

    readsFile1 = open( os.path.join(args.outDir, "reads_1.fq"), "w" )
    readsFile2 = open( os.path.join(args.outDir, "reads_2.fq"), "w" )
    alignmentsFile = open (os.path.join(args.outDir, "reads_alignment.sam"), "w")

    headerData = []

    for gene in genes:
        seq = tDict[gene.name] if gene.desc == "normal_transcript" else fusionSeqs[gene.name]

        # Library size selection

        if len(seq) < args.fragmentLength:
            continue

        r1,r2,aln = simulateSequencing(gene, outDir=args.outDir, seq=seq, numReads=gene.numReads,
                           readSize=args.readLength, fLenMean=args.fragmentLength, fLenStd=100,
                           seed=args.seed, protocol=args.protocol)

        mergeReadFiles(r1, readsFile1  )
        mergeReadFiles(r2, readsFile2  )
        mergeAlignments(aln, headerData, alignmentsFile)

    headerFile = open (os.path.join(args.outDir, "reads_alignment.sam.header"), "w")
    headerFile.write("@HD\tVN:1.4\tSO:unsorted\n")
    for line in headerData:
        headerFile.write(line)
    headerFile.write("@PG\tID:InFusion\n")
